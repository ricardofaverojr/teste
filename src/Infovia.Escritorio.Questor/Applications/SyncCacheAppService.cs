﻿using System;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.ServicesCache.Interfaces;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ApplicationsQuestor
{
    public class SyncCacheAppService : ISyncCacheAppService
    {
        private readonly IDepartamentoServiceQuestor _departamentoService;
        private readonly IGrupoServiceQuestor _grupoService;
        private readonly IEmpresaServiceQuestor _empresaService;
        private readonly IEstabelecimentoServiceQuestor _estabService;
        private readonly IUsuarioServiceQuestor _usuarioService;
        private readonly IGrupoEmpresaServiceQuestor _grupoEmpresaService;
        private readonly IGrupoUsuarioServiceQuestor _grupoUsuarioService;

        private readonly IDepartamentoServiceCache _departamentoCache;
        private readonly IGrupoServiceCache _grupoCache;
        private readonly IEmpresaServiceCache _empresaCache;
        private readonly IEstabelecimentoServiceCache _estabCache;
        private readonly IUsuarioServiceCache _usuarioCache;
        private readonly IGrupoEmpresaServiceCache _grupoEmpresaCache;
        private readonly IGrupoUsuarioServiceCache _grupoUsuarioCache;


        public SyncCacheAppService(IDepartamentoServiceQuestor departamentoService, IGrupoServiceQuestor grupoService, IEmpresaServiceQuestor empresaService, IEstabelecimentoServiceQuestor estabService, IUsuarioServiceQuestor usuarioService, IGrupoEmpresaServiceQuestor grupoEmpresaService, IGrupoUsuarioServiceQuestor grupoUsuarioService, IDepartamentoServiceCache departamentoCache, IGrupoServiceCache grupoCache, IEmpresaServiceCache empresaCache, IEstabelecimentoServiceCache estabCache, IUsuarioServiceCache usuarioCache, IGrupoEmpresaServiceCache grupoEmpresaCache, IGrupoUsuarioServiceCache grupoUsuarioCache)
        {
            _departamentoService = departamentoService;
            _grupoService = grupoService;
            _empresaService = empresaService;
            _estabService = estabService;
            _usuarioService = usuarioService;
            _grupoEmpresaService = grupoEmpresaService;
            _grupoUsuarioService = grupoUsuarioService;

            _departamentoCache = departamentoCache;
            _grupoCache = grupoCache;
            _empresaCache = empresaCache;
            _estabCache = estabCache;
            _usuarioCache = usuarioCache;
            _grupoEmpresaCache = grupoEmpresaCache;
            _grupoUsuarioCache = grupoUsuarioCache;
        }

        public bool StoreCache()
        {
            try
            {
                CleanStore();

                var departamentos = _departamentoService.GetAll();

                _departamentoCache.AddRange(departamentos);
                _grupoCache.AddRange(_grupoService.GetAll());
                _empresaCache.AddRange(_empresaService.GetAll());
                _estabCache.AddRange(_estabService.GetAll());
                _usuarioCache.AddRange(_usuarioService.GetAll());
                _grupoEmpresaCache.AddRange(_grupoEmpresaService.GetAll());
                _grupoUsuarioCache.AddRange(_grupoUsuarioService.GetAll());

                return true;
            }
            catch (Exception)
            {
                CleanStore();
                return false;
            }
        }

        private void CleanStore()
        {
            _departamentoCache.DeleteAll();
            _grupoCache.DeleteAll();
            _empresaCache.DeleteAll();
            _estabCache.DeleteAll();
            _usuarioCache.DeleteAll();
            _grupoUsuarioCache.DeleteAll();
            _grupoEmpresaCache.DeleteAll();
        }
    }
}
