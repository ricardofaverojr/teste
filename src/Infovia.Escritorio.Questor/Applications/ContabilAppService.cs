﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.Applications
{
    public class ContabilAppService : IContabilAppService
    {
        private readonly IHistoricoContabilServiceQuestor _historicoContabilQuestor;
        private readonly IPlanoEspecificoServiceQuestor _planoEspecificoQuestor;

        public ContabilAppService(IHistoricoContabilServiceQuestor historicoContabilQuestor, IPlanoEspecificoServiceQuestor planoEspecificoQuestor)
        {
            _historicoContabilQuestor = historicoContabilQuestor;
            _planoEspecificoQuestor = planoEspecificoQuestor;
        }

        public IEnumerable<HistoricoContabilQuestor> GetHistoricoCtb() => _historicoContabilQuestor.GetAll();

        public IEnumerable<PlanoEspecificoQuestor> GetPlanoEspecifico(int codigo) => _planoEspecificoQuestor.GetByCommand($"SELECT CONTACTB AS CodigoConta,TIPOCONTA AS TipoConta,CLASSIFCONTA AS Classificacao,DESCRCONTA AS DescricaoConta FROM PLANOESPEC WHERE CODIGOEMPRESA = {codigo}");
    }
}
