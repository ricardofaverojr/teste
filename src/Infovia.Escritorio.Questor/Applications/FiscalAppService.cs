﻿using System.Collections.Generic;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ApplicationsQuestor
{
    public class FiscalAppService : IFiscalAppService
    {
        private readonly IGrupoProdutoServiceQuestor _grupoProdutoQuestor;

        public FiscalAppService(IGrupoProdutoServiceQuestor grupoProdutoQuestor)
        {
            _grupoProdutoQuestor = grupoProdutoQuestor;
        }

        public IEnumerable<GrupoProdutoQuestor> GetGrupoProdutos(int codigo) => _grupoProdutoQuestor.GetByCommand($"SELECT CodigoGrupoProduto, DESCRGRUPOPRODUTO AS DescricaoGrupoProduto FROM GRUPOPRODUTO WHERE CODIGOEMPRESA = {codigo}");
    }
}
