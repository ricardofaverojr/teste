﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.Applications.Interfaces
{
    public interface IContabilAppService
    {
        IEnumerable<HistoricoContabilQuestor> GetHistoricoCtb();

        IEnumerable<PlanoEspecificoQuestor> GetPlanoEspecifico(int codigo);
    }
}
