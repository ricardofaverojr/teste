﻿namespace Infovia.Escritorio.Questor.Applications.Interfaces
{
    public interface ISyncCacheAppService
    {
        bool StoreCache();
    }
}
