﻿using System.Collections.Generic;
using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.Applications.Interfaces
{
    public interface IFiscalAppService
    {
        IEnumerable<GrupoProdutoQuestor> GetGrupoProdutos(int codigo);
    }
}
