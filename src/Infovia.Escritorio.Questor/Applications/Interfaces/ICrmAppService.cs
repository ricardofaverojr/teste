﻿using System.Collections.Generic;
using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.Applications.Interfaces
{
    public interface ICrmAppService
    {
        IEnumerable<DepartamentoQuestor> GetDepartamentos();
        IEnumerable<EmpresaQuestor> GetEmpresasAtivas();
        IEnumerable<GrupoQuestor> GetGrupos();
        IEnumerable<EstabelecimentoQuestor> GetEstabsAtivas();
        IEnumerable<GrupoEmpresaQuestor> GetGruposEmpresas();
        IEnumerable<GrupoUsuarioQuestor> GetGruposUsuarios();
        IEnumerable<UsuarioQuestor> GetUsuarios();
    }
}
