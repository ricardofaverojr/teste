﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesCache.Interfaces;

namespace Infovia.Escritorio.Questor.Applications
{
    public class CrmAppService : ICrmAppService
    {
        public CrmAppService(IDepartamentoServiceCache departamentoCache, IEmpresaServiceCache empresaCache, IEstabelecimentoServiceCache estabCache, IGrupoServiceCache grupoCache, IGrupoEmpresaServiceCache grupoEmpresaCache, IGrupoUsuarioServiceCache grupoUsuarioCache, IUsuarioServiceCache usuarioCache)
        {
            _departamentoCache = departamentoCache;
            _empresaCache = empresaCache;
            _estabCache = estabCache;
            _grupoCache = grupoCache;
            _grupoEmpresaCache = grupoEmpresaCache;
            _grupoUsuarioCache = grupoUsuarioCache;
            _usuarioCache = usuarioCache;
        }

        private readonly IDepartamentoServiceCache _departamentoCache;
        private readonly IEmpresaServiceCache _empresaCache;
        private readonly IEstabelecimentoServiceCache _estabCache;
        private readonly IGrupoServiceCache _grupoCache;
        private readonly IGrupoEmpresaServiceCache _grupoEmpresaCache;
        private readonly IGrupoUsuarioServiceCache _grupoUsuarioCache;
        private readonly IUsuarioServiceCache _usuarioCache;

        public IEnumerable<DepartamentoQuestor> GetDepartamentos() => _departamentoCache.Get();

        public IEnumerable<EmpresaQuestor> GetEmpresasAtivas() => _empresaCache.Get();

        public IEnumerable<GrupoQuestor> GetGrupos() => _grupoCache.Get();

        public IEnumerable<EstabelecimentoQuestor> GetEstabsAtivas() => _estabCache.Get();

        public IEnumerable<GrupoEmpresaQuestor> GetGruposEmpresas() => _grupoEmpresaCache.Get();

        public IEnumerable<GrupoUsuarioQuestor> GetGruposUsuarios() => _grupoUsuarioCache.Get();

        public IEnumerable<UsuarioQuestor> GetUsuarios() => _usuarioCache.Get();
    }
}
