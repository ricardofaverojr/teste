﻿using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesCache.Interfaces;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.ServicesCache
{
    public class DepartamentoServiceCache : ServiceCache<DepartamentoQuestor>, IDepartamentoServiceCache
    {
        public DepartamentoServiceCache()
        {
            Name = "departamento";
        }
    }
}
