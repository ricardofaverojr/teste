﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesCache.Interfaces
{
    public interface IGrupoEmpresaServiceCache : IServiceCache<GrupoEmpresaQuestor>
    {
    }
}
