﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class GrupoUsuarioQuestor : ModelCache
    {
        public int CodigoUsuario { get; set; }
        public string NomeUsuario { get; set; }
        public decimal CodigoGrupo { get; set; }
        public string NomeGrupo { get; set; }
        public decimal CodigoDepartamento { get; set; }
        public string DescricaoDepartamento { get; set; }
    }
}
