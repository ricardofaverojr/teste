﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class EmpresaQuestor : ModelCache
    {
        public int CodigoEmpresa { get; set; }
        public string NomeEmpresa { get; set; }
    }
}
