﻿using System;
using Infovia.Escritorio.Questor.Enums;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class EstabelecimentoQuestor : ModelCache
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoEstab { get; set; }
        public string NomeEstab { get; set; }
        public string Apelido { get; set; }
        public DateTime InicioAtividade { get; set; }
        public DateTime EncerramentoAtividade { get; set; }
        public ETipoInscricao TipoInscricao { get; set; }
        public string InscricaoFederal { get; set; }
        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }
        public string SiglaEstado { get; set; }
        public string NomeMunicipio { get; set; }
    }
}
