﻿using System;
namespace Infovia.Escritorio.Questor.Models
{
    public class PlanoEspecificoQuestor
    {
        public PlanoEspecificoQuestor() => Ativo = true;

        public decimal CodigoConta { get; set; }
        public int TipoConta { get; set; }
        public string Classificacao { get; set; }
        public string DescricaoConta { get; set; }
        public bool Ativo { get; set; }
    }
}
