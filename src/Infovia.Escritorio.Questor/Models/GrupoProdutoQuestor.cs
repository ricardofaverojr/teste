﻿using System;
namespace Infovia.Escritorio.Questor.Models
{
    public class GrupoProdutoQuestor
    {
        public int CodigoGrupoProduto { get; set; }
        public string DescricaoGrupoProduto { get; set; }
    }
}
