﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class GrupoQuestor : ModelCache
    {
        public decimal CodigoGrupo { get; set; }
        public string NomeGrupo { get; set; }
    }
}
