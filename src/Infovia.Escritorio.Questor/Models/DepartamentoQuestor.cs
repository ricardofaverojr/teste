﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class DepartamentoQuestor : ModelCache
    {
        public decimal CodigoDepartamento { get; set; }
        public string DescricaoDepartamento { get; set; }
    }
}
