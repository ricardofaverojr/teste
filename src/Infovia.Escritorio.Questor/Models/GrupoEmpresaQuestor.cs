﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class GrupoEmpresaQuestor : ModelCache
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoEstab { get; set; }
        public string NomeEmpresa { get; set; }
        public decimal CodigoGrupo { get; set; }
        public string NomeGrupo { get; set; }
        public decimal CodigoDepartamento { get; set; }
        public string DescricaoDepartamento { get; set; }
    }
}
