﻿using System;
namespace Infovia.Escritorio.Questor.Models
{
    public class HistoricoContabilQuestor
    {
        public HistoricoContabilQuestor() => Ativo = true;

        public int CodigoHistorico { get; set; }
        public string DescricaoHistorico { get; set; }
        public bool Ativo { get; set; }
    }
}
