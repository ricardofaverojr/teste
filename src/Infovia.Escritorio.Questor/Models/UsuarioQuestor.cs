﻿using System;
using Infovia.Escritorio.Questor.ServicesQuestor;

namespace Infovia.Escritorio.Questor.Models
{
    public class UsuarioQuestor : ModelCache
    {
        public int CodigoUsuario { get; set; }
        public string NomeUsuario { get; set; }
        public string EmailUsuario { get; set; }
        public DateTime? DataBaixa { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
