﻿using System;
using System.Data;
using System.IO;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class ConnectionQuestor : IConnectionQuestor
    {
        public string Default()
        {
            var con = Connection();
            const string sql = "SELECT Usuario, Senha, Caminho, Endereco, Porta FROM BancoQuestor WHERE Padrao = 1";
            con.Open();
            var command = new SqlCommand(sql, con);
            if (con.State != ConnectionState.Open) return null;
            var data = command.ExecuteReader();
            var connectionString = GetConnection(data);
            command.Dispose();
            data.Close();
            con.Close();
            return connectionString;
        }

        public string ConnectionId(Guid id)
        {
            var con = Connection();
            var sql = $"SELECT Usuario, Password, Caminho, Endereco, Porta FROM BancoQuestor WHERE Id = '{id}'";
            con.Open();
            var command = new SqlCommand(sql, con);
            if (con.State != ConnectionState.Open) return null;
            var data = command.ExecuteReader();
            var connectionString = GetConnection(data);
            command.Dispose();
            data.Close();
            con.Close();
            return connectionString;
        }

        private static SqlConnection Connection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile($"appsettings.json");
            var configuration = builder.Build();
            return new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }

        private static string GetConnection(SqlDataReader data)
        {
            if (!data.HasRows) return null;
            while (data.Read())
            {
                var connection =
                    $"User={data[0]};Password={data[1]};Database={data[2]};DataSource={data[3]};Port={data[4]};Dialect=3;Charset=ISO8859_1;Role=;Connection lifetime=15;Pooling=true;Packet Size=8192;ServerType=0";
                return connection;
            }
            return null;
        }
    }
}
