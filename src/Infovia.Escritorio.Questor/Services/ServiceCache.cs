﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class ServiceCache<T> : IServiceCache<T> where T : ModelCache
    {
        public string Name { get; set; }
        private readonly IMongoDatabase _database;

        public ServiceCache() => _database = Connection();

        private static IMongoDatabase Connection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile($"appsettings.json");
            var configuration = builder.Build();
            var client = new MongoClient(configuration.GetConnectionString("CacheConnection"));
            return client.GetDatabase("QuestorCacheDB");
        }

        public void AddRange(IEnumerable<T> list)
        {
            var con = _database.GetCollection<T>(Name);
            con.InsertMany(list);
        }

        public void DeleteAll() => _database.DropCollection(Name);

        public void DeleteByExpression(FilterDefinition<T> filter)
        {
            var con = _database.GetCollection<T>(Name);
            con.DeleteOne(filter); ;
        }

        public IEnumerable<T> Get() => _database.GetCollection<T>(Name).AsQueryable().ToList();

        public T GetByCodigo(FilterDefinition<T> filter) => _database.GetCollection<T>(Name).Find(filter).FirstOrDefault();

        public IEnumerable<T> GetByFilter(FilterDefinition<T> filter) => _database.GetCollection<T>(Name).Find(filter).ToList();
    }
}
