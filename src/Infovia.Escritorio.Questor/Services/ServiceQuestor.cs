﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;
using Infovia.NugetLibrary.Firebird.Models;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Core
{
    public class ServiceQuestor<T> : IServiceQuestor<T> where T : class
    {
        public string CommandSql { get; set; }
        private readonly string _connection;

        public ServiceQuestor(IConnectionQuestor connection) => _connection = connection.Default();

        public IEnumerable<T> GetAll() => _connection != null ? new Import<T>(_connection, CommandSql).Get() : null;

        public IEnumerable<T> GetByCommand(string commandSql) => _connection != null ? new Import<T>(_connection, CommandSql).Get(commandSql) : null;

        public IEnumerable<T> GetByPredicate(Func<T, bool> predicate) => _connection != null ? new Import<T>(_connection, CommandSql).Get(predicate) : null;
    }
}
