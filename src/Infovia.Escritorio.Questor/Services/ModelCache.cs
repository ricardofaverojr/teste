﻿using System;
using MongoDB.Bson;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public abstract class ModelCache
    {
        public ObjectId Id { get; set; }
    }
}
