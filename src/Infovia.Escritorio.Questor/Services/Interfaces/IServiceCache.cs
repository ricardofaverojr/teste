﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IServiceCache<T> where T : ModelCache
    {
        string Name { get; set; }

        void AddRange(IEnumerable<T> list);
        void DeleteByExpression(FilterDefinition<T> filter);
        void DeleteAll();

        IEnumerable<T> Get();
        IEnumerable<T> GetByFilter(FilterDefinition<T> filter);
        T GetByCodigo(FilterDefinition<T> filter);
    }
}
