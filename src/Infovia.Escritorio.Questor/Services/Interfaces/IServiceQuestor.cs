﻿using System;
using System.Collections.Generic;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IServiceQuestor<out T> where T : class
    {
        string CommandSql { get; set; }
        IEnumerable<T> GetAll();
        IEnumerable<T> GetByCommand(string commandSql);
        IEnumerable<T> GetByPredicate(Func<T, bool> predicate);
    }
}
