﻿using System;
namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IConnectionQuestor
    {
        string Default();
        string ConnectionId(Guid id);
    }
}
