﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class GrupoServiceQuestor : ServiceQuestor<GrupoQuestor>, IGrupoServiceQuestor
    {
        public GrupoServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT ID AS CodigoGrupo, NOME AS NomeGrupo FROM LEANGRUPOS ORDER BY 2";
        }
    }
}
