﻿using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IGrupoEmpresaServiceQuestor : IServiceQuestor<GrupoEmpresaQuestor>
    {
    }
}
