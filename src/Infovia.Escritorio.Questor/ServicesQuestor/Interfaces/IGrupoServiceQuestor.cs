﻿using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IGrupoServiceQuestor : IServiceQuestor<GrupoQuestor>
    {
    }
}
