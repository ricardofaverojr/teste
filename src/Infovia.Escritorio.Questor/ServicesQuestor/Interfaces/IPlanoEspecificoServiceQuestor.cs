﻿using System;
using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IPlanoEspecificoServiceQuestor : IServiceQuestor<PlanoEspecificoQuestor>
    {
    }
}
