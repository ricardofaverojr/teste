﻿using Infovia.Escritorio.Questor.Models;

namespace Infovia.Escritorio.Questor.ServicesQuestor.Interfaces
{
    public interface IHistoricoContabilServiceQuestor : IServiceQuestor<HistoricoContabilQuestor>
    {
    }
}
