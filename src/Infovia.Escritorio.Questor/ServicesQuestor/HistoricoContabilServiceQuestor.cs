﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class HistoricoContabilServiceQuestor : ServiceQuestor<HistoricoContabilQuestor>, IHistoricoContabilServiceQuestor
    {
        public HistoricoContabilServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT CODIGOHISTCTB AS CodigoHistorico,DESCRHISTCTB AS DescricaoHistorico FROM HISTORICOCTB";
        }
    }
}
