﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class EstabelecimentoServiceQuestor : ServiceQuestor<EstabelecimentoQuestor>, IEstabelecimentoServiceQuestor
    {
        public EstabelecimentoServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT E.CodigoEmpresa, E.CodigoEstab, E.NomeEstab, E.APELIDOESTAB AS Apelido, E.DataInicioAtiv as InicioAtividade, E.DataEncerAtiv as EncerramentoAtividade, E.TIPOINSCR as TipoInscricao, E.INSCRFEDERAL AS InscricaoFederal, " +
                         "E.INSCRESTAD AS InscricaoEstadual, E.INSCRMUNIC AS InscricaoMunicipal, E.SiglaEstado, M.NOMEMUNIC AS NomeMunicipio FROM ESTAB E INNER JOIN MUNICIPIO M ON E.SIGLAESTADO = M.SIGLAESTADO AND E.CODIGOMUNIC = M.CODIGOMUNIC WHERE E.CODIGOEMPRESA IN " +
                         "(SELECT CODIGOEMPRESA FROM CLIENTE WHERE CODIGOCLIENTE IN (SELECT CODIGOCLIENTE FROM SERVICOFIXO WHERE COMPETFINALVALID IS NULL)) ORDER BY 1,2";
        }
    }
}
