﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class GrupoUsuarioServiceQuestor : ServiceQuestor<GrupoUsuarioQuestor>, IGrupoUsuarioServiceQuestor
    {
        public GrupoUsuarioServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT LGF.USUARIOID AS CodigoUsuario,U.NOMEUSUARIOCOMPL AS NomeUsuario,LGF.GRUPOID AS CodigoGrupo,LG.NOME AS NomeGrupo, LD.ID AS CodigoDepartamento, LD.DESCRDPTOESCRIT AS DescricaoDepartamento FROM LEANGRUPOFUNCIONARIOS LGF INNER JOIN USUARIO U ON LGF.USUARIOID = U.CODIGOUSUARIO INNER JOIN LEANGRUPOS LG ON LGF.GRUPOID = LG.ID INNER JOIN LEANDEPARTAMENTOS LD ON LD.DESCRDPTOESCRIT = IIF(POSITION(' ' IN LG.NOME) > 0, SUBSTRING(LG.NOME FROM 1 FOR POSITION(' ' IN LG.NOME)), LG.NOME) WHERE U.EMAILUSUARIO IS NOT NULL ORDER BY 4,2";
        }
    }
}
