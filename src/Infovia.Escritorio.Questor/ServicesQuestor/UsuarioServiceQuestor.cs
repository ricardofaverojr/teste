﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class UsuarioServiceQuestor : ServiceQuestor<UsuarioQuestor>, IUsuarioServiceQuestor
    {
        public UsuarioServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT CodigoUsuario,NOMEUSUARIOCOMPL AS NomeUsuario,EmailUsuario,DATABAIXAUSUARIO AS DataBaixa,DataAlteracao FROM USUARIO WHERE EMAILUSUARIO IS NOT NULL";
        }
    }
}
