﻿using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class DepartamentoServiceQuestor : ServiceQuestor<DepartamentoQuestor>, IDepartamentoServiceQuestor
    {
        public DepartamentoServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT ID AS CodigoDepartamento, DESCRDPTOESCRIT AS DescricaoDepartamento FROM LEANDEPARTAMENTOS ORDER BY 2";
        }
    }
}
