﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class PlanoEspecificoServiceQuestor : ServiceQuestor<PlanoEspecificoQuestor>, IPlanoEspecificoServiceQuestor
    {
        public PlanoEspecificoServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT CONTACTB AS CodigoConta,TIPOCONTA AS TipoConta,CLASSIFCONTA AS Classificacao,DESCRCONTA AS DescricaoConta FROM PLANOESPEC";
        }
    }
}
