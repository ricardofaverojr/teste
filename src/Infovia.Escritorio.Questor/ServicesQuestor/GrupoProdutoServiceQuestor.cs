﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class GrupoProdutoServiceQuestor : ServiceQuestor<GrupoProdutoQuestor>, IGrupoProdutoServiceQuestor
    {
        public GrupoProdutoServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT CodigoGrupoProduto, DESCRGRUPOPRODUTO AS DescricaoGrupoProduto FROM GRUPOPRODUTO";
        }
    }
}
