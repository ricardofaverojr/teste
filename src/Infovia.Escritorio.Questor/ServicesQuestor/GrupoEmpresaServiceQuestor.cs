﻿using System;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class GrupoEmpresaServiceQuestor : ServiceQuestor<GrupoEmpresaQuestor>, IGrupoEmpresaServiceQuestor
    {
        public GrupoEmpresaServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT LGU.CodigoEmpresa, LGU.CodigoEstab, E.NomeEmpresa, LG.ID AS CodigoGrupo, LG.NOME AS NomeGrupo, LD.ID AS CodigoDepartamento, LD.DESCRDPTOESCRIT AS DescricaoDepartamento FROM LEANGRUPOUSUESTAB LGU INNER JOIN EMPRESA E ON LGU.CODIGOEMPRESA = E.CODIGOEMPRESA INNER JOIN GRUPOUSUARIO GU ON GU.CODIGOGRUPOUSUARIO = LGU.CODIGOGRUPOUSUARIO INNER JOIN LEANGRUPOS LG ON REPLACE(REPLACE(REPLACE(GU.DESCRGRUPOUSUARIO,'Lean - ',''),'Operador ',''),'Supervisor ','') = LG.NOME INNER JOIN LEANDEPARTAMENTOS LD ON LD.DESCRDPTOESCRIT = IIF(POSITION(' ' IN LG.NOME) > 0, SUBSTRING(LG.NOME FROM 1 FOR POSITION(' ' IN LG.NOME)), LG.NOME)";
        }
    }
}
