﻿using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.Questor.ServicesQuestor.Core;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;

namespace Infovia.Escritorio.Questor.ServicesQuestor
{
    public class EmpresaServiceQuestor : ServiceQuestor<EmpresaQuestor>, IEmpresaServiceQuestor
    {
        public EmpresaServiceQuestor(IConnectionQuestor connection) : base(connection)
        {
            CommandSql = "SELECT CodigoEmpresa, NomeEmpresa FROM EMPRESA WHERE CODIGOEMPRESA IN (SELECT CODIGOEMPRESA FROM CLIENTE WHERE CODIGOCLIENTE IN (SELECT CODIGOCLIENTE FROM SERVICOFIXO WHERE COMPETFINALVALID IS NULL)) ORDER BY 1";
        }
    }
}
