﻿using System;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.ImpostosRetidos
{
    public class ImpostoRetidoMap : IEntityTypeConfiguration<ImpostoRetido>
    {
        public void Configure(EntityTypeBuilder<ImpostoRetido> builder)
        {
            builder.HasKey(ir => ir.Id);

            builder.ToTable("ImpostoRetido");
        }
    }
}
