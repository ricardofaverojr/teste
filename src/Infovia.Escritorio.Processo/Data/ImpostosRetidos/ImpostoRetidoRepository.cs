﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces;

namespace Infovia.Escritorio.Processo.Data.ImpostosRetidos
{
    public class ImpostoRetidoRepository : ProcessoRepository<ImpostoRetido>, IImpostoRetidoRepository
    {
        public ImpostoRetidoRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
