﻿using System;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.ImpostosRetidos
{
    public class ImpostoRetidoDapper : Dapper<ImpostoRetido>, IImpostoRetidoDapper
    {
        public ImpostoRetidoDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
