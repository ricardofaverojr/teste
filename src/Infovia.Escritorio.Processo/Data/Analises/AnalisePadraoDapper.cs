﻿using System;
using System.Collections.Generic;
using Dommel;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnalisePadraoDapper : Dapper<AnalisePadrao>, IAnalisePadraoDapper
    {
        public AnalisePadraoDapper(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<AnalisePadraoFormula> GetFormulaAll(Guid analiseId) => _cn.Select<AnalisePadraoFormula>(a => a.AnalisePadraoId == analiseId);

        public AnalisePadraoFormula GetFormulaById(Guid id) => _cn.FirstOrDefault<AnalisePadraoFormula>(a => a.Id == id);
    }
}
