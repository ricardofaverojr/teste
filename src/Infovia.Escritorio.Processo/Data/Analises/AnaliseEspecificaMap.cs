﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseEspecificaMap : IEntityTypeConfiguration<AnaliseEspecifica>
    {
        public void Configure(EntityTypeBuilder<AnaliseEspecifica> builder)
        {
            builder.HasKey(ae => ae.Id);

            builder.Property(ap => ap.Classificacao)
                .HasMaxLength(30)
                .HasColumnType("varchar(30)")
                .IsRequired();

            builder.Property(ap => ap.Descricao)
                .HasMaxLength(60).
                HasColumnType("varchar(60)")
                .IsRequired();

            builder.HasOne(ap => ap.AnalisePadrao)
                .WithMany(ap => ap.AnalisesEspecifica)
                .HasForeignKey(ap => ap.AnalisePadraoId)
                .IsRequired();

            builder.ToTable("AnaliseEspecifica");
        }
    }
}
