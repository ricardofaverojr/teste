﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseDapper : Dapper<Analise>, IAnaliseDapper
    {
        public AnaliseDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
