﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseEspecificaFormulaMap : IEntityTypeConfiguration<AnaliseEspecificaFormula>
    {
        public void Configure(EntityTypeBuilder<AnaliseEspecificaFormula> builder)
        {
            builder.HasKey(aef => aef.Id);

            builder.HasOne(l => l.AnaliseEspecifica)
                .WithMany(l => l.Formulas)
                .HasForeignKey(l => l.AnaliseEspecificaId)
                .IsRequired();

            builder.ToTable("AnaliseEspecificaFormula");
        }
    }
}
