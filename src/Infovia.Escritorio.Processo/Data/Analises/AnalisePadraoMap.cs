﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnalisePadraoMap : IEntityTypeConfiguration<AnalisePadrao>
    {
        public void Configure(EntityTypeBuilder<AnalisePadrao> builder)
        {
            builder.HasKey(ap => ap.Id);

            builder.Property(ap => ap.Classificacao)
                .HasMaxLength(30)
                .HasColumnType("varchar(30)")
                .IsRequired();

            builder.Property(ap => ap.Descricao)
                .HasMaxLength(60)
                .HasColumnType("varchar(60)")
                .IsRequired();

            builder.HasOne(ap => ap.Analise)
                .WithMany(ap => ap.AnalisesPadrao)
                .HasForeignKey(ap => ap.AnaliseId)
                .IsRequired();

            builder.ToTable("AnalisePadrao");
        }
    }
}
