﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseEspecificaRepository : ProcessoRepository<AnaliseEspecifica>, IAnaliseEspecificaRepository
    {        
        public AnaliseEspecificaRepository(ProcessoContext context) : base(context)
        {
        }

        public void AddFormula(AnaliseEspecificaFormula formula) => _context.AnaliseEspecificaFormula.Add(formula);

        public void DeleteFormula(Guid id) => _context.AnaliseEspecificaFormula.Remove(_context.AnaliseEspecificaFormula.Find(id));

        public IEnumerable<AnaliseEspecificaFormula> GetFormulaAll(Guid analiseId) => _context.AnaliseEspecificaFormula.AsNoTracking().Where(f => f.AnaliseEspecificaId == analiseId).ToList();

        public AnaliseEspecificaFormula GetFormulaById(Guid id) => _context.AnaliseEspecificaFormula.FirstOrDefault(f => f.Id == id);
    }
}
