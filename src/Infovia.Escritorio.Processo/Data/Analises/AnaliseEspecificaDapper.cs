﻿using System;
using System.Collections.Generic;
using Dommel;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseEspecificaDapper : Dapper<AnaliseEspecifica>, IAnaliseEspecificaDapper
    {
        public AnaliseEspecificaDapper(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<AnaliseEspecificaFormula> GetFormulaAll(Guid analiseId) => _cn.Select<AnaliseEspecificaFormula>(a => a.AnaliseEspecificaId == analiseId);

        public AnaliseEspecificaFormula GetFormulaById(Guid id) => _cn.FirstOrDefault<AnaliseEspecificaFormula>(a => a.Id == id);
    }
}
