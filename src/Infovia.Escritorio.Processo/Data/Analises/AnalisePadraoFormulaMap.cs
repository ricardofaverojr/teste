﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnalisePadraoFormulaMap : IEntityTypeConfiguration<AnalisePadraoFormula>
    {
        public void Configure(EntityTypeBuilder<AnalisePadraoFormula> builder)
        {
            builder.HasKey(apf => apf.Id);

            builder.HasOne(l => l.AnalisePadrao).WithMany(l => l.Formulas).HasForeignKey(l => l.AnalisePadraoId).IsRequired();

            builder.ToTable("AnalisePadraoFormula");
        }
    }
}
