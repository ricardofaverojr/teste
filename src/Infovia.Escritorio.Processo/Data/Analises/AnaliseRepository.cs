﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseRepository : ProcessoRepository<Analise>, IAnaliseRepository
    {
        public AnaliseRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
