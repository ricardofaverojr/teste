﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnalisePadraoRepository : ProcessoRepository<AnalisePadrao>, IAnalisePadraoRepository
    {
        public AnalisePadraoRepository(ProcessoContext context) : base(context)
        {
        }

        public void AddFormula(AnalisePadraoFormula formula) => _context.AnalisePadraoFormula.Add(formula);

        public void DeleteFormula(Guid id) => _context.AnaliseEspecificaFormula.Remove(_context.AnaliseEspecificaFormula.Find(id));

        public IEnumerable<AnalisePadraoFormula> GetFormulaAll(Guid analiseId) => _context.AnalisePadraoFormula.AsNoTracking().Where(f => f.AnalisePadraoId == analiseId).ToList();

        public AnalisePadraoFormula GetFormulaById(Guid id) => _context.AnalisePadraoFormula.FirstOrDefault(f => f.Id == id);
    }
}
