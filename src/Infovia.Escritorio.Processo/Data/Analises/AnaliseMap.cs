﻿using System;
using Infovia.Escritorio.Processo.Domain.Analises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.Analises
{
    public class AnaliseMap : IEntityTypeConfiguration<Analise>
    {
        public void Configure(EntityTypeBuilder<Analise> builder)
        {
            builder.HasKey(a => a.Id);

            builder.Property(a => a.Descricao)
                .HasMaxLength(80)
                .HasColumnType("varchar(80)")
                .IsRequired();

            builder.ToTable("Analise");
        }
    }
}
