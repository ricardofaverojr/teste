﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infovia.Escritorio.Processo.Data.Context.Migrations
{
    public partial class ProcessoMigrationV1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Analise",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Descricao = table.Column<string>(type: "varchar(100)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Analise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HistoricoReferencia",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    CodigoHistorico = table.Column<int>(nullable: false),
                    DescrHistorico = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    CodigoReferencia = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricoReferencia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImpostoRetido",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CodigoImpostoIrrf = table.Column<int>(nullable: false),
                    VariacaoImpostoIrrf = table.Column<int>(nullable: false),
                    DataIrrf = table.Column<int>(nullable: false),
                    CodigoCfopIrrf = table.Column<int>(nullable: false),
                    CodigoImpostoPisCofinsCsll = table.Column<int>(nullable: false),
                    VariacaoImpostoPisCofinsCsll = table.Column<int>(nullable: false),
                    DataPisCofinsCsll = table.Column<int>(nullable: false),
                    CodigoCfopPisCofinsCsll = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImpostoRetido", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LeiauteContabil",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DescricaoLeiaute = table.Column<string>(type: "varchar(100)", nullable: true),
                    TipoLeiaute = table.Column<int>(nullable: false),
                    PosicaoDebito = table.Column<int>(nullable: false),
                    PosicaoCredito = table.Column<int>(nullable: false),
                    PosicaoHistorico = table.Column<int>(nullable: true),
                    TamanhoDebito = table.Column<int>(nullable: true),
                    TamanhoCredito = table.Column<int>(nullable: true),
                    TamanhoHistorico = table.Column<int>(nullable: true),
                    RegistroConta = table.Column<int>(nullable: true),
                    TamanhoRegistro = table.Column<int>(nullable: true),
                    Separador = table.Column<string>(type: "varchar(1)", maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeiauteContabil", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanoReferencia",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    CodigoConta = table.Column<int>(nullable: false),
                    DescricaoConta = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    ContaReferencia = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanoReferencia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessoPersonalizado",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Departamento = table.Column<int>(nullable: false),
                    Empresas = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Parametros = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false),
                    NomeProcesso = table.Column<string>(type: "varchar(50)", maxLength: 20, nullable: false),
                    Resultado = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    Descricao = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessoPersonalizado", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TabelaContribuicao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Registro = table.Column<string>(type: "varchar(4)", maxLength: 4, nullable: false),
                    Item = table.Column<int>(nullable: false),
                    Cfop = table.Column<int>(nullable: false),
                    CstPis = table.Column<int>(nullable: false),
                    BaseCalculoPis = table.Column<int>(nullable: false),
                    AliquotaPis = table.Column<int>(nullable: false),
                    BaseCalculoQtdePis = table.Column<int>(nullable: false),
                    AliquotaQtdePis = table.Column<int>(nullable: false),
                    ValorPis = table.Column<int>(nullable: false),
                    CstCofins = table.Column<int>(nullable: false),
                    BaseCalculoCofins = table.Column<int>(nullable: false),
                    AliquotaCofins = table.Column<int>(nullable: false),
                    BaseCalculoQtdeCofins = table.Column<int>(nullable: false),
                    AliquotaQtdeCofins = table.Column<int>(nullable: false),
                    ValorCofins = table.Column<int>(nullable: false),
                    Totalizador = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TabelaContribuicao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TabelaFiscal",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Registro = table.Column<string>(type: "varchar(4)", maxLength: 4, nullable: false),
                    Cfop = table.Column<int>(nullable: false),
                    Cst = table.Column<int>(nullable: false),
                    AliqIcms = table.Column<int>(nullable: false),
                    ValorOperacao = table.Column<int>(nullable: false),
                    BaseCalculoIcms = table.Column<int>(nullable: false),
                    ValorIcms = table.Column<int>(nullable: false),
                    BaseCalculoIcmsSt = table.Column<int>(nullable: false),
                    ValorIcmsSt = table.Column<int>(nullable: false),
                    ValorIpi = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TabelaFiscal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AnalisePadrao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnaliseId = table.Column<Guid>(nullable: false),
                    TipoConta = table.Column<int>(nullable: false),
                    Classificacao = table.Column<string>(type: "varchar(100)", maxLength: 30, nullable: false),
                    Descricao = table.Column<string>(type: "varchar(100)", maxLength: 60, nullable: false),
                    Operador = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnalisePadrao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnalisePadrao_Analise_AnaliseId",
                        column: x => x.AnaliseId,
                        principalTable: "Analise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnaliseEspecifica",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnalisePadraoId = table.Column<Guid>(nullable: false),
                    TipoConta = table.Column<int>(nullable: false),
                    Classificacao = table.Column<string>(type: "varchar(100)", maxLength: 30, nullable: false),
                    Descricao = table.Column<string>(type: "varchar(100)", maxLength: 60, nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    Operador = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnaliseEspecifica", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnaliseEspecifica_AnalisePadrao_AnalisePadraoId",
                        column: x => x.AnalisePadraoId,
                        principalTable: "AnalisePadrao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnalisePadraoFormula",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnalisePadraoId = table.Column<Guid>(nullable: false),
                    Seq = table.Column<int>(nullable: false),
                    TipoConta = table.Column<int>(nullable: false),
                    ContaCtb = table.Column<int>(nullable: false),
                    ValorAnalise = table.Column<int>(nullable: false),
                    Operador = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnalisePadraoFormula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnalisePadraoFormula_AnalisePadrao_AnalisePadraoId",
                        column: x => x.AnalisePadraoId,
                        principalTable: "AnalisePadrao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnaliseEspecificaFormula",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnaliseEspecificaId = table.Column<Guid>(nullable: false),
                    Seq = table.Column<int>(nullable: false),
                    TipoConta = table.Column<int>(nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    ContaCtb = table.Column<int>(nullable: false),
                    ValorAnalise = table.Column<int>(nullable: false),
                    Operador = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnaliseEspecificaFormula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnaliseEspecificaFormula_AnaliseEspecifica_AnaliseEspecificaId",
                        column: x => x.AnaliseEspecificaId,
                        principalTable: "AnaliseEspecifica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnaliseEspecifica_AnalisePadraoId",
                table: "AnaliseEspecifica",
                column: "AnalisePadraoId");

            migrationBuilder.CreateIndex(
                name: "IX_AnaliseEspecificaFormula_AnaliseEspecificaId",
                table: "AnaliseEspecificaFormula",
                column: "AnaliseEspecificaId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalisePadrao_AnaliseId",
                table: "AnalisePadrao",
                column: "AnaliseId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalisePadraoFormula_AnalisePadraoId",
                table: "AnalisePadraoFormula",
                column: "AnalisePadraoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnaliseEspecificaFormula");

            migrationBuilder.DropTable(
                name: "AnalisePadraoFormula");

            migrationBuilder.DropTable(
                name: "HistoricoReferencia");

            migrationBuilder.DropTable(
                name: "ImpostoRetido");

            migrationBuilder.DropTable(
                name: "LeiauteContabil");

            migrationBuilder.DropTable(
                name: "PlanoReferencia");

            migrationBuilder.DropTable(
                name: "ProcessoPersonalizado");

            migrationBuilder.DropTable(
                name: "TabelaContribuicao");

            migrationBuilder.DropTable(
                name: "TabelaFiscal");

            migrationBuilder.DropTable(
                name: "AnaliseEspecifica");

            migrationBuilder.DropTable(
                name: "AnalisePadrao");

            migrationBuilder.DropTable(
                name: "Analise");
        }
    }
}
