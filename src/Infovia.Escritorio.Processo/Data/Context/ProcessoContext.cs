﻿using System;
using System.Linq;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.Processo.Data.Context
{
    public class ProcessoContext : DbContext, IUnitOfWork
    {
        public ProcessoContext(DbContextOptions<ProcessoContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
            ChangeTracker.LazyLoadingEnabled = true;
        }

        public DbSet<HistoricoReferencia> HistoricoReferencia { get; set; }
        public DbSet<ImpostoRetido> ImpostoRetido { get; set; }
        public DbSet<LeiauteContabil> LeiauteContabil { get; set; }
        public DbSet<PlanoReferencia> PlanoReferencia { get; set; }
        public DbSet<TabelaContribuicao> TabelaContribuicao { get; set; }
        public DbSet<TabelaFiscal> TabelaFiscal { get; set; }
        public DbSet<ProcessoPersonalizado> ProcessoPersonalizado { get; set; }
        public DbSet<Analise> Analise { get; set; }
        public DbSet<AnalisePadrao> AnalisePadrao { get; set; }
        public DbSet<AnaliseEspecifica> AnaliseEspec { get; set; }
        public DbSet<AnalisePadraoFormula> AnalisePadraoFormula { get; set; }
        public DbSet<AnaliseEspecificaFormula> AnaliseEspecificaFormula { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(100)");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProcessoContext).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.Ignore<Event>();

            base.OnModelCreating(modelBuilder);
        }

        public bool Commit()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;

                if (entry.State == EntityState.Modified)
                    entry.Property("DataCadastro").IsModified = false;
            }
            return base.SaveChanges() > 0;
        }
    }
}
