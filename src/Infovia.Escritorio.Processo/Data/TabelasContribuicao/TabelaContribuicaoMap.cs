﻿using System;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.TabelasContribuicao
{
    public class TabelaContribuicaoMap : IEntityTypeConfiguration<TabelaContribuicao>
    {
        public void Configure(EntityTypeBuilder<TabelaContribuicao> builder)
        {
            builder.HasKey(tc => tc.Id);

            builder.Property(tc => tc.Registro)
                .HasColumnType("varchar(4)")
                .HasMaxLength(4)
                .IsRequired();

            builder.ToTable("TabelaContribuicao");
        }
    }
}
