﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces;

namespace Infovia.Escritorio.Processo.Data.TabelasContribuicao
{
    public class TabelaContribuicaoRepository : ProcessoRepository<TabelaContribuicao>, ITabelaContribuicaoRepository
    {
        public TabelaContribuicaoRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
