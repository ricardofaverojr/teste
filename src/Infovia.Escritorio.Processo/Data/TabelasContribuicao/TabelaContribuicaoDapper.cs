﻿using System;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.TabelasContribuicao
{
    public class TabelaContribuicaoDapper : Dapper<TabelaContribuicao>, ITabelaContribuicaoDapper
    {
        public TabelaContribuicaoDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
