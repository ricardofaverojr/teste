﻿using System;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.LeiautesContabeis
{
    public class LeiauteContabilMap : IEntityTypeConfiguration<LeiauteContabil>
    {
        public void Configure(EntityTypeBuilder<LeiauteContabil> builder)
        {
            builder.HasKey(lc => lc.Id);

            builder.Property(lc => lc.Separador)
                .HasColumnType("varchar(1)")
                .HasMaxLength(1)
                .IsRequired();

            builder.ToTable("LeiauteContabil");
        }
    }
}
