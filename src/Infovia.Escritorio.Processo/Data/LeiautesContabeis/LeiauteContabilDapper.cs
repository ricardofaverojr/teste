﻿using System;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.LeiautesContabeis
{
    public class LeiauteContabilDapper : Dapper<LeiauteContabil>, ILeiauteContabilDapper
    {
        public LeiauteContabilDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
