﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces;

namespace Infovia.Escritorio.Processo.Data.LeiautesContabeis
{
    public class LeiauteContabilRepository : ProcessoRepository<LeiauteContabil>, ILeiauteContabilRepository
    {
        public LeiauteContabilRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
