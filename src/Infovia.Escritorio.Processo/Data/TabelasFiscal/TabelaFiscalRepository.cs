﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces;

namespace Infovia.Escritorio.Processo.Data.TabelasFiscal
{
    public class TabelaFiscalRepository : ProcessoRepository<TabelaFiscal>, ITabelaFiscalRepository
    {
        public TabelaFiscalRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
