﻿using System;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.TabelasFiscal
{
    public class TabelaFiscalDapper : Dapper<TabelaFiscal>, ITabelaFiscalDapper
    {
        public TabelaFiscalDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
