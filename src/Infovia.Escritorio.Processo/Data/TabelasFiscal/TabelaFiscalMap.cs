﻿using System;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.TabelasFiscal
{
    public class TabelaFiscalMap : IEntityTypeConfiguration<TabelaFiscal>
    {
        public void Configure(EntityTypeBuilder<TabelaFiscal> builder)
        {
            builder.HasKey(tf => tf.Id);

            builder.Property(tf => tf.Registro)
                .HasColumnType("varchar(4)")
                .HasMaxLength(4)
                .IsRequired(); ;

            builder.ToTable("TabelaFiscal");
        }
    }
}
