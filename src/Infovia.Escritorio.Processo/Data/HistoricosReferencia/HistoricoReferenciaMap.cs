﻿using System;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.HistoricosReferencia
{
    public class HistoricoReferenciaMap : IEntityTypeConfiguration<HistoricoReferencia>
    {
        public void Configure(EntityTypeBuilder<HistoricoReferencia> builder)
        {
            builder.HasKey(hr => hr.Id);

            builder.Property(hr => hr.DescrHistorico)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(hr => hr.CodigoReferencia)
                .HasColumnType("varchar(10)")
                .HasMaxLength(10)
                .IsRequired();

            builder.ToTable("HistoricoReferencia");
        }
    }
}
