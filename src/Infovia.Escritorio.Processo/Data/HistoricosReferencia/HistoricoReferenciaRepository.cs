﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces;

namespace Infovia.Escritorio.Processo.Data.HistoricosReferencia
{
    public class HistoricoReferenciaRepository : ProcessoRepository<HistoricoReferencia>, IHistoricoReferenciaRepository
    {
        public HistoricoReferenciaRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
