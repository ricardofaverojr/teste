﻿using System;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.HistoricosReferencia
{
    public class HistoricoReferenciaDapper : Dapper<HistoricoReferencia>, IHistoricoReferenciaDapper
    {
        public HistoricoReferenciaDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
