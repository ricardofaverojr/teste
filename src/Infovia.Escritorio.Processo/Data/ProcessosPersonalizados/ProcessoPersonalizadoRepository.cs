﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces;

namespace Infovia.Escritorio.Processo.Data.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoRepository : ProcessoRepository<ProcessoPersonalizado>, IProcessoPersonalizadoRepository
    {
        public ProcessoPersonalizadoRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
