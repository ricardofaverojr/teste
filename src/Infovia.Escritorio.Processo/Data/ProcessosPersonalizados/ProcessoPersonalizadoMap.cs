﻿using System;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoMap : IEntityTypeConfiguration<ProcessoPersonalizado>
    {
        public void Configure(EntityTypeBuilder<ProcessoPersonalizado> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nome)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(p => p.Empresas)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50);

            builder.Property(p => p.Parametros)
                .HasColumnType("varchar(150)")
                .HasMaxLength(150)
                .IsRequired();

            builder.Property(p => p.Resultado)
                .HasColumnType("varchar(60)")
                .HasMaxLength(60);

            builder.Property(p => p.NomeProcesso)
                .HasColumnType("varchar(50)")
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(p => p.Descricao)
                .HasColumnType("varchar(300)")
                .HasMaxLength(300)
                .IsRequired();

            builder.ToTable("ProcessoPersonalizado");
        }
    }
}
