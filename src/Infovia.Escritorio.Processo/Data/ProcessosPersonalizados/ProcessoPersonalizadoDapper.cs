﻿using System;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoDapper : Dapper<ProcessoPersonalizado>, IProcessoPersonalizadoDapper
    {
        public ProcessoPersonalizadoDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
