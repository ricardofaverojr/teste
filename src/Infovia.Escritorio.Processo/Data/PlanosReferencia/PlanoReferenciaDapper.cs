﻿using System;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.Processo.Data.PlanosReferencia
{
    public class PlanoReferenciaDapper : Dapper<PlanoReferencia>, IPlanoReferenciaDapper
    {
        public PlanoReferenciaDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
