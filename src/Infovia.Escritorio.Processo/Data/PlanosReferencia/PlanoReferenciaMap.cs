﻿using System;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.Processo.Data.PlanosReferencia
{
    public class PlanoReferenciaMap : IEntityTypeConfiguration<PlanoReferencia>
    {
        public void Configure(EntityTypeBuilder<PlanoReferencia> builder)
        {
            builder.HasKey(pr => pr.Id);

            builder.Property(pr => pr.DescricaoConta)
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();

            builder.Property(pr => pr.ContaReferencia)
                .HasColumnType("varchar(10)")
                .HasMaxLength(10)
                .IsRequired();

            builder.ToTable("PlanoReferencia");
        }
    }
}
