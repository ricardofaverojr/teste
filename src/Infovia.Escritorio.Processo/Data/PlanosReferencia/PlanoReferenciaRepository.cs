﻿using System;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces;

namespace Infovia.Escritorio.Processo.Data.PlanosReferencia
{
    public class PlanoReferenciaRepository : ProcessoRepository<PlanoReferencia>, IPlanoReferenciaRepository
    {
        public PlanoReferenciaRepository(ProcessoContext context) : base(context)
        {
        }
    }
}
