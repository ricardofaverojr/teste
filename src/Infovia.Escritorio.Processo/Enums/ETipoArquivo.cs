﻿using System;
namespace Infovia.Escritorio.Processo.Enums
{
    public enum ETipoArquivo
    {
        Fiscal, Contribuição, Contábil, FCont, Ecf, Inválido, Contabilização, ExtratoNfe, ExtratoCte
    }
}
