﻿using System;
namespace Infovia.Escritorio.Processo.Enums
{
    public enum EAcaoArquivo
    {
        Totalizar, CompararSped, PesquisarNotas, ConverterPlano, CompararSefaz, CompararSefazCte, ImportarCte, AjustarComplObs, ContaClienteFornecedor, AjustarProdutosSped
    }
}
