﻿using System;
namespace Infovia.Escritorio.Processo.Enums
{
    public enum EDataRetido
    {
        Vencimento, Emissão
    }
}
