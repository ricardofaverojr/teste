﻿using System;
namespace Infovia.Escritorio.Processo.Enums
{
    public enum EOperador
    {
        Somar, Subtrair, Totalizar
    }
}
