﻿using System;
namespace Infovia.Escritorio.Processo.Enums
{
    public enum ESpedContribuicao
    {
        LinhaUnica, LinhaItem, LinhaPis, LinhaCofins
    }
}
