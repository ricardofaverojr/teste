﻿using System;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.TabelasContribuicao
{
    public class TabelaContribuicaoViewModel : ViewModel
    {
        public string Registro { get; set; }
        public int Item { get; set; }
        public int Cfop { get; set; }
        public int CstPis { get; set; }
        public int BaseCalculoPis { get; set; }
        public int AliquotaPis { get; set; }
        public int BaseCalculoQtdePis { get; set; }
        public int AliquotaQtdePis { get; set; }
        public int ValorPis { get; set; }
        public int CstCofins { get; set; }
        public int BaseCalculoCofins { get; set; }
        public int AliquotaCofins { get; set; }
        public int BaseCalculoQtdeCofins { get; set; }
        public int AliquotaQtdeCofins { get; set; }
        public int ValorCofins { get; set; }
        public ESpedContribuicao Totalizador { get; set; }
    }
}
