﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.TabelasContribuicao.Interfaces
{
    public interface ITabelaContribuicaoAppService : IAppService<TabelaContribuicaoViewModel>
    {
    }
}
