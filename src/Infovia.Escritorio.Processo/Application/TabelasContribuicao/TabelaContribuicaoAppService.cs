﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.TabelasContribuicao.Interfaces;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces;

namespace Infovia.Escritorio.Processo.Application.TabelasContribuicao
{
    public class TabelaContribuicaoAppService : ITabelaContribuicaoAppService
    {
        private readonly ITabelaContribuicaoService _service;
        private readonly ITabelaContribuicaoRepository _repository;
        private readonly IMapper _mapper;

        public TabelaContribuicaoAppService(ITabelaContribuicaoService service, ITabelaContribuicaoRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(TabelaContribuicaoViewModel entity) => _service.Add(_mapper.Map<TabelaContribuicao>(entity));

        public bool Update(TabelaContribuicaoViewModel entity) => _service.Update(_mapper.Map<TabelaContribuicao>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<TabelaContribuicaoViewModel> GetAll() => _mapper.Map<IEnumerable<TabelaContribuicaoViewModel>>(_repository.GetAll());

        public TabelaContribuicaoViewModel GetById(Guid id) => _mapper.Map<TabelaContribuicaoViewModel>(_repository.GetById(id));

        public IEnumerable<TabelaContribuicaoViewModel> GetByPredicate(Expression<Func<TabelaContribuicaoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<TabelaContribuicaoViewModel, bool>>, Expression<Func<TabelaContribuicao, bool>>>(predicate);
            return _mapper.Map<IEnumerable<TabelaContribuicaoViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
