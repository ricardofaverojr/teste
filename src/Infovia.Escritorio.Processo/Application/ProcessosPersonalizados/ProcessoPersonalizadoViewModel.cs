﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(100, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Nome { get; set; }
        public int Departamento { get; set; }
        [StringLength(50, ErrorMessage = "Campo {0} deve ter no máximo {1}")]
        public string Empresas { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(150, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Parametros { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(50, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string NomeProcesso { get; set; }
        [StringLength(60, ErrorMessage = "Campo {0} deve ter no máximo {1}")]
        public string Resultado { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(300, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Descricao { get; set; }
        public bool Ativo { get; set; }

        public override string ToString() => Nome;
    }
}
