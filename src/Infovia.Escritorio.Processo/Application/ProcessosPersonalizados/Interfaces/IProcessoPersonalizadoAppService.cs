﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.ProcessosPersonalizados.Interfaces
{
    public interface IProcessoPersonalizadoAppService : IAppService<ProcessoPersonalizadoViewModel>
    {
    }
}
