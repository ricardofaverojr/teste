﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados.Interfaces;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces;

namespace Infovia.Escritorio.Processo.Application.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoAppService : IProcessoPersonalizadoAppService
    {
        private readonly IProcessoPersonalizadoService _service;
        private readonly IProcessoPersonalizadoRepository _repository;
        private readonly IMapper _mapper;

        public ProcessoPersonalizadoAppService(IProcessoPersonalizadoService service, IProcessoPersonalizadoRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(ProcessoPersonalizadoViewModel entity) => _service.Add(_mapper.Map<ProcessoPersonalizado>(entity));

        public bool Update(ProcessoPersonalizadoViewModel entity) => _service.Update(_mapper.Map<ProcessoPersonalizado>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<ProcessoPersonalizadoViewModel> GetAll() => _mapper.Map<IEnumerable<ProcessoPersonalizadoViewModel>>(_repository.GetAll());

        public ProcessoPersonalizadoViewModel GetById(Guid id) => _mapper.Map<ProcessoPersonalizadoViewModel>(_repository.GetById(id));

        public IEnumerable<ProcessoPersonalizadoViewModel> GetByPredicate(Expression<Func<ProcessoPersonalizadoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<ProcessoPersonalizadoViewModel, bool>>, Expression<Func<ProcessoPersonalizado, bool>>>(predicate);
            return _mapper.Map<IEnumerable<ProcessoPersonalizadoViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
