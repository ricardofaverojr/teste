﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnaliseEspecificaViewModel : ViewModel
    {
        public Guid AnalisePadraoId { get; set; }
        public EConta TipoConta { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(30, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Classificacao { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(60, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Descricao { get; set; }
        public int CodigoEmpresa { get; set; }
        public EOperador Operador { get; set; }

        public override string ToString() => $"{Classificacao} - {Descricao}";
    }
}
