﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.Analises.Interfaces;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnaliseAppService : IAnaliseAppService
    {
        private readonly IAnaliseService _service;
        private readonly IAnaliseRepository _repository;
        private readonly IMapper _mapper;

        public AnaliseAppService(IAnaliseService service, IAnaliseRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(AnaliseViewModel entity) => _service.Add(_mapper.Map<Analise>(entity));

        public bool Update(AnaliseViewModel entity) => _service.Update(_mapper.Map<Analise>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<AnaliseViewModel> GetAll() => _mapper.Map<IEnumerable<AnaliseViewModel>>(_repository.GetAll());

        public AnaliseViewModel GetById(Guid id) => _mapper.Map<AnaliseViewModel>(_repository.GetById(id));

        public IEnumerable<AnaliseViewModel> GetByPredicate(Expression<Func<AnaliseViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<AnaliseViewModel, bool>>, Expression<Func<Analise, bool>>>(predicate);
            return _mapper.Map<IEnumerable<AnaliseViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }
    }
}
