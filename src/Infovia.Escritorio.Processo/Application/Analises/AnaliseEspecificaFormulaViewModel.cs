﻿using System;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnaliseEspecificaFormulaViewModel : ViewModel
    {
        public Guid AnaliseEspecificaId { get; set; }
        public int Seq { get; set; }
        public EConta TipoConta { get; set; }
        public int CodigoEmpresa { get; set; }
        public int ContaCtb { get; set; }
        public EValorAnalise ValorAnalise { get; set; }
        public EOperador Operador { get; set; }
    }
}
