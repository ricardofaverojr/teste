﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnaliseViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(80, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Descricao { get; set; }

        public override string ToString() => Descricao;
    }
}
