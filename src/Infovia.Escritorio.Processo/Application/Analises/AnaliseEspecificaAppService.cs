﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.Analises.Interfaces;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnaliseEspecificaAppService : IAnaliseEspecificaAppService
    {
        private readonly IAnaliseEspecificaService _service;
        private readonly IAnaliseEspecificaRepository _repository;
        private readonly IMapper _mapper;

        public AnaliseEspecificaAppService(IAnaliseEspecificaService service, IAnaliseEspecificaRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(AnaliseEspecificaViewModel entity) => _service.Add(_mapper.Map<AnaliseEspecifica>(entity));

        public bool Update(AnaliseEspecificaViewModel entity) => _service.Update(_mapper.Map<AnaliseEspecifica>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<AnaliseEspecificaViewModel> GetAll() => _mapper.Map<IEnumerable<AnaliseEspecificaViewModel>>(_repository.GetAll());

        public AnaliseEspecificaViewModel GetById(Guid id) => _mapper.Map<AnaliseEspecificaViewModel>(_repository.GetById(id));

        public IEnumerable<AnaliseEspecificaViewModel> GetByPredicate(Expression<Func<AnaliseEspecificaViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<AnaliseEspecificaViewModel, bool>>, Expression<Func<AnaliseEspecifica, bool>>>(predicate);
            return _mapper.Map<IEnumerable<AnaliseEspecificaViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }

        public IEnumerable<AnaliseEspecificaFormulaViewModel> GetFormulaAll(Guid analiseId) => _mapper.Map<IEnumerable<AnaliseEspecificaFormulaViewModel>>(_repository.GetFormulaAll(analiseId));

        public AnaliseEspecificaFormulaViewModel GetFormulaById(Guid id) => _mapper.Map<AnaliseEspecificaFormulaViewModel>(_repository.GetFormulaById(id));

        public bool UpdateFormula(IEnumerable<AnaliseEspecificaFormulaViewModel> insertList, IEnumerable<Guid> deleteList) => _service.UpdateFormulas(_mapper.Map<IEnumerable<AnaliseEspecificaFormula>>(insertList), deleteList);
    }
}
