﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.Analises.Interfaces;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnalisePadraoAppService : IAnalisePadraoAppService
    {
        private readonly IAnalisePadraoService _service;
        private readonly IAnalisePadraoRepository _repository;
        private readonly IMapper _mapper;

        public AnalisePadraoAppService(IAnalisePadraoService service, IAnalisePadraoRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(AnalisePadraoViewModel entity) => _service.Add(_mapper.Map<AnalisePadrao>(entity));

        public bool Update(AnalisePadraoViewModel entity) => _service.Update(_mapper.Map<AnalisePadrao>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<AnalisePadraoViewModel> GetAll() => _mapper.Map<IEnumerable<AnalisePadraoViewModel>>(_repository.GetAll());

        public AnalisePadraoViewModel GetById(Guid id) => _mapper.Map<AnalisePadraoViewModel>(_repository.GetById(id));

        public IEnumerable<AnalisePadraoViewModel> GetByPredicate(Expression<Func<AnalisePadraoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<AnalisePadraoViewModel, bool>>, Expression<Func<AnalisePadrao, bool>>>(predicate);
            return _mapper.Map<IEnumerable<AnalisePadraoViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }

        public IEnumerable<AnalisePadraoFormulaViewModel> GetFormulaAll(Guid analiseId) => _mapper.Map<IEnumerable<AnalisePadraoFormulaViewModel>>(_repository.GetFormulaAll(analiseId));

        public AnalisePadraoFormulaViewModel GetFormulaById(Guid id) => _mapper.Map<AnalisePadraoFormulaViewModel>(_repository.GetFormulaById(id));

        public bool UpdateFormula(IEnumerable<AnalisePadraoFormulaViewModel> insertList, IEnumerable<Guid> deleteList) => _service.UpdateFormulas(_mapper.Map<IEnumerable<AnalisePadraoFormula>>(insertList), deleteList);
    }
}
