﻿using System;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.Analises
{
    public class AnalisePadraoFormulaViewModel : ViewModel
    {
        public Guid AnalisePadraoId { get; set; }
        public int Seq { get; set; }
        public EConta TipoConta { get; set; }
        public int ContaCtb { get; set; }
        public EValorAnalise ValorAnalise { get; set; }
        public EOperador Operador { get; set; }

        public override string ToString() => ContaCtb.ToString();
    }
}
