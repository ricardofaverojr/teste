﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises.Interfaces
{
    public interface IAnaliseEspecificaAppService : IAppService<AnaliseEspecificaViewModel>
    {
        IEnumerable<AnaliseEspecificaFormulaViewModel> GetFormulaAll(Guid analiseId);
        AnaliseEspecificaFormulaViewModel GetFormulaById(Guid id);
        bool UpdateFormula(IEnumerable<AnaliseEspecificaFormulaViewModel> insertList, IEnumerable<Guid> deleteList);
    }
}
