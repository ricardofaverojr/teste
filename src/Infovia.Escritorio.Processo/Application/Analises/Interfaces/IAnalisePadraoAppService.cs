﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises.Interfaces
{
    public interface IAnalisePadraoAppService : IAppService<AnalisePadraoViewModel>
    {
        IEnumerable<AnalisePadraoFormulaViewModel> GetFormulaAll(Guid analiseId);
        AnalisePadraoFormulaViewModel GetFormulaById(Guid id);
        bool UpdateFormula(IEnumerable<AnalisePadraoFormulaViewModel> insertList, IEnumerable<Guid> deleteList);

    }
}
