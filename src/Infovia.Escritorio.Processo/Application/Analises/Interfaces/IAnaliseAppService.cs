﻿using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.Analises.Interfaces
{
    public interface IAnaliseAppService : IAppService<AnaliseViewModel>
    {
    }
}
