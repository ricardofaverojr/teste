﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.PlanosReferencia
{
    public class PlanoReferenciaViewModel : ViewModel
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoConta { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(80, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string DescricaoConta { get; set; }
        public string ContaReferencia { get; set; }

        public override string ToString() => $"{CodigoConta} - {DescricaoConta} - {ContaReferencia}";
    }
}
