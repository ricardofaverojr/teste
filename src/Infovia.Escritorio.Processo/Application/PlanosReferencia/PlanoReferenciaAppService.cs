﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.PlanosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces;

namespace Infovia.Escritorio.Processo.Application.PlanosReferencia
{
    public class PlanoReferenciaAppService : IPlanoReferenciaAppService
    {
        private readonly IPlanoReferenciaService _service;
        private readonly IPlanoReferenciaRepository _repository;
        private readonly IMapper _mapper;

        public PlanoReferenciaAppService(IPlanoReferenciaService service, IPlanoReferenciaRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(PlanoReferenciaViewModel entity) => _service.Add(_mapper.Map<PlanoReferencia>(entity));

        public bool Update(PlanoReferenciaViewModel entity) => _service.Update(_mapper.Map<PlanoReferencia>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<PlanoReferenciaViewModel> GetAll() => _mapper.Map<IEnumerable<PlanoReferenciaViewModel>>(_repository.GetAll());

        public PlanoReferenciaViewModel GetById(Guid id) => _mapper.Map<PlanoReferenciaViewModel>(_repository.GetById(id));

        public IEnumerable<PlanoReferenciaViewModel> GetByPredicate(Expression<Func<PlanoReferenciaViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<PlanoReferenciaViewModel, bool>>, Expression<Func<PlanoReferencia, bool>>>(predicate);
            return _mapper.Map<IEnumerable<PlanoReferenciaViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
