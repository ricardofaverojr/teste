﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.PlanosReferencia.Interfaces
{
    public interface IPlanoReferenciaAppService : IAppService<PlanoReferenciaViewModel>
    {
    }
}
