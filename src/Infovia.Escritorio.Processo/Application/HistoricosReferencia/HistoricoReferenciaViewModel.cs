﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.HistoricosReferencia
{
    public class HistoricoReferenciaViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1, 9999, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int CodigoEmpresa { get; set; }
        public int CodigoHistorico { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(50, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string DescrHistorico { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(10, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 1)]
        public string CodigoReferencia { get; set; }

        public override string ToString() => $"{CodigoHistorico} - {DescrHistorico} - {CodigoReferencia}";
    }
}
