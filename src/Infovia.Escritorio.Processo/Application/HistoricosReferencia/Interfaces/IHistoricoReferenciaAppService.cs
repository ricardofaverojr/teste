﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.HistoricosReferencia.Interfaces
{
    public interface IHistoricoReferenciaAppService : IAppService<HistoricoReferenciaViewModel>
    {
    }
}
