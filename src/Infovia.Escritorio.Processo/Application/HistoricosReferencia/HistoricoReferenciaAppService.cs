﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces;

namespace Infovia.Escritorio.Processo.Application.HistoricosReferencia
{
    public class HistoricoReferenciaAppService : IHistoricoReferenciaAppService
    {
        private readonly IHistoricoReferenciaService _service;
        private readonly IHistoricoReferenciaRepository _repository;
        private readonly IMapper _mapper;

        public HistoricoReferenciaAppService(IHistoricoReferenciaService service, IHistoricoReferenciaRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(HistoricoReferenciaViewModel entity) => _service.Add(_mapper.Map<HistoricoReferencia>(entity));

        public bool Update(HistoricoReferenciaViewModel entity) => _service.Update(_mapper.Map<HistoricoReferencia>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<HistoricoReferenciaViewModel> GetAll() => _mapper.Map<IEnumerable<HistoricoReferenciaViewModel>>(_repository.GetAll());

        public HistoricoReferenciaViewModel GetById(Guid id) => _mapper.Map<HistoricoReferenciaViewModel>(_repository.GetById(id));

        public IEnumerable<HistoricoReferenciaViewModel> GetByPredicate(Expression<Func<HistoricoReferenciaViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<HistoricoReferenciaViewModel, bool>>, Expression<Func<HistoricoReferencia, bool>>>(predicate);
            return _mapper.Map<IEnumerable<HistoricoReferenciaViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
