﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.TabelasFiscal.Interfaces
{
    public interface ITabelaFiscalAppService : IAppService<TabelaFiscalViewModel>
    {
    }
}
