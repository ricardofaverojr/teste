﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.TabelasFiscal.Interfaces;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces;

namespace Infovia.Escritorio.Processo.Application.TabelasFiscal
{
    public class TabelaFiscalAppService : ITabelaFiscalAppService
    {
        private readonly ITabelaFiscalService _service;
        private readonly ITabelaFiscalRepository _repository;
        private readonly IMapper _mapper;

        public TabelaFiscalAppService(ITabelaFiscalService service, ITabelaFiscalRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(TabelaFiscalViewModel entity) => _service.Add(_mapper.Map<TabelaFiscal>(entity));

        public bool Update(TabelaFiscalViewModel entity) => _service.Update(_mapper.Map<TabelaFiscal>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<TabelaFiscalViewModel> GetAll() => _mapper.Map<IEnumerable<TabelaFiscalViewModel>>(_repository.GetAll());

        public TabelaFiscalViewModel GetById(Guid id) => _mapper.Map<TabelaFiscalViewModel>(_repository.GetById(id));

        public IEnumerable<TabelaFiscalViewModel> GetByPredicate(Expression<Func<TabelaFiscalViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<TabelaFiscalViewModel, bool>>, Expression<Func<TabelaFiscal, bool>>>(predicate);
            return _mapper.Map<IEnumerable<TabelaFiscalViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
