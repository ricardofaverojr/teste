﻿using System;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.TabelasFiscal
{
    public class TabelaFiscalViewModel : ViewModel
    {
        public string Registro { get; set; }
        public int Cfop { get; set; }
        public int Cst { get; set; }
        public int AliqIcms { get; set; }
        public int ValorOperacao { get; set; }
        public int BaseCalculoIcms { get; set; }
        public int ValorIcms { get; set; }
        public int BaseCalculoIcmsSt { get; set; }
        public int ValorIcmsSt { get; set; }
        public int ValorIpi { get; set; }
    }
}
