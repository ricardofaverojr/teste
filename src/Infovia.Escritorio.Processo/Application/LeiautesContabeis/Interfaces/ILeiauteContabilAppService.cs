﻿using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.LeiautesContabeis.Interfaces
{
    public interface ILeiauteContabilAppService : IAppService<LeiauteContabilViewModel>
    {
    }
}
