﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.LeiautesContabeis.Interfaces;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces;

namespace Infovia.Escritorio.Processo.Application.LeiautesContabeis
{
    public class LeiauteContabilAppService : ILeiauteContabilAppService
    {
        private readonly ILeiauteContabilService _service;
        private readonly ILeiauteContabilRepository _repository;
        private readonly IMapper _mapper;

        public LeiauteContabilAppService(ILeiauteContabilService service, ILeiauteContabilRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(LeiauteContabilViewModel entity) => _service.Add(_mapper.Map<LeiauteContabil>(entity));

        public bool Update(LeiauteContabilViewModel entity) => _service.Update(_mapper.Map<LeiauteContabil>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<LeiauteContabilViewModel> GetAll() => _mapper.Map<IEnumerable<LeiauteContabilViewModel>>(_repository.GetAll());

        public LeiauteContabilViewModel GetById(Guid id) => _mapper.Map<LeiauteContabilViewModel>(_repository.GetById(id));

        public IEnumerable<LeiauteContabilViewModel> GetByPredicate(Expression<Func<LeiauteContabilViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<LeiauteContabilViewModel, bool>>, Expression<Func<LeiauteContabil, bool>>>(predicate);
            return _mapper.Map<IEnumerable<LeiauteContabilViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
