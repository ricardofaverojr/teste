﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.LeiautesContabeis
{
    public class LeiauteContabilViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(80, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string DescricaoLeiaute { get; set; }
        public ELeiaute TipoLeiaute { get; set; }
        public int PosicaoDebito { get; set; }
        public int PosicaoCredito { get; set; }
        public int? PosicaoHistorico { get; set; }
        public int? TamanhoDebito { get; set; }
        public int? TamanhoCredito { get; set; }
        public int? TamanhoHistorico { get; set; }
        public int? RegistroConta { get; set; }
        public int? TamanhoRegistro { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(1, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 1)]
        public string Separador { get; set; }

        public override string ToString() => DescricaoLeiaute;
    }
}
