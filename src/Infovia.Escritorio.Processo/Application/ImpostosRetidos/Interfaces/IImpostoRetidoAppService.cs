﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.Processo.Application.ImpostosRetidos.Interfaces
{
    public interface IImpostoRetidoAppService : IAppService<ImpostoRetidoViewModel>
    {
    }
}
