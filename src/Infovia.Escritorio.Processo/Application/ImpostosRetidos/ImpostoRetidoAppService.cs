﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos.Interfaces;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces;

namespace Infovia.Escritorio.Processo.Application.ImpostosRetidos
{
    public class ImpostoRetidoAppService : IImpostoRetidoAppService
    {
        private readonly IImpostoRetidoService _service;
        private readonly IImpostoRetidoRepository _repository;
        private readonly IMapper _mapper;

        public ImpostoRetidoAppService(IImpostoRetidoService service, IImpostoRetidoRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(ImpostoRetidoViewModel entity) => _service.Add(_mapper.Map<ImpostoRetido>(entity));

        public bool Update(ImpostoRetidoViewModel entity) => _service.Update(_mapper.Map<ImpostoRetido>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<ImpostoRetidoViewModel> GetAll() => _mapper.Map<IEnumerable<ImpostoRetidoViewModel>>(_repository.GetAll());

        public ImpostoRetidoViewModel GetById(Guid id) => _mapper.Map<ImpostoRetidoViewModel>(_repository.GetById(id));

        public IEnumerable<ImpostoRetidoViewModel> GetByPredicate(Expression<Func<ImpostoRetidoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<ImpostoRetidoViewModel, bool>>, Expression<Func<ImpostoRetido, bool>>>(predicate);
            return _mapper.Map<IEnumerable<ImpostoRetidoViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
