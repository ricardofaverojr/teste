﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.Processo.Application.ImpostosRetidos
{
    public class ImpostoRetidoViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1, 9999, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int CodigoImpostoIrrf { get; set; }
        public int VariacaoImpostoIrrf { get; set; }
        public EDataRetido DataIrrf { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1000, 1999999, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int CodigoCfopIrrf { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1, 9999, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int CodigoImpostoPisCofinsCsll { get; set; }
        public int VariacaoImpostoPisCofinsCsll { get; set; }
        public EDataRetido DataPisCofinsCsll { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1000, 1999999, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int CodigoCfopPisCofinsCsll { get; set; }
    }
}
