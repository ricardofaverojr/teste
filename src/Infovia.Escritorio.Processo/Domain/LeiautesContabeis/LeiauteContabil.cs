﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.LeiautesContabeis
{
    public class LeiauteContabil : Entity
    {
        public string DescricaoLeiaute { get; set; }
        public ELeiaute TipoLeiaute { get; set; }
        public int PosicaoDebito { get; set; }
        public int PosicaoCredito { get; set; }
        public int? PosicaoHistorico { get; set; }
        public int? TamanhoDebito { get; set; }
        public int? TamanhoCredito { get; set; }
        public int? TamanhoHistorico { get; set; }
        public int? RegistroConta { get; set; }
        public int? TamanhoRegistro { get; set; }
        public string Separador { get; set; }

        public override string ToString() => DescricaoLeiaute;

        public override ValidationResult ValidateEntity() => new LeiauteContabilValidation().Validate(this);
    }

    public class LeiauteContabilValidation : AbstractValidator<LeiauteContabil>
    {
        public LeiauteContabilValidation()
        {
            RuleFor(lc => lc.DescricaoLeiaute).Length(2, 80).WithMessage("'{PropertyName}' inválido");
            RuleFor(lc => lc.Separador).Length(1, 1).WithMessage("'{PropertyName}' inválido");
        }
    }
}
