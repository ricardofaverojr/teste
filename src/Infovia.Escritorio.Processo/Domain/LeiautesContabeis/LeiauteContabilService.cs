﻿using Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.LeiautesContabeis
{
    public class LeiauteContabilService : Service<LeiauteContabil>, ILeiauteContabilService
    {
        public LeiauteContabilService(ILeiauteContabilRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
