﻿using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces
{
    public interface ILeiauteContabilDapper : IDapper<LeiauteContabil>
    {
    }
}
