﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces
{
    public interface ILeiauteContabilService : IService<LeiauteContabil>
    {
    }
}
