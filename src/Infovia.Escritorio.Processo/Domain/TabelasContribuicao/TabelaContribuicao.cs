﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.TabelasContribuicao
{
    public class TabelaContribuicao : Entity
    {
        public string Registro { get; set; }
        public int Item { get; set; }
        public int Cfop { get; set; }
        public int CstPis { get; set; }
        public int BaseCalculoPis { get; set; }
        public int AliquotaPis { get; set; }
        public int BaseCalculoQtdePis { get; set; }
        public int AliquotaQtdePis { get; set; }
        public int ValorPis { get; set; }
        public int CstCofins { get; set; }
        public int BaseCalculoCofins { get; set; }
        public int AliquotaCofins { get; set; }
        public int BaseCalculoQtdeCofins { get; set; }
        public int AliquotaQtdeCofins { get; set; }
        public int ValorCofins { get; set; }
        public ESpedContribuicao Totalizador { get; set; }

        public override string ToString() => Registro;

        public override ValidationResult ValidateEntity() => new TabelaContribuicaoValidation().Validate(this);        
    }

    public class TabelaContribuicaoValidation : AbstractValidator<TabelaContribuicao>
    {
        public TabelaContribuicaoValidation()
        {
            RuleFor(t => t.Registro).Length(4, 4).WithMessage("'{PropertyName}' inválido");
        }
    }
}
