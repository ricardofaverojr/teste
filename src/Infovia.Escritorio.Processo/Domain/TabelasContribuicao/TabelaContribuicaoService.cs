﻿using System;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasContribuicao
{
    public class TabelaContribuicaoService : Service<TabelaContribuicao>, ITabelaContribuicaoService
    {
        public TabelaContribuicaoService(ITabelaContribuicaoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
