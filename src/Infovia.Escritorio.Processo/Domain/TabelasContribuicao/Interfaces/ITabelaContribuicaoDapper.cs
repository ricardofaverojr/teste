﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces
{
    public interface ITabelaContribuicaoDapper : IDapper<TabelaContribuicao>
    {
    }
}
