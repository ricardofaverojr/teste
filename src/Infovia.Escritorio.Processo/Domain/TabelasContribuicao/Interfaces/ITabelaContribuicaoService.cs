﻿using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces
{
    public interface ITabelaContribuicaoService : IService<TabelaContribuicao>
    {
    }
}
