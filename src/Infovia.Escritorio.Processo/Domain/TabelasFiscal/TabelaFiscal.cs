﻿using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.TabelasFiscal
{
    public class TabelaFiscal : Entity
    {
        public string Registro { get; set; }
        public int Cfop { get; set; }
        public int Cst { get; set; }
        public int AliqIcms { get; set; }
        public int ValorOperacao { get; set; }
        public int BaseCalculoIcms { get; set; }
        public int ValorIcms { get; set; }
        public int BaseCalculoIcmsSt { get; set; }
        public int ValorIcmsSt { get; set; }
        public int ValorIpi { get; set; }

        public override string ToString() => Registro;

        public override ValidationResult ValidateEntity() => new TabelaFiscalValidation().Validate(this);
    }

    public class TabelaFiscalValidation : AbstractValidator<TabelaFiscal>
    {
        public TabelaFiscalValidation()
        {
            RuleFor(t => t.Registro).Length(4, 4).WithMessage("'{PropertyName}' inválido");
        }
    }
}