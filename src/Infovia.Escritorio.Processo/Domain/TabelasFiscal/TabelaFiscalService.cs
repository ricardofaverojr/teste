﻿using Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasFiscal
{
    public class TabelaFiscalService : Service<TabelaFiscal>, ITabelaFiscalService
    {
        public TabelaFiscalService(ITabelaFiscalRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
