﻿using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces
{
    public interface ITabelaFiscalService : IService<TabelaFiscal>
    {
    }
}
