﻿using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces
{
    public interface ITabelaFiscalDapper : IDapper<TabelaFiscal>
    {
    }
}
