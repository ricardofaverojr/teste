﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados
{
    public class ProcessoPersonalizado : Entity
    {
        public string Nome { get; set; }
        public int Departamento { get; set; }
        public string Empresas { get; set; }
        public string Parametros { get; set; }
        public string NomeProcesso { get; set; }
        public string Resultado { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }

        public override string ToString() => Nome;

        public override ValidationResult ValidateEntity() => new ProcessoPersonalizadoValidation().Validate(this);
    }

    public class ProcessoPersonalizadoValidation : AbstractValidator<ProcessoPersonalizado>
    {
        public ProcessoPersonalizadoValidation()
        {
            RuleFor(p => p.Nome).Length(2, 100).WithMessage("'{PropertyName}' inválido");
            RuleFor(p => p.Empresas).MaximumLength(50).WithMessage("'{PropertyName}' inválido");
            RuleFor(p => p.Parametros).Length(2, 150).WithMessage("'{PropertyName}' inválido");
            RuleFor(p => p.NomeProcesso).Length(2, 50).WithMessage("'{PropertyName}' inválido");
            RuleFor(p => p.Resultado).MaximumLength(60).WithMessage("'{PropertyName}' inválido");
            RuleFor(p => p.Descricao).Length(2, 300).WithMessage("'{PropertyName}' inválido");
        }
    }
}
