﻿using System;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados
{
    public class ProcessoPersonalizadoService : Service<ProcessoPersonalizado>, IProcessoPersonalizadoService
    {
        private readonly IProcessoPersonalizadoRepository _repository;

        public ProcessoPersonalizadoService(IProcessoPersonalizadoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
            _repository = repository;
        }

        public bool AtivarDesativar(Guid id)
        {
            var processo = _repository.GetById(id);
            processo.Ativo = !processo.Ativo;
            return Update(processo);
        }
    }
}
