﻿using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces
{
    public interface IProcessoPersonalizadoDapper : IDapper<ProcessoPersonalizado>
    {
    }
}
