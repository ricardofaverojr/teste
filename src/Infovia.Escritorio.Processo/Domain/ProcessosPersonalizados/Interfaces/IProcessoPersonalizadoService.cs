﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces
{
    public interface IProcessoPersonalizadoService : IService<ProcessoPersonalizado>
    {
        bool AtivarDesativar(Guid id);
    }
}
