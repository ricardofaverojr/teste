﻿using System;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.HistoricosReferencia
{
    public class HistoricoReferenciaService : Service<HistoricoReferencia>, IHistoricoReferenciaService
    {
        public HistoricoReferenciaService(IHistoricoReferenciaRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
