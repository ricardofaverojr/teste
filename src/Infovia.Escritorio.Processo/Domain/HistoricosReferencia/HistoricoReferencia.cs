﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.HistoricosReferencia
{
    public class HistoricoReferencia : Entity
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoHistorico { get; set; }
        public string DescrHistorico { get; set; }
        public string CodigoReferencia { get; set; }

        public override string ToString() => $"{CodigoHistorico} - {DescrHistorico} - {CodigoReferencia}";

        public override ValidationResult ValidateEntity() => new HistoricoReferenciaValidation().Validate(this);        
    }

    public class HistoricoReferenciaValidation : AbstractValidator<HistoricoReferencia>
    {
        public HistoricoReferenciaValidation()
        {
            RuleFor(hr => hr.CodigoEmpresa).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
            RuleFor(hr => hr.DescrHistorico).Length(2, 50).WithMessage("'{PropertyName}' inválido");
            RuleFor(hr => hr.CodigoReferencia).Length(1, 10).WithMessage("'{PropertyName}' inválido");
        }
    }
}
