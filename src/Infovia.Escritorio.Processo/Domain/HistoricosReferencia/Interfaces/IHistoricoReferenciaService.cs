﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces
{
    public interface IHistoricoReferenciaService : IService<HistoricoReferencia>
    {
    }
}
