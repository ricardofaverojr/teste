﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces
{
    public interface IHistoricoReferenciaDapper : IDapper<HistoricoReferencia>
    {
    }
}
