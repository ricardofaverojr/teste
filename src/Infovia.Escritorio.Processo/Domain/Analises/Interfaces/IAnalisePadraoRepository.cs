﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnalisePadraoRepository : IRepository<AnalisePadrao>
    {
        IEnumerable<AnalisePadraoFormula> GetFormulaAll(Guid analiseId);
        AnalisePadraoFormula GetFormulaById(Guid id);
        void AddFormula(AnalisePadraoFormula formula);
        void DeleteFormula(Guid id);
    }
}
