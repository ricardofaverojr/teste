﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnalisePadraoService : IService<AnalisePadrao>
    {
        bool UpdateFormulas(IEnumerable<AnalisePadraoFormula> insertList, IEnumerable<Guid> deleteList);
    }
}
