﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnaliseEspecificaDapper : IDapper<AnaliseEspecifica>
    {
        IEnumerable<AnaliseEspecificaFormula> GetFormulaAll(Guid analiseId);
        AnaliseEspecificaFormula GetFormulaById(Guid id);
    }
}
