﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnaliseEspecificaService : IService<AnaliseEspecifica>
    {
        bool UpdateFormulas(IEnumerable<AnaliseEspecificaFormula> insertList, IEnumerable<Guid> deleteList);
    }
}
