﻿using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnaliseService : IService<Analise>
    {
    }
}
