﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnaliseEspecificaRepository : IRepository<AnaliseEspecifica>
    {
        IEnumerable<AnaliseEspecificaFormula> GetFormulaAll(Guid analiseId);
        AnaliseEspecificaFormula GetFormulaById(Guid id);
        void AddFormula(AnaliseEspecificaFormula formula);
        void DeleteFormula(Guid id);
    }
}
