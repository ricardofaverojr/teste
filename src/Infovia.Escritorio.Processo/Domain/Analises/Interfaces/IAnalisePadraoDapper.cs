﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises.Interfaces
{
    public interface IAnalisePadraoDapper : IDapper<AnalisePadrao>
    {
        IEnumerable<AnalisePadraoFormula> GetFormulaAll(Guid analiseId);
        AnalisePadraoFormula GetFormulaById(Guid id);
    }
}
