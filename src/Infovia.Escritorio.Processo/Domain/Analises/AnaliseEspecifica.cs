﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnaliseEspecifica : Entity
    {
        public Guid AnalisePadraoId { get; set; }
        public EConta TipoConta { get; set; }
        public string Classificacao { get; set; }
        public string Descricao { get; set; }
        public int CodigoEmpresa { get; set; }
        public EOperador Operador { get; set; }

        public AnalisePadrao AnalisePadrao { get; set; }

        public ICollection<AnaliseEspecificaFormula> Formulas { get; set; }

        public override string ToString() => $"{Classificacao} - {Descricao}";

        public override ValidationResult ValidateEntity() => new AnaliseEspecificaValidation().Validate(this);
    }

    public class AnaliseEspecificaValidation : AbstractValidator<AnaliseEspecifica>
    {
        public AnaliseEspecificaValidation()
        {
            RuleFor(ap => ap.Classificacao).Length(2, 30).WithMessage("'{PropertyName}' inválido");
            RuleFor(ap => ap.Descricao).Length(2, 60).WithMessage("'{PropertyName}' inválido");
        }
    }
}
