﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnalisePadraoService : Service<AnalisePadrao>, IAnalisePadraoService
    {
        private readonly IAnalisePadraoRepository _repository;

        public AnalisePadraoService(IAnalisePadraoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
            _repository = repository;
        }

        public bool UpdateFormulas(IEnumerable<AnalisePadraoFormula> insertList, IEnumerable<Guid> deleteList)
        {
            try
            {
                foreach (var item in insertList)
                    _repository.AddFormula(item);

                foreach (var item in deleteList)
                    _repository.DeleteFormula(item);

                return _repository.UnitOfWork.Commit();
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
