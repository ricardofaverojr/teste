﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnaliseEspecificaFormula : Entity
    {
        public Guid AnaliseEspecificaId { get; set; }
        public int Seq { get; set; }
        public EConta TipoConta { get; set; }
        public int CodigoEmpresa { get; set; }
        public int ContaCtb { get; set; }
        public EValorAnalise ValorAnalise { get; set; }
        public EOperador Operador { get; set; }

        public AnaliseEspecifica AnaliseEspecifica { get; set; }

        public override string ToString() => ContaCtb.ToString();

        public override ValidationResult ValidateEntity() => new AnaliseEspecificaFormulaValidation().Validate(this);                
    }

    public class AnaliseEspecificaFormulaValidation : AbstractValidator<AnaliseEspecificaFormula>
    {
        public AnaliseEspecificaFormulaValidation()
        {
            RuleFor(a => a.CodigoEmpresa).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
        }
    }
}
