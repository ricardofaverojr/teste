﻿using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnaliseService : Service<Analise>, IAnaliseService
    {
        public AnaliseService(IAnaliseRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
