﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnalisePadrao : Entity
    {
        public Guid AnaliseId { get; set; }
        public EConta TipoConta { get; set; }
        public string Classificacao { get; set; }
        public string Descricao { get; set; }
        public EOperador Operador { get; set; }

        public Analise Analise { get; set; }

        public ICollection<AnalisePadraoFormula> Formulas { get; set; }
        public ICollection<AnaliseEspecifica> AnalisesEspecifica { get; set; }

        public override string ToString() => $"{Classificacao} - {Descricao}";

        public override ValidationResult ValidateEntity() => new AnalisePadraoValidator().Validate(this);        
    }

    public class AnalisePadraoValidator : AbstractValidator<AnalisePadrao>
    {
        public AnalisePadraoValidator()
        {
            RuleFor(ap => ap.Classificacao).Length(2, 30).WithMessage("'{PropertyName}' inválido");
            RuleFor(ap => ap.Descricao).Length(2, 60).WithMessage("'{PropertyName}' inválido");
        }
    }
}
