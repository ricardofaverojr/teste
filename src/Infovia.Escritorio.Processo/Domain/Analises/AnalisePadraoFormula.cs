﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class AnalisePadraoFormula : Entity
    {
        public Guid AnalisePadraoId { get; set; }
        public int Seq { get; set; }
        public EConta TipoConta { get; set; }
        public int ContaCtb { get; set; }
        public EValorAnalise ValorAnalise { get; set; }
        public EOperador Operador { get; set; }

        public AnalisePadrao AnalisePadrao { get; set; }

        public override string ToString() => ContaCtb.ToString();

        public override ValidationResult ValidateEntity() => new AnalisePadraoFormulaValidation().Validate(this);
    }

    public class AnalisePadraoFormulaValidation : AbstractValidator<AnalisePadraoFormula>
    {
        public AnalisePadraoFormulaValidation()
        {
        }
    }
}
