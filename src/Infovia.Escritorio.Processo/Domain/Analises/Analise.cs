﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.Analises
{
    public class Analise : Entity
    {
        public string Descricao { get; set; }

        public ICollection<AnalisePadrao> AnalisesPadrao { get; set; }

        public override string ToString() => Descricao;

        public override ValidationResult ValidateEntity() => new AnaliseValidation().Validate(this);
    }

    public class AnaliseValidation : AbstractValidator<Analise>
    {
        public AnaliseValidation()
        {
            RuleFor(a => a.Descricao).Length(2, 80).WithMessage("'{PropertyName}' inválido");
        }
    }
}
