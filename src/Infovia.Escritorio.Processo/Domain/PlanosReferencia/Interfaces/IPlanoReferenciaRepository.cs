﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces
{
    public interface IPlanoReferenciaRepository : IRepository<PlanoReferencia>
    {
    }
}
