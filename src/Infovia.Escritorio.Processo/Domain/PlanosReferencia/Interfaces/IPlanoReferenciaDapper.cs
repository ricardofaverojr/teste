﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces
{
    public interface IPlanoReferenciaDapper : IDapper<PlanoReferencia>
    {
    }
}
