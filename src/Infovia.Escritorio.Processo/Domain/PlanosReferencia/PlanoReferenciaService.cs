﻿using Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.PlanosReferencia
{
    public class PlanoReferenciaService : Service<PlanoReferencia>, IPlanoReferenciaService
    {
        public PlanoReferenciaService(IPlanoReferenciaRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
