﻿using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.PlanosReferencia
{
    public class PlanoReferencia : Entity
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoConta { get; set; }
        public string DescricaoConta { get; set; }
        public string ContaReferencia { get; set; }

        public override string ToString() => $"{CodigoConta} - {DescricaoConta} - {ContaReferencia}";

        public override ValidationResult ValidateEntity() => new PlanoReferenciaValidation().Validate(this);
    }

    public class PlanoReferenciaValidation : AbstractValidator<PlanoReferencia>
    {
        public PlanoReferenciaValidation()
        {
            RuleFor(pr => pr.DescricaoConta).Length(2, 80).WithMessage("'{PropertyName}' inválido");
            RuleFor(pr => pr.DescricaoConta).Length(2, 10).WithMessage("'{PropertyName}' inválido");
        }
    }
}
