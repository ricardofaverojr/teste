﻿using System;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ImpostosRetidos
{
    public class ImpostoRetidoService : Service<ImpostoRetido>, IImpostoRetidoService
    {
        public ImpostoRetidoService(IImpostoRetidoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
