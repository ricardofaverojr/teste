﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces
{
    public interface IImpostoRetidoDapper : IDapper<ImpostoRetido>
    {
    }
}
