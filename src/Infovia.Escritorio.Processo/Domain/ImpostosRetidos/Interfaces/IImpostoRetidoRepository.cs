﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces
{
    public interface IImpostoRetidoRepository : IRepository<ImpostoRetido>
    {
    }
}
