﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.Processo.Enums;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.Processo.Domain.ImpostosRetidos
{
    public class ImpostoRetido : Entity
    {
        public int CodigoImpostoIrrf { get; set; }
        public int VariacaoImpostoIrrf { get; set; }
        public EDataRetido DataIrrf { get; set; }
        public int CodigoCfopIrrf { get; set; }
        public int CodigoImpostoPisCofinsCsll { get; set; }
        public int VariacaoImpostoPisCofinsCsll { get; set; }
        public EDataRetido DataPisCofinsCsll { get; set; }
        public int CodigoCfopPisCofinsCsll { get; set; }

        public override ValidationResult ValidateEntity() => new ImpostoRetidoValidation().Validate(this);
    }

    public class ImpostoRetidoValidation : AbstractValidator<ImpostoRetido>
    {
        public ImpostoRetidoValidation()
        {
            RuleFor(ir => ir.CodigoImpostoIrrf).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
            RuleFor(ir => ir.CodigoImpostoPisCofinsCsll).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
            RuleFor(ir => ir.CodigoCfopIrrf).ExclusiveBetween(1000, 19999999).WithMessage("'{PropertyName}' inválido");
            RuleFor(ir => ir.CodigoCfopPisCofinsCsll).ExclusiveBetween(1000, 1999999).WithMessage("'{PropertyName}' inválido");
        }
    }
}
