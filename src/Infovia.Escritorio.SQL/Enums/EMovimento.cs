﻿using System;
namespace Infovia.Escritorio.SQL.Enums
{
    public enum EMovimento
    {
        Geral, Entrada, Saída, Apuração, Configuração
    }
}
