﻿using System;
namespace Infovia.Escritorio.SQL.Enums
{
    public enum EParametro
    {
        Date = 0,
        Text = 1,
        Number = 2,
        Estab = 3,
        Enum = 4,
        FileTexto = 5,
        FileXML = 6,
        FileExcel = 7,
        FileWord = 8,
        FilePDF = 9,
        CheckBox = 10,
        GrupoProduto = 11
    }
}
