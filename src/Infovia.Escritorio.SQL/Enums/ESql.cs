﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Infovia.Escritorio.SQL.Enums
{
    public enum ESql
    {
        [Display(Name = "Execução")]
        Execução,
        Consulta, Script,
        [Display(Name = "Rotina da Empresa")]
        RotinaEmpresa
    }
}
