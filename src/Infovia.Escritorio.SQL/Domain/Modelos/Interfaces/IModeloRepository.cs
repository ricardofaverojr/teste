﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Modelos.Interfaces
{
    public interface IModeloRepository : IRepository<Modelo>
    {
    }
}
