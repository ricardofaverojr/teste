﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Modelos.Interfaces
{
    public interface IModeloDapper : IDapper<Modelo>
    {
    }
}
