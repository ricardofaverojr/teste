﻿using Infovia.Escritorio.SQL.Domain.Modelos.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Modelos
{
    public class ModeloService : Service<Modelo>, IModeloService
    {
        public ModeloService(IModeloRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
