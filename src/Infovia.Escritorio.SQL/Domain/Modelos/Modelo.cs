﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.SQL.Domain.Modelos
{
    public class Modelo : Entity
    {
        public string Descricao { get; set; }
        public string Sql { get; set; }
        public string Parametro { get; set; }
        public string NomeArquivo { get; set; }
        public string TipoArquivo { get; set; }
        public long TamanhoArquivo { get; set; }
        public byte[] ConteudoArquivo { get; set; }

        public override string ToString() => Descricao;

        public override ValidationResult ValidateEntity() => new ModeloValidation().Validate(this);
    }

    public class ModeloValidation : AbstractValidator<Modelo>
    {
        public ModeloValidation()
        {
            RuleFor(c => c.Descricao).Length(2, 100).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Sql).Length(2, 8000).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Parametro).Length(2, 300).WithMessage("'{PropertyName}' inválido");
        }
    }
}
