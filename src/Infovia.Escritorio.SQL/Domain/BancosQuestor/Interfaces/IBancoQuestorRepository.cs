﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces
{
    public interface IBancoQuestorRepository : IRepository<BancoQuestor>
    {
        string GetConnectionString(Guid? id);
    }
}
