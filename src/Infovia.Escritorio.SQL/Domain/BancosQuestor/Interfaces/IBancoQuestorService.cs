﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces
{
    public interface IBancoQuestorService : IService<BancoQuestor>
    {
        void Padrao(Guid id);
    }
}
