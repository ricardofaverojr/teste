﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces
{
    public interface IBancoQuestorDapper : IDapper<BancoQuestor>
    {
    }
}
