﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.SQL.Domain.BancosQuestor
{
    public class BancoQuestor : Entity
    {
        public string Endereco { get; set; }
        public string Caminho { get; set; }
        public int Porta { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public bool Padrao { get; set; }

        public ICollection<Log> Logs { get; set; }

        public override string ToString() => $"User={Usuario};Password={Senha};Database={Caminho};DataSource={Endereco};Port={Porta};Dialect=3;Charset=ISO8859_1;Role=;Connection lifetime=15;Pooling=true;Packet Size=8192;ServerType=0";

        public override ValidationResult ValidateEntity() => new BancoQuestorValidation().Validate(this);
    }

    public class BancoQuestorValidation : AbstractValidator<BancoQuestor>
    {
        public BancoQuestorValidation()
        {
            RuleFor(bq => bq.Endereco).Length(2, 40).WithMessage("'{PropertyName}' inválido");
            RuleFor(bq => bq.Caminho).Length(2, 40).WithMessage("'{PropertyName}' inválido");
            RuleFor(bq => bq.Porta).ExclusiveBetween(1, 65535).WithMessage("{'{PropertyName}' inválido");
            RuleFor(bq => bq.Usuario).Length(2, 40).WithMessage("'{PropertyName}' inválido");
            RuleFor(bq => bq.Senha).Length(1, 20).WithMessage("'{PropertyName}' inválido");
        }
    }

}
