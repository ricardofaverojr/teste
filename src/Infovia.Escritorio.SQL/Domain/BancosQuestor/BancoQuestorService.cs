﻿using System;
using Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.BancosQuestor
{
    public class BancoQuestorService : Service<BancoQuestor>, IBancoQuestorService
    {
        private readonly IBancoQuestorRepository _repository;

        public BancoQuestorService(IBancoQuestorRepository repository, IMediatorHandler mediator) : base(repository, mediator) => _repository = repository;

        public void Padrao(Guid id)
        {
            foreach (var banco in _repository.GetAll())
            {
                banco.Padrao = banco.Id == id;
                Update(banco);
            }
        }
    }
}
