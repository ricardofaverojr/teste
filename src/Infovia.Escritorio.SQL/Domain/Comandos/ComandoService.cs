﻿using System;
using System.Collections.Generic;
using Infovia.Escritorio.SQL.Domain.Comandos.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Comandos
{
    public class ComandoService : Service<Comando>, IComandoService
    {
        private readonly IComandoRepository _repository;

        public ComandoService(IComandoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
            _repository = repository;
        }

        public bool AddLog(Log entity)
        {
            _repository.AddLog(entity);
            return _repository.UnitOfWork.Commit();
        }

        public bool AtivarDesativar(Guid id)
        {
            var comando = _repository.GetById(id);
            comando.Ativo = !comando.Ativo;
            return Update(comando);
        }

        public bool UpdateEmpresas(IEnumerable<ComandoEmpresa> insertList, IEnumerable<Guid> deleteList)
        {
            try
            {
                foreach (var item in insertList)
                    _repository.AddEmpresa(item);

                foreach (var item in deleteList)
                    _repository.DeleteEmpresa(item);

                return _repository.UnitOfWork.Commit();
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
