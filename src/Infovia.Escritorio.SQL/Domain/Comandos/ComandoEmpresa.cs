﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.SQL.Domain.Comandos
{
    public class ComandoEmpresa : Entity
    {
        public int CodigoEmpresa { get; set; }
        public Guid ComandoId { get; set; }
        public int OrdemExecucao { get; set; }

        public Comando Comando { get; private set; }

        public override ValidationResult ValidateEntity() => new ComandoEmpresaValidation().Validate(this);
    }

    public class ComandoEmpresaValidation : AbstractValidator<ComandoEmpresa>
    {
        public ComandoEmpresaValidation()
        {
            RuleFor(ce => ce.CodigoEmpresa).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
            RuleFor(ce => ce.ComandoId).NotEqual(Guid.Empty).WithMessage("'{PropertyName}' inválido");
            RuleFor(ce => ce.OrdemExecucao).ExclusiveBetween(1, 20).WithMessage("'{PropertyName}' inválido");
        }
    }
}
