﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.SQL.Domain.Comandos
{
    public class Log : Entity
    {
        public Guid ComandoId { get; set; }
        public int CodigoEmpresa { get; set; }
        public Guid BancoQuestorId { get; set; }
        public string Sql { get; set; }
        public string Resultado { get; set; }
        public string Parametros { get; set; }
        public int Departamento { get; set; }
        public DateTime DataCadastro { get; set; }
        public int CodigoUsuario { get; set; }

        public Comando Comando { get; set; }
        public BancoQuestor BancoQuestor { get; set; }

        public override string ToString() => $"{CodigoEmpresa} - {DataCadastro:D} - {Resultado}";

        public override ValidationResult ValidateEntity() => new LogValidation().Validate(this);
    }

    public class LogValidation : AbstractValidator<Log>
    {
        public LogValidation()
        {
            RuleFor(l => l.ComandoId).NotEqual(Guid.Empty).WithMessage("'{PropertyName}' inválido");
            RuleFor(l => l.CodigoEmpresa).ExclusiveBetween(1, 9999).WithMessage("'{PropertyName}' inválido");
            RuleFor(l => l.Sql).Length(2, 10000).WithMessage("'{PropertyName}' inválido");
            RuleFor(l => l.Resultado).Length(2, 200).WithMessage("'{PropertyName}' inválido");
            RuleFor(l => l.Parametros).Length(2, 300).WithMessage("'{PropertyName}' inválido");
            RuleFor(l => l.Departamento).ExclusiveBetween(0, 20).WithMessage("'{PropertyName}' inválido");
        }
    }

}
