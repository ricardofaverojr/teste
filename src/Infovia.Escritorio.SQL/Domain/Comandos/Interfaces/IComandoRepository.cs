﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Comandos.Interfaces
{
    public interface IComandoRepository : IRepository<Comando>
    {
        // Comandos Empresa
        void AddEmpresa(ComandoEmpresa entity);
        void DeleteEmpresa(Guid id);

        IEnumerable<ComandoEmpresa> GetEmpresaByPredicate(Expression<Func<ComandoEmpresa, bool>> predicate);

        // Logs
        void AddLog(Log entity);

        IEnumerable<Log> GetLogByPredicate(Expression<Func<Log, bool>> predicate);
        Log GetLogById(Guid id);
    }
}
