﻿using System;
using System.Collections.Generic;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Comandos.Interfaces
{
    public interface IComandoService : IService<Comando>
    {
        bool AtivarDesativar(Guid id);

        bool UpdateEmpresas(IEnumerable<ComandoEmpresa> insertList, IEnumerable<Guid> deleteList);

        bool AddLog(Log entity);
    }
}
