﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Comandos.Interfaces
{
    public interface IComandoDapper : IDapper<Comando>
    {
        // Comandos Empresa
        IEnumerable<ComandoEmpresa> GetEmpresaByComando(Guid comandoId);
        IEnumerable<ComandoEmpresa> GetEmpresaByCodEmpresa(int codigoEmpresa);

        // Logs
        IEnumerable<Log> GetLogByPredicate(Expression<Func<Log, bool>> predicate);
        Log GetLogById(Guid id);
    }
}
