﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using Infovia.Escritorio.SQL.Enums;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.SQL.Domain.Comandos
{
    public class Comando : Entity, IAggregateRoot
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public string Sql { get; set; }
        public string Parametro { get; set; }
        public ESql TipoSql { get; set; }
        public EMovimento TipoMovimento { get; set; }
        public int Departamento { get; set; }
        public bool Ativo { get; set; }

        public ICollection<ComandoEmpresa> Empresas { get; set; }
        public ICollection<Log> Logs { get; set; }

        public override string ToString() => $"{Codigo} - {Descricao}";

        public override ValidationResult ValidateEntity() => new ComandoValidation().Validate(this);
    }

    public class ComandoValidation : AbstractValidator<Comando>
    {
        public ComandoValidation()
        {
            RuleFor(c => c.Codigo).ExclusiveBetween(0, 5000).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Descricao).Length(2, 100).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Sql).Length(2, 8000).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Parametro).Length(2, 300).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.TipoSql).IsInEnum().WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.TipoMovimento).IsInEnum().WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Departamento).ExclusiveBetween(0, 20).WithMessage("'{PropertyName}' inválido");
        }
    }

}
