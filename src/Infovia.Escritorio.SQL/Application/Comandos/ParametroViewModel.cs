﻿using System.Collections.Generic;
using System.Linq;
using Infovia.Escritorio.SQL.Enums;

namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ParametroViewModel
    {
        public ParametroViewModel(string parametro, EParametro tipo)
        {
            Parametro = parametro;
            TipoParametro = tipo;
        }

        public string Parametro { get; set; }
        public EParametro TipoParametro { get; set; }
        public string Html { get; set; }

        public string HtmlParametro(List<ParametroAuxiliarViewModel> lista)
        {
            var html = "";
            switch (TipoParametro)
            {
                case EParametro.Date:
                    html = "<div class='form-group'>" +
                            $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                            "<div class='col-lg-9'>" +
                            "<div id='dp-txtinput'>" +
                            $"<input type = 'text' class='form-control' name='campoModal' placeholder='{Parametro}'/>" +
                            "</div>" +
                            "</div>" +
                            "</div>";
                    break;
                case EParametro.Text:
                    html = "<div class='form-group'>" +
                            $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                            "<div class='col-lg-9'>" +
                            $"<input type = 'text' class='form-control' name='campoModal' placeholder='{Parametro}' />" +
                            "</div>" +
                            "</div>";
                    break;
                case EParametro.Number:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<input type = 'number' class='form-control' name='campoModal' placeholder='{Parametro}'/>" +
                           "</div>" +
                           "</div>";
                    break;
                case EParametro.Estab:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<select class='form-control' name='campoModal' placeholder='{Parametro}' id='{Parametro}'>";

                    html += $"<option selected='selected' value=''>{Parametro}</option>";
                    html = lista.Aggregate(html, (current, item) => current + $"<option value='{item.Valor}'>{item.Nome}</option>");
                    html += "</select></div></div>";
                    break;
                case EParametro.Enum:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<select class='form-control' name='campoModal' placeholder='{Parametro}' id='{Parametro}'>";

                    html += $"<option selected='selected' value=''>{Parametro}</option>";
                    foreach (var item in lista)
                        html += $"<option value='{item.Valor}'>{item.Nome}</option>";
                    html += "</select></div></div>";
                    break;
                case EParametro.FileTexto:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<input type ='file' class='form-control' name='upload' placeholder='{Parametro}' accept='text/*'/>" +
                           "</div>" +
                           "</div>";
                    break;
                case EParametro.FileXML:
                    html = "<div class='form-group'>" +
                        $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}'>{Parametro}</label>" +
                        "<div class='col-lg-9'>" +
                        $"<input type ='file' class='form-control' name='upload' placeholder='{Parametro}' accept='application/xml'/>" +
                        "</div>" +
                        "</div>";
                    break;
                case EParametro.FileExcel:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<input type ='file' class='form-control' name='upload' placeholder='{Parametro}' accept='application/vnd.ms-excel'/>" +
                           "</div>" +
                           "</div>";
                    break;
                case EParametro.FileWord:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<input type ='file' class='form-control' name='upload' placeholder='{Parametro}' accept='application/msword'/>" +
                           "</div>" +
                           "</div>";
                    break;
                case EParametro.FilePDF:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<input type ='file' class='form-control' name='upload' placeholder='{Parametro}' accept='application/pdf'/>" +
                           "</div>" +
                           "</div>";
                    break;
                case EParametro.CheckBox:
                    html = "<div class='form-group'>" +
                           $"<label class='col-md-3 control-label' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                           "<div class='col-md-9'>" +
                           "<div class='checkbox'>";

                    foreach (var item in lista)
                        html += $"<input id='{item.Valor}' class='magic-checkbox' type='checkbox' name='campoModal' value='true'><label for='{item.Valor}'>{item.Nome}</label>";

                    html += "</div></div></div>";

                    break;
                case EParametro.GrupoProduto:
                    html = "<div class='form-group'>" +
                           $"<label class='col-lg-2 control-label text-semibold' id='{Parametro}' name='labelModal'>{Parametro}</label>" +
                           "<div class='col-lg-9'>" +
                           $"<select class='form-control' name='campoModal' placeholder='{Parametro}' id='{Parametro}'>";

                    html += $"<option selected='selected' value=''>{Parametro}</option>";
                    html = lista.Aggregate(html, (current, item) => current + $"<option value='{item.Valor}'>{item.Nome}</option>");
                    html += "</select></div></div>";
                    break;
            }

            return html;
        }
    }
}
