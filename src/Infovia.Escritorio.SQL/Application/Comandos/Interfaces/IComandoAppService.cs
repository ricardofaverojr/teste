﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.SQL.Application.Comandos.Interfaces
{
    public interface IComandoAppService : IAppService<ComandoViewModel>
    {
        bool AtivarDesativar(Guid id);

        bool UpdateEmpresa(IEnumerable<ComandoEmpresaViewModel> insertList, IEnumerable<Guid> deleteList);
        IEnumerable<ComandoEmpresaViewModel> GetEmpresaByComando(Guid comandoId);
        IEnumerable<ComandoEmpresaViewModel> GetEmpresaByCodEmpresa(int codigoEmpresa);

        bool AddLog(LogViewModel entity);
        IEnumerable<LogViewModel> GetLogByPredicate(Expression<Func<LogViewModel, bool>> predicate);
        LogViewModel GetLogById(Guid id);
    }
}
