﻿using System;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ComandoEmpresaViewModel : ViewModel
    {
        public int CodigoEmpresa { get; set; }
        public Guid ComandoId { get; set; }
        public int OrdemExecucao { get; set; }

        public string Descricao { get; set; }
        public string Sql { get; set; }
        public string Parametros { get; set; }

        public string NomeEmpresa { get; set; }
        public int CodigoComando { get; set; }
    }
}
