﻿using System;
namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ParametroValorViewModel
    {
        public int CodigoSql { get; set; }
        public string ValorParametros { get; set; }
    }
}
