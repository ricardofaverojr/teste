﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.Escritorio.SQL.Enums;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ComandoViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório")]
        public int Codigo { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(100, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Descricao { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(8000, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Sql { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(300, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Parametro { get; set; }
        public ESql TipoSql { get; set; }
        public EMovimento TipoMovimento { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(0, 20, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int Departamento { get; set; }
        public bool Ativo { get; set; }

        public string DescricaoDepartamento { get; set; }

        public override string ToString() => $"{Codigo} - {Descricao}";
    }
}
