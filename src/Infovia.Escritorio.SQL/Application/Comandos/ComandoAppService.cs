﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.SQL.Application.Comandos.Interfaces;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.Escritorio.SQL.Domain.Comandos.Interfaces;

namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ComandoAppService : IComandoAppService
    {
        private readonly IComandoRepository _repository;
        private readonly IComandoService _service;
        private readonly IMapper _mapper;

        public ComandoAppService(IComandoRepository repository, IComandoService service, IMapper mapper)
        {
            _repository = repository;
            _service = service;
            _mapper = mapper;
        }

        public bool Add(ComandoViewModel entity) => _service.Add(_mapper.Map<Comando>(entity));

        public bool Update(ComandoViewModel entity) => _service.Update(_mapper.Map<Comando>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<ComandoViewModel> GetAll() => _mapper.Map<IEnumerable<ComandoViewModel>>(_repository.GetAll());

        public IEnumerable<ComandoViewModel> GetByPredicate(Expression<Func<ComandoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<ComandoViewModel, bool>>, Expression<Func<Comando, bool>>>(predicate);
            return _mapper.Map<IEnumerable<ComandoViewModel>>(_repository.GetByPredicate(expression));
        }

        public ComandoViewModel GetById(Guid id) => _mapper.Map<ComandoViewModel>(_repository.GetById(id));

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }

        // Comando Empresa
        public IEnumerable<ComandoEmpresaViewModel> GetEmpresaByCodEmpresa(int codigoEmpresa) => _mapper.Map<IEnumerable<ComandoEmpresaViewModel>>(_repository.GetEmpresaByPredicate(c => c.CodigoEmpresa == codigoEmpresa));

        public IEnumerable<ComandoEmpresaViewModel> GetEmpresaByComando(Guid comandoId) => _mapper.Map<IEnumerable<ComandoEmpresaViewModel>>(_repository.GetEmpresaByPredicate(c => c.ComandoId == comandoId));

        public bool UpdateEmpresa(IEnumerable<ComandoEmpresaViewModel> insertList, IEnumerable<Guid> deleteList) => _service.UpdateEmpresas(_mapper.Map<IEnumerable<ComandoEmpresa>>(insertList), deleteList);

        // Log
        public bool AddLog(LogViewModel entity) => _service.AddLog(_mapper.Map<Log>(entity));

        public LogViewModel GetLogById(Guid id) => _mapper.Map<LogViewModel>(_repository.GetLogById(id));

        public IEnumerable<LogViewModel> GetLogByPredicate(Expression<Func<LogViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<LogViewModel, bool>>, Expression<Func<Log, bool>>>(predicate);
            return _mapper.Map<IEnumerable<LogViewModel>>(_repository.GetLogByPredicate(expression));
        }

        public bool AtivarDesativar(Guid id) => _service.AtivarDesativar(id);
    }
}
