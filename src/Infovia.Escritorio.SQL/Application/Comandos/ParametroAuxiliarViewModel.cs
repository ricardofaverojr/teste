﻿using System;
namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class ParametroAuxiliarViewModel
    {
        public string Nome { get; set; }
        public string Valor { get; set; }
    }
}
