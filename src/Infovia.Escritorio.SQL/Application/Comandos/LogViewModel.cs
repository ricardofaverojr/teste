﻿using System;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.SQL.Application.Comandos
{
    public class LogViewModel : ViewModel
    {
        public Guid ComandoId { get; set; }
        public int CodigoEmpresa { get; set; }
        public Guid BancoQuestorId { get; set; }
        public string Sql { get; set; }
        public string Resultado { get; set; }
        public string Parametros { get; set; }
        public int Departamento { get; set; }
        public DateTime DataCadastro { get; set; }
        public int CodigoUsuario { get; set; }        

        public string ConnectionString { get; set; }
        public int CodigoComando { get; set; }

        public override string ToString() => $"{CodigoEmpresa} - {DataCadastro:D} - {Resultado}";
    }
}
