﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.SQL.Application.Modelos.Interfaces;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Infovia.Escritorio.SQL.Domain.Modelos.Interfaces;

namespace Infovia.Escritorio.SQL.Application.Modelos
{
    public class ModeloAppService : IModeloAppService
    {
        private readonly IModeloRepository _repository;
        private readonly IModeloService _service;
        private readonly IMapper _mapper;

        public ModeloAppService(IModeloRepository repository, IModeloService service, IMapper mapper)
        {
            _repository = repository;
            _service = service;
            _mapper = mapper;
        }

        public bool Add(ModeloViewModel entity) => _service.Add(_mapper.Map<Modelo>(entity));

        public bool Update(ModeloViewModel entity) => _service.Update(_mapper.Map<Modelo>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<ModeloViewModel> GetAll() => _mapper.Map<IEnumerable<ModeloViewModel>>(_repository.GetAll());

        public ModeloViewModel GetById(Guid id) => _mapper.Map<ModeloViewModel>(_repository.GetById(id));

        public IEnumerable<ModeloViewModel> GetByPredicate(Expression<Func<ModeloViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<ModeloViewModel, bool>>, Expression<Func<Modelo, bool>>>(predicate);
            return _mapper.Map<IEnumerable<ModeloViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }
    }
}
