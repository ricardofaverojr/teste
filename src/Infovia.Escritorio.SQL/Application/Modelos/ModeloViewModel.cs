﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.SQL.Application.Modelos
{
    public class ModeloViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(100, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Descricao { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(8000, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Sql { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(300, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Parametro { get; set; }
        public string NomeArquivo { get; set; }
        public string TipoArquivo { get; set; }
        public long TamanhoArquivo { get; set; }
        public byte[] ConteudoArquivo { get; set; }
    }
}
