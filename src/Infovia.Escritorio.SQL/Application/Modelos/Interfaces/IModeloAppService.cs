﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.SQL.Application.Modelos.Interfaces
{
    public interface IModeloAppService : IAppService<ModeloViewModel>
    {
    }
}
