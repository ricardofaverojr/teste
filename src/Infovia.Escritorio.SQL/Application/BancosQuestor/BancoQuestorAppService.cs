﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces;
using Infovia.NugetLibrary.Firebird.Models;

namespace Infovia.Escritorio.SQL.Application.BancosQuestor
{
    public class BancoQuestorAppService : IBancoQuestorAppService
    {
        private readonly IBancoQuestorRepository _repository;
        private readonly IBancoQuestorService _service;
        private readonly IMapper _mapper;

        public BancoQuestorAppService(IBancoQuestorRepository repository, IBancoQuestorService service, IMapper mapper)
        {
            _repository = repository;
            _service = service;
            _mapper = mapper;
        }

        public bool Add(BancoQuestorViewModel entity) => _service.Add(_mapper.Map<BancoQuestor>(entity));

        public bool Update(BancoQuestorViewModel entity) => _service.Update(_mapper.Map<BancoQuestor>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public IEnumerable<BancoQuestorViewModel> GetAll() => _mapper.Map<IEnumerable<BancoQuestorViewModel>>(_repository.GetAll());

        public IEnumerable<BancoQuestorViewModel> GetByPredicate(Expression<Func<BancoQuestorViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<BancoQuestorViewModel, bool>>, Expression<Func<BancoQuestor, bool>>>(predicate);
            return _mapper.Map<IEnumerable<BancoQuestorViewModel>>(_repository.GetByPredicate(expression));
        }

        public BancoQuestorViewModel GetById(Guid id) => _mapper.Map<BancoQuestorViewModel>(_repository.GetById(id));

        public void Dispose()
        {
            _repository?.Dispose();
            _service?.Dispose();
        }

        public void Padrao(Guid id) => _service.Padrao(id);

        public bool Teste(Guid id) => new SqlUtils(_repository.GetById(id).ToString()).Test();
    }
}
