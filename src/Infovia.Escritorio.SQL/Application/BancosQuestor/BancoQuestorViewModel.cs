﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.SQL.Application.BancosQuestor
{
    public class BancoQuestorViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(40, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Endereco { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(40, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Caminho { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), Range(1, 65535, ErrorMessage = "Campo {0} precisa ter o valor mínimo de {1} e máximo de {2}")]
        public int Porta { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(40, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Usuario { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Senha { get; set; }
        public bool Padrao { get; set; }

        public override string ToString() => $"User={Usuario};Password={Senha};Database={Caminho};DataSource={Endereco};Port={Porta};Dialect=3;Charset=ISO8859_1;Role=;Connection lifetime=15;Pooling=true;Packet Size=8192;ServerType=0";

    }
}
