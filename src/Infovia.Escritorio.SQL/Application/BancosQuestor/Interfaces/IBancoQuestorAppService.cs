﻿using System;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces
{
    public interface IBancoQuestorAppService : IAppService<BancoQuestorViewModel>
    {
        void Padrao(Guid id);

        bool Teste(Guid id);
    }
}
