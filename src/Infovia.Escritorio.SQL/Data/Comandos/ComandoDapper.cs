﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Dommel;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.Escritorio.SQL.Domain.Comandos.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.SQL.Data.Comandos
{
    public class ComandoDapper : Dapper<Comando>, IComandoDapper
    {
        public ComandoDapper(IConfiguration configuration) : base(configuration)
        {
        }

        // Comando Empresa
        public IEnumerable<ComandoEmpresa> GetEmpresaByCodEmpresa(int codigoEmpresa) => _cn.Select<ComandoEmpresa>(c => c.CodigoEmpresa == codigoEmpresa);

        public IEnumerable<ComandoEmpresa> GetEmpresaByComando(Guid comandoId) => _cn.Select<ComandoEmpresa>(c => c.ComandoId == comandoId);


        // Log
        public Log GetLogById(Guid id) => _cn.Get<Log>(id);

        public IEnumerable<Log> GetLogByPredicate(Expression<Func<Log, bool>> predicate) => _cn.Select<Log>(predicate);
    }
}
