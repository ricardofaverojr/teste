﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Infovia.Escritorio.SQL.Data.Context;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.Escritorio.SQL.Domain.Comandos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.SQL.Data.Comandos
{
    public class ComandoRepository : SqlRepository<Comando>, IComandoRepository
    {
        public ComandoRepository(SqlContext context) : base(context)
        {
        }

        // Comando Empresa
        public void AddEmpresa(ComandoEmpresa entity) => _context.ComandoEmpresa.Add(entity);

        public void DeleteEmpresa(Guid id) => _context.ComandoEmpresa.Remove(_context.ComandoEmpresa.Find(id));

        public IEnumerable<ComandoEmpresa> GetEmpresaByPredicate(Expression<Func<ComandoEmpresa, bool>> predicate) => _context.ComandoEmpresa.Include(c => c.Comando).AsNoTracking().Where(predicate).ToList();


        // Log
        public void AddLog(Log entity) => _context.Log.Add(entity);

        public Log GetLogById(Guid id) => _context.Log.FirstOrDefault(l => l.Id == id);

        public IEnumerable<Log> GetLogByPredicate(Expression<Func<Log, bool>> predicate) => _context.Log.AsNoTracking().Where(predicate).ToList();
    }
}
