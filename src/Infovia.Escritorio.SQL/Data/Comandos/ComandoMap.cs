﻿using System;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.SQL.Data.Comandos
{
    public class ComandoMap : IEntityTypeConfiguration<Comando>
    {
        public void Configure(EntityTypeBuilder<Comando> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Codigo)
                .IsRequired();

            builder.Property(c => c.Descricao)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.Sql)
                .HasColumnType("varchar(max)")
                .HasMaxLength(8000)
                .IsRequired();

            builder.Property(c => c.Parametro)
                .HasColumnType("varchar(300)")
                .HasMaxLength(300);

            builder.ToTable("Comando");
        }
    }
}
