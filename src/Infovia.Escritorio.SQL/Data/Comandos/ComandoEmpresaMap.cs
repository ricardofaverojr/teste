﻿using System;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.SQL.Data.Comandos
{
    public class ComandoEmpresaMap : IEntityTypeConfiguration<ComandoEmpresa>
    {
        public void Configure(EntityTypeBuilder<ComandoEmpresa> builder)
        {
            builder.HasKey(ce => ce.Id);

            builder.HasOne(ce => ce.Comando)
                .WithMany(ce => ce.Empresas)
                .HasForeignKey(ce => ce.ComandoId)
                .IsRequired();

            builder.ToTable("ComandoEmpresa"); ;
        }
    }
}
