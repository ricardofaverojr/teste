﻿using System;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.SQL.Data.Comandos
{
    public class LogMap : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.HasKey(l => l.Id);

            builder.Property(l => l.Sql)
                .HasColumnType("varchar(max)")
                .HasMaxLength(10000)
                .IsRequired();

            builder.Property(l => l.Resultado)
                .HasColumnType("varchar(200)")
                .HasMaxLength(200);

            builder.Property(l => l.Parametros)
                .HasColumnType("varchar(300)")
                .HasMaxLength(300);

            builder.HasOne(l => l.Comando)
                .WithMany(l => l.Logs)
                .HasForeignKey(l => l.ComandoId)
                .IsRequired();

            builder.HasOne(l => l.BancoQuestor)
                .WithMany(l => l.Logs)
                .HasForeignKey(l => l.BancoQuestorId)
                .IsRequired();

            builder.ToTable("Log");
        }
    }
}
