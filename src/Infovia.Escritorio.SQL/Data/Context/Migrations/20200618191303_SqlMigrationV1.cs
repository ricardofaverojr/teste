﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infovia.Escritorio.SQL.Data.Context.Migrations
{
    public partial class SqlMigrationV1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BancoQuestor",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Endereco = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: false),
                    Caminho = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: false),
                    Porta = table.Column<int>(nullable: false),
                    Usuario = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: false),
                    Senha = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    Padrao = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BancoQuestor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comando",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Codigo = table.Column<int>(nullable: false),
                    Descricao = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Sql = table.Column<string>(type: "varchar(max)", maxLength: 8000, nullable: false),
                    Parametro = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    TipoSql = table.Column<int>(nullable: false),
                    TipoMovimento = table.Column<int>(nullable: false),
                    Departamento = table.Column<int>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comando", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Modelo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Descricao = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Sql = table.Column<string>(type: "varchar(max)", maxLength: 8000, nullable: false),
                    Parametro = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    NomeArquivo = table.Column<string>(type: "varchar(100)", nullable: true),
                    TipoArquivo = table.Column<string>(type: "varchar(100)", nullable: true),
                    TamanhoArquivo = table.Column<long>(nullable: false),
                    ConteudoArquivo = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modelo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComandoEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    ComandoId = table.Column<Guid>(nullable: false),
                    OrdemExecucao = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComandoEmpresa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComandoEmpresa_Comando_ComandoId",
                        column: x => x.ComandoId,
                        principalTable: "Comando",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Log",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ComandoId = table.Column<Guid>(nullable: false),
                    CodigoEmpresa = table.Column<int>(nullable: false),
                    BancoQuestorId = table.Column<Guid>(nullable: false),
                    Sql = table.Column<string>(type: "varchar(max)", maxLength: 10000, nullable: false),
                    Resultado = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    Parametros = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    Departamento = table.Column<int>(nullable: false),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    CodigoUsuario = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Log_BancoQuestor_BancoQuestorId",
                        column: x => x.BancoQuestorId,
                        principalTable: "BancoQuestor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Log_Comando_ComandoId",
                        column: x => x.ComandoId,
                        principalTable: "Comando",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComandoEmpresa_ComandoId",
                table: "ComandoEmpresa",
                column: "ComandoId");

            migrationBuilder.CreateIndex(
                name: "IX_Log_BancoQuestorId",
                table: "Log",
                column: "BancoQuestorId");

            migrationBuilder.CreateIndex(
                name: "IX_Log_ComandoId",
                table: "Log",
                column: "ComandoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComandoEmpresa");

            migrationBuilder.DropTable(
                name: "Log");

            migrationBuilder.DropTable(
                name: "Modelo");

            migrationBuilder.DropTable(
                name: "BancoQuestor");

            migrationBuilder.DropTable(
                name: "Comando");
        }
    }
}
