﻿using System;
using System.Linq;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.SQL.Data.Context
{
    public class SqlContext : DbContext, IUnitOfWork
    {
        public SqlContext(DbContextOptions<SqlContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
            ChangeTracker.LazyLoadingEnabled = true;
        }

        public DbSet<BancoQuestor> BancoQuestor { get; set; }
        public DbSet<Comando> Comando { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<ComandoEmpresa> ComandoEmpresa { get; set; }
        public DbSet<Modelo> Modelo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(100)");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SqlContext).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.Ignore<Event>();

            base.OnModelCreating(modelBuilder);
        }

        public bool Commit()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;

                if (entry.State == EntityState.Modified)
                    entry.Property("DataCadastro").IsModified = false;
            }
            return base.SaveChanges() > 0;
        }
    }
}
