﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infovia.Escritorio.SQL.Data.Context
{
    public class SqlRepository<T> : IRepository<T> where T : Entity
    {
        protected readonly SqlContext _context;
        protected readonly DbSet<T> _db;
        public IUnitOfWork UnitOfWork => _context;

        public SqlRepository(SqlContext context)
        {
            _context = context;
            _db = _context.Set<T>();
        }

        public void Add(T entity) => _db.Add(entity);

        public void Update(T entity) => _db.Update(entity);

        public void Delete(Guid id) => _db.Remove(_db.Find(id));


        public IEnumerable<T> GetAll() => _db.AsNoTracking().ToList();

        public IEnumerable<T> GetByPredicate(Expression<Func<T, bool>> predicate) => _db.Where(predicate).ToList();

        public T GetById(Guid id) => _db.AsNoTracking().FirstOrDefault(r => r.Id == id);

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
