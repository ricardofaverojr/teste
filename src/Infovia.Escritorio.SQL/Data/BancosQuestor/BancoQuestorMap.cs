﻿using System;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.SQL.Data.BancosQuestor
{
    public class BancoQuestorMap : IEntityTypeConfiguration<BancoQuestor>
    {
        public void Configure(EntityTypeBuilder<BancoQuestor> builder)
        {
            builder.HasKey(bq => bq.Id);

            builder.Property(bq => bq.Endereco)
                .HasColumnType("varchar(40)")
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(bq => bq.Caminho)
                .HasColumnType("varchar(40)")
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(bq => bq.Porta)
                .IsRequired();

            builder.Property(bq => bq.Usuario)
                .HasColumnType("varchar(40)")
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(bq => bq.Senha)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20)
                .IsRequired();

            builder.ToTable("BancoQuestor"); ;
        }
    }
}
