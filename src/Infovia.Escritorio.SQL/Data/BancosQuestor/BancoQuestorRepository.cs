﻿using System;
using System.Linq;
using Infovia.Escritorio.SQL.Data.Context;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces;

namespace Infovia.Escritorio.SQL.Data.BancosQuestor
{
    public class BancoQuestorRepository : SqlRepository<BancoQuestor>, IBancoQuestorRepository
    {
        public BancoQuestorRepository(SqlContext context) : base(context)
        {
        }

        public string GetConnectionString(Guid? id) => id != null ? _db.Find(id).ToString() : _db.FirstOrDefault(bq => bq.Padrao).ToString();
    }
}
