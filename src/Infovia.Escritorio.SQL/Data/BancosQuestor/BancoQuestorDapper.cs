﻿using System;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.SQL.Data.BancosQuestor
{
    public class BancoQuestorDapper : Dapper<BancoQuestor>, IBancoQuestorDapper
    {
        public BancoQuestorDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
