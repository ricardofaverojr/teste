﻿using System;
using Infovia.Escritorio.SQL.Data.Context;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Infovia.Escritorio.SQL.Domain.Modelos.Interfaces;

namespace Infovia.Escritorio.SQL.Data.Modelos
{
    public class ModeloRepository : SqlRepository<Modelo>, IModeloRepository
    {
        public ModeloRepository(SqlContext context) : base(context)
        {
        }
    }
}
