﻿using System;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.SQL.Data.Modelos
{
    public class ModeloMap : IEntityTypeConfiguration<Modelo>
    {
        public void Configure(EntityTypeBuilder<Modelo> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Descricao)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.Sql)
                .HasColumnType("varchar(max)")
                .HasMaxLength(8000)
                .IsRequired();

            builder.Property(c => c.Parametro)
                .HasColumnType("varchar(300)")
                .HasMaxLength(300);

            builder.ToTable("Modelo");
        }
    }
}
