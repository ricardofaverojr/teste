﻿using System;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Infovia.Escritorio.SQL.Domain.Modelos.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.SQL.Data.Modelos
{
    public class ModeloDapper : Dapper<Modelo>, IModeloDapper
    {
        public ModeloDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
