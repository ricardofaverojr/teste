﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.CRM.Domain.Contatos.Interfaces
{
    public interface IContatoRepository : IRepository<Contato>
    {
    }
}
