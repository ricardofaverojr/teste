﻿using System;
using Infovia.NugetLibrary.DevPack.Domain.Interfaces;

namespace Infovia.Escritorio.CRM.Domain.Contatos.Interfaces
{
    public interface IContatoService : IService<Contato>
    {
    }
}
