﻿using System;
using Infovia.NugetLibrary.DevPack.Data.Interfaces;

namespace Infovia.Escritorio.CRM.Domain.Contatos.Interfaces
{
    public interface IContatoDapper : IDapper<Contato>
    {
    }
}
