﻿using Infovia.Escritorio.CRM.Domain.Contatos.Interfaces;
using Infovia.NugetLibrary.DevPack.Domain;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;

namespace Infovia.Escritorio.CRM.Domain.Contatos
{
    public class ContatoService : Service<Contato>, IContatoService
    {
        public ContatoService(IContatoRepository repository, IMediatorHandler mediator) : base(repository, mediator)
        {
        }
    }
}
