﻿using FluentValidation;
using FluentValidation.Results;
using Infovia.NugetLibrary.DevPack.Domain;

namespace Infovia.Escritorio.CRM.Domain.Contatos
{
    public class Contato : Entity
    {
        public string Nome { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public string Telefone3 { get; set; }
        public string Telefone4 { get; set; }
        public string Telefone5 { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Observacao { get; set; }

        public override string ToString() => Nome;

        public override ValidationResult ValidateEntity() => new ContatoValidation().Validate(this);
    }

    public class ContatoValidation : AbstractValidator<Contato>
    {
        public ContatoValidation()
        {
            RuleFor(c => c.Nome).Length(2, 100).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Telefone1).Length(2, 20).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Telefone2).MaximumLength(20).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Telefone3).MaximumLength(20).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Telefone4).MaximumLength(20).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Telefone5).MaximumLength(20).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Email).MaximumLength(100).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Endereco).MaximumLength(200).WithMessage("'{PropertyName}' inválido");
            RuleFor(c => c.Observacao).MaximumLength(400).WithMessage("'{PropertyName}' inválido");
        }
    }
}
