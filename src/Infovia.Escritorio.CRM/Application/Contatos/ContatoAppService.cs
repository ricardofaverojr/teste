﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using Infovia.Escritorio.CRM.Application.Contatos.Interfaces;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Infovia.Escritorio.CRM.Domain.Contatos.Interfaces;

namespace Infovia.Escritorio.CRM.Application.Contatos
{
    public class ContatoAppService : IContatoAppService
    {
        private readonly IContatoService _service;
        private readonly IContatoRepository _repository;
        private readonly IMapper _mapper;

        public ContatoAppService(IContatoService service, IContatoRepository repository, IMapper mapper)
        {
            _service = service;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Add(ContatoViewModel entity) => _service.Add(_mapper.Map<Contato>(entity));

        public bool Delete(Guid id) => _service.Delete(id);

        public bool Update(ContatoViewModel entity) => _service.Update(_mapper.Map<Contato>(entity));

        public IEnumerable<ContatoViewModel> GetAll() => _mapper.Map<IEnumerable<ContatoViewModel>>(_repository.GetAll());

        public ContatoViewModel GetById(Guid id) => _mapper.Map<ContatoViewModel>(_repository.GetById(id));

        public IEnumerable<ContatoViewModel> GetByPredicate(Expression<Func<ContatoViewModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<ContatoViewModel, bool>>, Expression<Func<Contato, bool>>>(predicate);
            return _mapper.Map<IEnumerable<ContatoViewModel>>(_repository.GetByPredicate(expression));
        }

        public void Dispose()
        {
            _service.Dispose();
            _repository.Dispose();
        }
    }
}
