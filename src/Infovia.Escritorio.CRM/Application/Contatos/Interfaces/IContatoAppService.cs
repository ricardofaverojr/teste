﻿using Infovia.NugetLibrary.DevPack.Application.Interfaces;

namespace Infovia.Escritorio.CRM.Application.Contatos.Interfaces
{
    public interface IContatoAppService : IAppService<ContatoViewModel>
    {
    }
}
