﻿using System;
using System.ComponentModel.DataAnnotations;
using Infovia.NugetLibrary.DevPack.Application;

namespace Infovia.Escritorio.CRM.Application.Contatos
{
    public class ContatoViewModel : ViewModel
    {
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(100, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Campo {0} é obrigatório"), StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Telefone1 { get; set; }
        [StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Telefone2 { get; set; }
        [StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Telefone3 { get; set; }
        [StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Telefone4 { get; set; }
        [StringLength(20, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Telefone5 { get; set; }
        [StringLength(100, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Email { get; set; }
        [StringLength(200, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Endereco { get; set; }
        [StringLength(400, ErrorMessage = "Campo {0} deve ter no máximo {1}", MinimumLength = 2)]
        public string Observacao { get; set; }

        public override string ToString() => Nome;
    }
}
