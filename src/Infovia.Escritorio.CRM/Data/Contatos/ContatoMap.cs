﻿using System;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infovia.Escritorio.CRM.Data.Contatos
{
    public class ContatoMap : IEntityTypeConfiguration<Contato>
    {
        public void Configure(EntityTypeBuilder<Contato> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Telefone1)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(c => c.Telefone2)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20);

            builder.Property(c => c.Telefone3)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20);

            builder.Property(c => c.Telefone4)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20);

            builder.Property(c => c.Telefone5)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20);

            builder.Property(c => c.Endereco)
                .HasColumnType("varchar(200)")
                .HasMaxLength(200);

            builder.Property(c => c.Observacao)
                .HasColumnType("varchar(400)")
                .HasMaxLength(400);

            builder.ToTable("Contato");

        }
    }
}
