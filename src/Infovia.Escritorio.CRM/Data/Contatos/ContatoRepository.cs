﻿using System;
using Infovia.Escritorio.CRM.Data.Context;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Infovia.Escritorio.CRM.Domain.Contatos.Interfaces;

namespace Infovia.Escritorio.CRM.Data.Contatos
{
    public class ContatoRepository : CrmRepository<Contato>, IContatoRepository
    {
        public ContatoRepository(CrmContext context) : base(context)
        {
        }
    }
}
