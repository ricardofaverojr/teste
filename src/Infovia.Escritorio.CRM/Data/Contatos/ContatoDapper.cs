﻿using System;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Infovia.Escritorio.CRM.Domain.Contatos.Interfaces;
using Infovia.NugetLibrary.DevPack.Data;
using Microsoft.Extensions.Configuration;

namespace Infovia.Escritorio.CRM.Data.Contatos
{
    public class ContatoDapper : Dapper<Contato>, IContatoDapper
    {
        public ContatoDapper(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
