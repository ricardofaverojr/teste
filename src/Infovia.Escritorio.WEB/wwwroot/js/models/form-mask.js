
// Form-Validation.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -


$(document).on('nifty.ready', function() {


    // FORM VALIDATION
    // =================================================================
    // Require Bootstrap Validator
    // http://bootstrapvalidator.com/
    // =================================================================


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }

    // Initialize Masked Inputs
    // a - Represents an alpha character (A-Z,a-z)
    // 9 - Represents a numeric character (0-9)
    // * - Represents an alphanumeric character (A-Z,a-z,0-9)
    $('#msk-date').mask('99/99/9999');
    $('#msk-date2').mask('99-99-9999');
    $('#msk-phone').mask('(99) 9999-9999');
    $('#msk-phone-ext').mask('(999) 999-9999? x99999');
    $('#msk-taxid').mask('99-9999999');
    $('#msk-ssn').mask('999-99-9999');
    $('#msk-pkey').mask('a*-999-a999');
    $('#msk-cnpj').mask('99.999.999/9999-99');
    $('#cep').mask('99999-999');
    $('#cepsocio').mask('99999-999');
    $('#dddfone').mask('99');
    $('#numerofone').mask('9999-9999');

});
