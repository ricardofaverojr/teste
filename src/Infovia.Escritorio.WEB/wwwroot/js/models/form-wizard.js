
// Form-Wizard.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -


$(document).on('nifty.ready', function() {
    // FORM WIZARD
    // =================================================================
    // Require Bootstrap Wizard
    // http://vadimg.com/twitter-bootstrap-wizard-example/
    // =================================================================

    // MAIN FORM WIZARD
    // =================================================================
    $('#main-wz').bootstrapWizard({
        tabClass		: 'wz-steps',
        nextSelector	: '.next',
        previousSelector	: '.previous',
        onTabClick: function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#main-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            var wdt = 100/$total;
            var lft = wdt*index;

            $('#main-wz').find('.progress-bar').css({width:wdt+'%',left:lft+"%", 'position':'relative', 'transition':'all .5s'});


            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#main-wz').find('.next').hide();
                $('#main-wz').find('.finish').show();
                $('#main-wz').find('.finish').prop('disabled', false);
            } else {
                $('#main-wz').find('.next').show();
                $('#main-wz').find('.finish').hide().prop('disabled', true);
            }
        }
    });




    // CLASSIC STYLE
    // =================================================================
    $('#cls-wz').bootstrapWizard({
        tabClass		: 'wz-classic',
        nextSelector	: '.next',
        previousSelector	: '.previous',
        onTabClick: function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#cls-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            var wdt = 100/$total;
            var lft = wdt*index;
            $('#cls-wz').find('.progress-bar').css({width:$percent+'%'});

            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#cls-wz').find('.next').hide();
                $('#cls-wz').find('.finish').show();
                $('#cls-wz').find('.finish').prop('disabled', false);
            } else {
                $('#cls-wz').find('.next').show();
                $('#cls-wz').find('.finish').hide().prop('disabled', true);
            }
        }
    });




    // BUBBLE NUMBERS
    // =================================================================
    $('#step-wz').bootstrapWizard({
        tabClass		: 'wz-steps',
        nextSelector	: '.next',
        previousSelector	: '.previous',
        onTabClick: function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#step-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = (index/$total) * 100;
            var wdt = 100/$total;
            var lft = wdt*index;
            var margin = (100/$total)/2;
            $('#step-wz').find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});


            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#step-wz').find('.next').hide();
                $('#step-wz').find('.finish').show();
                $('#step-wz').find('.finish').prop('disabled', false);
            } else {
                $('#step-wz').find('.next').show();
                $('#step-wz').find('.finish').hide().prop('disabled', true);
            }
        }
    });



    // FORM WIZARD WITH TOOLTIP
    // =================================================================
    $('#cir-wz').bootstrapWizard({
        tabClass		    : 'wz-steps',
        nextSelector	    : '.next',
        previousSelector    : '.previous',
        onTabClick: function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#cir-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = (index/$total) * 100;
            var margin = (100/$total)/2;
            $('#cir-wz').find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});

            navigation.find('li:eq('+index+') a').trigger('focus');


            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#cir-wz').find('.next').hide();
                $('#cir-wz').find('.finish').show();
                $('#cir-wz').find('.finish').prop('disabled', false);
            } else {
                $('#cir-wz').find('.next').show();
                $('#cir-wz').find('.finish').hide().prop('disabled', true);
            }
        }
    })




    // FORM WIZARD WITH VALIDATION
    // =================================================================
    $('#bv-wz').bootstrapWizard({
        tabClass		    : 'wz-steps',
        nextSelector	    : '.next',
        previousSelector	: '.previous',
        onTabClick          : function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#bv-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            var wdt = 100/$total;
            var lft = wdt*index;

            $('#bv-wz').find('.progress-bar').css({width:wdt+'%',left:lft+"%", 'position':'relative', 'transition':'all .5s'});

            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#bv-wz').find('.next').hide();
                $('#bv-wz').find('.finish').show();
                $('#bv-wz').find('.finish').prop('disabled', false);
            } else {
                $('#bv-wz').find('.next').show();
                $('#bv-wz').find('.finish').hide().prop('disabled', true);
            }
        },
        onNext: function(){
            isValid = null;
            $('#bv-wz-form').bootstrapValidator('validate');
            if(isValid === false)return false;
        }
    });




    // FORM VALIDATION
    // =================================================================
    // Require Bootstrap Validator
    // http://bootstrapvalidator.com/
    // =================================================================

    var isValid;

    $('#Data')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#bv-wz-form').bootstrapValidator('revalidateField', 'Data');
        });

    $('#bv-wz-form')
        .find('[name="ProcedimentoId"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the color when it is changed */
            $('#bv-wz-form').bootstrapValidator('revalidateField', 'ProcedimentoId');
        });

    $('#bv-wz-form')
        .find('[name="OrigemProspecto"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the color when it is changed */
            $('#bv-wz-form').bootstrapValidator('revalidateField', 'OrigemProspecto');
        });

    $('#bv-wz-form')
        .find('[name="Marketing"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the color when it is changed */
            $('#bv-wz-form').bootstrapValidator('revalidateField', 'Marketing');
        });


    $('#bv-wz-form').bootstrapValidator({
        message: 'Valor Inv�lido!',
        feedbackIcons: {
        valid: 'fa fa-check fa-lg text-success',
        invalid: 'fa fa-close fa-lg',
        validating: 'fa fa-refresh'
        },
        fields: {
            Nome: {
                message: 'Inv�lido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigat�rio!'
                    },
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'N�o deve exceder o tamanho permitido(100)!'
                    }
                }
            },
            Data: {
                message: 'Inv�lido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigat�rio!'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'Formato inv�lido!'
                    }
                }
            },
            Contato: {
                message: 'Inv�lido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 60,
                        message: 'N�o deve exceder o tamanho permitido(60)!'
                    }
                }
            },
            Telefone: {
                message: 'Inv�lido!',
                validators: {
                    phone: {
                        country: 'BR',
                        message: 'Formato inv�lido!'
                    }
                }
            },
            Email: {
                message: 'Inv�lido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'N�o deve exceder o tamanho permitido(100)!'
                    },
                    emailAddress: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            InscrFederal: {
                message: 'Inv�lido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 18,
                        message: 'N�o deve exceder o tamanho permitido(200)!'
                    }
                }
            },
            ProcedimentoId: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: 'Procedimento inv�lido'
                    }
                }
            },
            OrigemProspecto: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: 'Origem do Prospecto inv�lido'
                    }
                }
            },
            Marketing: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: 'Marketing inv�lido'
                    }
                }
            },


            QtdeFiliais: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            CnpjFiliais: {
                message: 'Inv�lido!',
                validators: {
                    notEmpty: {
                        message: 'Cnpjs de Filiais inv�lido'
                    }
                }
            },
            QtdeProLabores: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            QtdeFuncionarios: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            MediaDocSaidas: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            MediaDocEntrada: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            ExpectativaFaturamentoAnual: {
                message: 'Inv�lido!',
                validators: {
                    notEmpty: {
                        message: 'Expectativa de Faturamento Anual inv�lido'
                    }
                }
            },
            QtdeContasBancarias: {
                message: 'Inv�lido!',
                validators: {
                    integer: {
                        message: 'Valor inv�lido!'
                    }
                }
            },
            NomeContadorAtual: {
                message: 'Inv�lido!',
                validators: {
                    notEmpty: {
                        message: 'Nome do Contador Atual inv�lido'
                    }
                }
            }
        }
    }).on('success.field.bv', function(e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');


        // Hide the success icon
        //$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    }).on('error.form.bv', function(e) {
        isValid = false;
    });


    $('#ExpectativaFaturamentoAnual').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#HonorarioMensalAtual').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#HonorarioAnualAtual').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#HonorarioEncerramentoAtual').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });


});
