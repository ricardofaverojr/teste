﻿$(document).ready(function () {

    var faIcon = {
        valid: 'fa fa-check fa-lg text-success',
        invalid: 'fa fa-close fa-lg',
        validating: 'fa fa-refresh'
    }

    // CADASTRO DE SQL
    // =================================================================
    $('#validarComando').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            Descricao: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 200,
                        message: 'Não deve exceder o tamanho permitido(200)!'
                    }
                }
            },
            Sql: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 8000,
                        message: 'Não deve exceder o tamanho permitido(8000)!'
                    }
                }
            },
            Parametro: {
                message: 'Inválido',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    }
                }
            },
            TipoSql: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('TipoSql').val();
                            return (options != null);
                        }
                    }
                }
            },
            TipoMovimento: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('TipoMovimento').val();
                            return (options != null);
                        }
                    }
                }
            },
            Departamento: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('Departamento').val();
                            return (options != null);
                        }
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // BANCO QUESTOR
    $('#validarBancoQuestor').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            Endereco: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 2,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Caminho: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Porta: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    integer: {
                        min: 1,
                        max: 65535,
                        message: 'Valor deve ser entre 1 e 65535'
                    }
                }
            },
            Usuario: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Senha: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 20,
                        message: 'Não deve exceder o tamanho permitido(20)!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
    });
});