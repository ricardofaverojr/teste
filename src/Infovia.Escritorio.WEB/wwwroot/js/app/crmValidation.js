﻿$(document).ready(function () {

    var faIcon = {
        valid: 'fa fa-check fa-lg text-success',
        invalid: 'fa fa-close fa-lg',
        validating: 'fa fa-refresh'
    }

    // CONTATOS
    // =================================================================
    $('#validarContato').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            Nome: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            },
            Telefone1: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Campo obrigatório!'
                    },
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Telefone2: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Telefone3: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Telefone4: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Telefone5: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 40,
                        message: 'Não deve exceder o tamanho permitido(40)!'
                    }
                }
            },
            Pessoa: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 60,
                        message: 'Não deve exceder o tamanho permitido(60)!'
                    }
                }
            },
            Endereco: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            },
            Email: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            },
            Observacao: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 300,
                        message: 'Não deve exceder o tamanho permitido(300)!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    $('#DataCertificado')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarCertificadoDigital').bootstrapValidator('revalidateField', 'DataCertificado');
        });
    // CONTATOS
    // =================================================================
    $('#validarCertificadoDigital').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            EmpresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('EmpresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            EstabId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('EstabId').val();
                            return (options != null);
                        }
                    }
                }
            },
            TipoCertificado: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('TipoCertificado').val();
                            return (options != null);
                        }
                    }
                }
            },
            DataCertificado: {
                message: 'A Data de Emissão não é válidA',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data de Emissão não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data de Emissão é obrigatório.'
                    }
                }
            },
            InscricaoFederal: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // CONVÊNIOS
    // =================================================================
    $('#validarConvenio').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {

            CodigoConvenio: {
                message: 'Inválido!',
                validators: {
                    integer: {
                        message: 'O valor do código informado não é um número.'
                    }
                }
            },
            DescricaoConvenio: {
                message: 'Inválido!',
                validators: {
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // LANÇAMENTO DE CONVÊNIOS
    // =================================================================
    $('#DataLcto')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarLctoConvenio').bootstrapValidator('revalidateField', 'DataLcto');
        });
    $('#validarLctoConvenio').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {

            EmpresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('EmpresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            FuncionarioId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('FuncionarioId').val();
                            return (options != null);
                        }
                    }
                }
            },
            ConvenioId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('ConvenioId').val();
                            return (options != null);
                        }
                    }
                }
            },
            
            DataLcto: {
                message: 'A Data de Lançamento não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data de Lançamento não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data de Lançamento é obrigatório.'
                    }
                }
            }
            
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // EXPORTACAO LANÇAMENTO DE CONVÊNIOS
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExportacaoLctosConvenio').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExportacaoLctosConvenio').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarExportacaoLctosConvenio').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            empresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('empresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            leiaute: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('leiaute').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            }

        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });




    $('#valorLcto').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });


    $('#capitalsocial').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#percentualanexo').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#percentualencerramento').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#percentquotas').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

});


