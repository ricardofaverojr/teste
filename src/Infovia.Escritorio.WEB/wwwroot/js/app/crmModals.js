﻿
// SEGMENTOS
function addSegmento() {
    var url = $('#urlAddSegmento').val();
    var urlIndex = $('#urlIndexSegmento').val();
    var descricao = $('#descricao').val();
    var atividade = $('#atividade').val();
    var corCategoria = $('#corCategoria').val();
    
    var segmento = { Descricao: descricao, Atividade: atividade, CorCategoria: corCategoria };
    $.ajax({
        url: url,
        data: segmento,
        dataType: 'json',
        type: 'post',
        success: function(result) {
            if (result) {
                window.location = urlIndex + '?success=Adicionado!';
            } else {
                window.location = urlIndex + '?error=Registro inválido!';
            }
        }
    });
}


// REGRAATIVIDADE
var codigo = 0;
var listaRegras = new Array();
var listaRegrasAux = new Array();

function modalRegra(id, descricao) {
    var urlDados = $("#urlDadosRegra").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function (dados) {
            codigo = id;
            listaRegrasAux = [];
            listaRegras = [];

            $(dados).each(function(i) {
                listaRegras.push({
                    Id: dados[i].id,
                    Codigo: dados[i].codigo,
                    Regra: dados[i].regra.toString(),
                    Valor: dados[i].valor.toString(),
                    Operador: dados[i].operador.toString()
                });
                listaRegrasAux.push({
                    Id: dados[i].id,
                    Codigo: dados[i].codigo,
                    Regra: dados[i].regra.toString(),
                    Valor: dados[i].valor.toString(),
                    Operador: dados[i].operador.toString()
                });
            });
            
            $('#regraatividade-modal').modal('show');
            $('#tituloRegra').text(descricao);
            tableRegras();
        }
    });
}

function changeRegra() {
    var regra = $('#regra').val();
    $('#valor').empty();
    $('#valor').append('<option value="">Selecione</option>');

    if (regra === '0') {
        $('#valor').append('<option value="0">Simples Nacional</option>');
        $('#valor').append('<option value="1">Geral</option>');
    }
    if (regra === '1') {
        $('#valor').append('<option value="10">Federal</option>');
        $('#valor').append('<option value="11">Estadual</option>');
        $('#valor').append('<option value="12">Municipal</option>');
    }
    if (regra === '2') {
        $('#valor').append('<option value="20">Clínica/Médico</option>');
        $('#valor').append('<option value="21">Indústria</option>');
    }
    if (regra === '3') {
        $('#valor').append('<option value="30">Folha de Pagamento</option>');
        $('#valor').append('<option value="31">Fiscal</option>');
        $('#valor').append('<option value="32">Contablidade</option>');
    }
    if (regra === '4') {
        $('#valor').append('<option value="40">Rio Grande do Sul</option>');
        $('#valor').append('<option value="41">Santa Catarina</option>');
        $('#valor').append('<option value="42">Paraná</option>');
        $('#valor').append('<option value="43">São Paulo</option>');
    }
    $("#valor").selectpicker("refresh");
}

function addRegra() {
    var regra = $('#regra').val();
    var valor = $('#valor').val();
    var operador = $('#operador').val();
    var validar = false;

    if (regra !== "" && valor !== "" && operador !== "") {
        $(listaRegrasAux).each(function () {
            if (this.Regra === regra && this.Valor === valor && this.Operador === operador) {
                validar = true;
                errorMessage('Regra existente!', 'Atividade (Regras)');
            }
        });    
        if (!validar) {
            listaRegrasAux.push({ Id: '', Codigo: codigo, Regra: regra, Valor: valor, Operador: operador });
            tableRegras();
        }        
    } else {
        errorMessage('Regra inválida!', 'Atividade (Regras)');
    }
}

function tableRegras() {
    var incremento = 0;
    $('#regrasTable tbody tr').remove();

    $(listaRegrasAux).each(function () {
        var btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteRegra(' + incremento + ');" style="cursor: pointer"> Excluir</a>';
        var tipoRegra = 'Regime Tributário';
        if (this.Regra === '1') {
            tipoRegra = 'Tipo de Inscrição';
        }
        if (this.Regra === '2') {
            tipoRegra = 'Atividade';
        }
        if (this.Regra === '3') {
            tipoRegra = 'Departamento';
        }
        if (this.Regra === '4') {
            tipoRegra = 'Estado';
        }

        var tipoValor = 'Simples Nacional';
        if (this.Valor === '1') {
            tipoValor = 'Geral';
        }
        if (this.Valor === '10') {
            tipoValor = 'Federal';
        }
        if (this.Valor === '11') {
            tipoValor = 'Estadual';
        }
        if (this.Valor === '12') {
            tipoValor = 'Municipal';
        }
        if (this.Valor === '20') {
            tipoValor = 'Clínica/Médico';
        }
        if (this.Valor === '21') {
            tipoValor = 'Indústria';
        }
        if (this.Valor === '30') {
            tipoValor = 'Folha de Pagamento';
        }
        if (this.Valor === '31') {
            tipoValor = 'Fiscal';
        }
        if (this.Valor === '32') {
            tipoValor = 'Contabilidade';
        }
        if (this.Valor === '40') {
            tipoValor = 'Rio Grande do Sul';
        }
        if (this.Valor === '41') {
            tipoValor = 'Santa Catarina';
        }
        if (this.Valor === '42') {
            tipoValor = 'Paraná';
        }
        if (this.Valor === '43') {
            tipoValor = 'São Paulo';
        }

        var tipoOperador = "OU.";
        if (this.Operador === '1') {
            tipoOperador = "E.";
        }

        $('#regra').val('').selectpicker("refresh");
        $('#valor').val('').selectpicker("refresh");
        $('#operador').val('').selectpicker("refresh");
        var html = '<tr><td>' + tipoRegra + '</td><td>' + tipoValor + '</td><td>' + tipoOperador + '</td><td class="text-right">' + btnExcluir + '</td></tr>';
        $(html).appendTo($('#regrasTable'));
        incremento++;
    });
    $('#regrasTable').trigger('footable_redraw');
}

function deleteRegra(id) {
    listaRegrasAux.splice(id, 1);
    tableRegras();
}

function saveRegra() {
    var url = $('#urlAddRegra').val();
    var urlIndex = $('#urlIndexRegra').val();

    $.ajax({
        url: url,
        data: { listaAtual : listaRegras, listaNova : listaRegrasAux },
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndex + '?success=Adicionados!';
            } else {
                window.location = urlIndex + '?error=Registros inválidos!';
            }
        }
    });
}

var usuario;
// USUARIOS
function modalUsuario(id, nome) {
    var urlDados = $("#urlDadosUsuario").val();
	$.ajax({
		url: urlDados,
		data: { id },
		dataType: 'json',
		type: 'post',
        success: function (objeto) {
	        
			$('#usuario-modal').modal('show');
            $('#tituloUsuario').text(nome);
            if (objeto !== null) {
	            $('#ramal').val(objeto.ramal);
	            $('#departamento').val(objeto.departamento).selectpicker("refresh");
                $('#icone').val(objeto.icone).selectpicker("refresh");
                usuario = {
	                Id: objeto.id,
	                Codigo: objeto.codigo,
	                Nome: objeto.nome,
	                Email: objeto.email,
	                Departamento: objeto.departamento,
	                DataInicial: objeto.dataInicial,
	                DataBaixa: objeto.dataBaixa,
	                Ramal: objeto.ramal,
	                Icone: objeto.icone
                };
            }
		}
	});
}

function saveUsuario() {
	var url = $('#urlEditUsuario').val();
    var urlIndex = $('#urlIndexUsuario').val();

    usuario.Departamento = $('#departamento').val();
    usuario.Ramal = $('#ramal').val();
    usuario.Icone = $('#icone').val();

	$.ajax({
		url: url,
		data: usuario,
		dataType: 'json',
		type: 'post',
		success: function (result) {
			if (result) {
				window.location = urlIndex + '?success=Atualizado!';
			} else {
				window.location = urlIndex + '?error=Registro inválidos!';
			}
		}
	});
}

// GRUPOUSUARIO
function modalGrupoUsuario(id, descricao) {
	var urlDados = $("#urlDadosUsuarios").val();
    $.ajax({
        url: urlDados,
        data: {id},
		dataType: 'json',
		type: 'post',
		success: function (dados) {

			$('#grupousuario-modal').modal('show');
			$('#tituloGrupo').text(descricao);

            $('#usuariosTable tbody tr').remove();

            $(dados).each(function() {
	            var html = '<tr><td>' + this.codigoUsuario + '</td><td>' + this.nomeUsuario + '</td><td>' + this.emailUsuario + '</td><td>' + this.ramal + '</td></tr>';
	            $(html).appendTo($('#usuariosTable'));
            });
            $('#usuariosTable').trigger('footable_redraw');
		}
	});
}

function modalUsuarioGrupo(id, descricao) {
    var urlDados = $("#urlDadosGruposUsuario").val();
	$.ajax({
		url: urlDados,
		data: { id },
		dataType: 'json',
		type: 'post',
		success: function (dados) {

            $('#usuariogrupo-modal').modal('show');
			$('#tituloUsuario').text(descricao);

			$('#gruposTable tbody tr').remove();

			$(dados).each(function () {
				var html = '<tr><td>' + this.codigoGrupo + '</td><td>' + this.nomeGrupo + '</td></tr>';
                $(html).appendTo($('#gruposTable'));
			});
            $('#gruposTable').trigger('footable_redraw');
		}
	});
}

function sincronizarUsuario(usuarioId, nomeUsuario) {

    var urlGetRolesUsuario = $("#urlGetRolesUsuario").val();

    $.ajax({
        url: urlGetRolesUsuario,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dados) {

            var permissao = "";
            for (var j = 0; j < dados.length; j++) {

                permissao = permissao + '<option value="' + dados[j].Name + '">' + dados[j].Name + '</option>';

            }

            bootbox.dialog({
                title: "Ativar Usuário: " + nomeUsuario,
                message: '<div class="row"> ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +


                    '<div class="form-group"> ' +
                    '<label class="col-md-3 control-label text-semibold">Permissão</label> ' +
                    '<div class="col-md-7"> ' +
                    '<select data-placeholder="Selecione a Permissão" id="role" class="form-control chosen-select" tabindex="2">' +
                    '<option value="">Selecione a Permissão</option>' +
                    permissao +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +

                    '</form> ' +
                    '</div> ' +
                    '</div>' +
                    '<script type="text/javascript">' +

                    '$(".modal").on("shown.bs.modal", function () {' +
                    '$(".chosen-select", this).chosen();' +
                    '});' +

                    '<\/script>',
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-primary",
                        callback: function () {

                            var role = $('#role').val();

                            var urlRetorno = $('#urlIndexUsuario').val();
                            var urlSincronizarUsuario = $('#urlSincronizarUsuario').val();

                            $.ajax({
                                url: urlSincronizarUsuario,
                                data: {
                                    id: usuarioId,
                                    selectedRole: role
                                },
                                dataType: 'json',
                                type: 'POST',
                                success: function (result) {
                                    if (result) {
                                        window.location =
                                            urlRetorno + '?success=Sincronização do Usuário Realizada!';
                                    } else {
                                        window.location = urlRetorno + '?error=Sincronização do Usuário Cancelada!';
                                    }
                                }
                            });

                        }
                    }
                }
            });

        }
    });
}

// ESTABS
//function modalEstab(id, empresa) {
//    var urlDados = $("#urlDadosEstabs").val();
//    var html = "";
//	$.ajax({
//		url: urlDados,
//		data: { id },
//		dataType: 'json',
//		type: 'post',
//		success: function (dados) {
//			$('#estab-modal').modal('show');
//			$('#tituloEstab').text(empresa);

//            $('#foo-accordion tbody tr').remove();

//            $(dados).each(function () {
//                var estadual = '';
//                if (this.inscricaoEstadual !== null) {
//	                estadual = this.inscricaoEstadual;
//                }

//                var municipal = '';
//                if (this.inscricaoMunicipal !== null) {
//	                municipal = this.inscricaoMunicipal;
//                }

//                var status = '<span class="label label-table label-success">Ativo</span>';
//                if (this.statusEstab === 2) {
//                    status = '<span class="label label-table label-primary">Inativa</span>';
//                }
//                if (this.statusEstab === 3) {
//	                status = '<span class="label label-table label-info">Devolvida</span>';
//                }
//                if (this.statusEstab === 4) {
//	                status = '<span class="label label-table label-dark">Baixada</span>';
//                }
//                if (this.statusEstab === 5) {
//	                status = '<span class="label label-table label-warning">Sem Movimento</span>';
//                }
                
//                html = html + '<tr><td class="footable-visible footable-first-column"><span class="footable-toggle"></span>' + this.codigoEstab + '</td><td>' + this.apelidoEstab + '</td><td>' + this.inscricaoFederal + '</td><td>' + this.dataInicio + '</td><td>' + status + '</td><td style="display:none">' + estadual + '</td><td style="display:none">' + municipal + '</td><td style="display:none">' + this.nomeMunicipio + ' - ' + this.estado + '</td><td style="display:none">' + this.atividadeFederal + '</td><td style="display:none">' + this.porteEmpresa + '</td><td style="display:none">' + this.dataEncerramento + '</td></tr>';
//            });

//            $(html).appendTo($('#foo-accordion'));

//            $('#foo-accordion').footable();
//            $('#foo-accordion').trigger('footable_redraw');
//		}
//	});
//}

//var empresa;
//function modalEmpresa(id, nome) {
//	var urlDados = $("#urlDadosEmpresa").val();
//	$.ajax({
//		url: urlDados,
//		data: { id },
//		dataType: 'json',
//		type: 'post',
//		success: function (objeto) {
//			$('#empresa-modal').modal('show');
//			$('#tituloEmpresa').text(nome);
//			if (objeto !== null) {
//                $('#segmento').val(objeto.segmentoId).selectpicker("refresh");
//                empresa = {
//					Id: objeto.id,
//					Codigo: objeto.codigo,
//					Nome: objeto.nome,
//					SegmentoId: objeto.segmentoId
//				};
//			}
//		}
//	});
//}

function saveEmpresa(id) {
    var url = $('#urlUpdateEmpresa').val();
	var urlIndex = $('#urlIndexEmpresa').val();

	var segmento = $('#segmento').val();

    $.ajax({
        url: url,
        data: { id, segmento },
		dataType: 'json',
		type: 'post',
		success: function (result) {
			if (result) {
				window.location = urlIndex + '?success=Atualizado!';
			} else {
				window.location = urlIndex + '?error=Registro inválidos!';
			}
		}
	});
}

function addRegime(id, codigo) {
    var url = $('#urlAddRegime').val();
    var urlIndex = $('#urlDetalheEmpresa').val();

    var regime = $('#regime').val();
    var datainicial = $('#datainicial').val();

    var novoRegime = { EmpresaId: id, TipoRegime: regime, CodigoEmpresa: codigo, DataInicial: datainicial, DataFinal: '31/12/2100' };

    $.ajax({
        url: url,
        data: novoRegime,
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndex + '?success=Regime Atualizado!';
            } else {
                window.location = urlIndex + '?error=Atualização Cancelada!';
            }
        }
    });
}

//function modalRegime(id, empresa) {
//	var urlDados = $("#urlDadosRegime").val();
//	$.ajax({
//		url: urlDados,
//		data: { id },
//		dataType: 'json',
//		type: 'post',
//		success: function (dados) {
//            $('#regimes-modal').modal('show');
//            $('#tituloRegime').text(empresa);

//            $('#regimeTable tbody tr').remove();
//            $(dados).each(function () {
	            
//                var corStatus = 'success';
//                var regime = 'Simples Nacional';
                
//                if (this.tipoRegime === 2) {
//	                corStatus = 'info';
//                    regime = 'Lucro Presumido';
//                }
//                if (this.tipoRegime === 3) {
//	                corStatus = 'primary';
//                    regime = 'Lucro Real';
//                }
//                if (this.tipoRegime === 4) {
//	                corStatus = 'purple';
//                    regime = 'Imune/Isenta';
//                }
//                if (this.tipoRegime === 5) {
//	                corStatus = 'warning';
//                    regime = 'Condomínio';
//                }
//                if (this.tipoRegime === 6) {
//	                corStatus = 'pink';
//                    regime = 'Indefinido';
//                }

//                var classe = 'class="text-' + corStatus + ' text-semibold"';
//                if (this.dataFinal !== '31/12/2100') {
//                    corStatus = 'default';
//                    classe = 'class="text-' + corStatus + '"';
//                }
//                var status = '<span class="label label-' + corStatus + '">' + regime + '</span>';

//                var html = '<tr ' + classe + '"><td>' + status + '</td><td>' + this.dataInicial + '</td><td>' + this.dataFinal + '</td></tr>';
//                $(html).appendTo($('#regimeTable'));
//            });
            
//            $('#regimeTable').footable();
//            $('#regimeTable').trigger('footable_redraw');
//		}
//	});
//}

// GRUPOEMPRESA
function modalEmpresaGrupo(id, nome) {
	var urlDados = $("#urlDadosGruposEmpresa").val();
	$.ajax({
		url: urlDados,
		data: { id },
		dataType: 'json',
		type: 'post',
		success: function (dados) {
			$('#empresagrupo-modal').modal('show');
            $('#tituloGrupoEmpresa').text(nome);

			$('#gruposTable tbody tr').remove();

			$(dados).each(function () {
                var html = '<tr><td>' + this.codigoGrupo + '</td><td>' + this.nomeGrupo + '</td><td>' + this.descricaoDepartamento + '</td></tr>';
				$(html).appendTo($('#gruposTable'));
			});
			$('#gruposTable').trigger('footable_redraw');
		}
	});
}

// CLIENTE

var clienteId;
function modalCliente(id, nome) {
    var urlAtivos = $("#urlClientesAtivos").val();
    $.ajax({
        url: urlAtivos,
        dataType: 'json',
        type: 'post',
        success: function (dados) {
            $(dados).each(function (i) {
                $('#chosen-select1').append(`<option value="${dados[i].id}">${dados[i].nome}</option>`);
            });
            $('#chosen-select1').trigger("chosen:updated");
        }
    });

	var urlDados = $("#urlDadosCliente").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function(objeto) {
            $('#cliente-modal').modal('show');
            $('#tituloCliente').text(nome);
            if (objeto !== null) {
                $('#chosen-select1').val(objeto.vinculadoId).trigger("chosen:updated");
            }
            clienteId = id;
        }
    });
}

function saveCliente() {
	var url = $('#urlEditCliente').val();
	var urlIndex = $('#urlIndexCliente').val();

    var vinculoId = $('#chosen-select1').val();
	$.ajax({
		url: url,
        data: { id: clienteId, vinculoId },
		dataType: 'json',
		type: 'post',
		success: function (result) {
			if (result) {
				window.location = urlIndex + '?success=Atualizado!';
			} else {
				window.location = urlIndex + '?error=Registro inválidos!';
			}
		}
	});
}

function enableCliente() {
    if ($('#chosen-select1').val() !== '') {
        document.getElementById('saveCliente').disabled = false;
    } else {
        document.getElementById('saveCliente').disabled = true;
    }
}

function changeFiltroIndexCliente() {
    var filtro = $('#filtro').val();

    if (filtro === '0') {
	    $(".filtroCliente").css('display', 'none');
        $(".filtroContrato").css('display', 'none');
        $(".filtroGrupo").css('display', 'none');
    }
    if (filtro === '1') {
        $(".filtroCliente").css('display', 'block');
        $(".filtroContrato").css('display', 'none');
        $(".filtroGrupo").css('display', 'none');
    }
    if (filtro === '2') {
	    $(".filtroCliente").css('display', 'none');
	    $(".filtroContrato").css('display', 'block');
	    $(".filtroGrupo").css('display', 'none');
    }
    if (filtro === '3') {
	    $(".filtroCliente").css('display', 'none');
	    $(".filtroContrato").css('display', 'none');
	    $(".filtroGrupo").css('display', 'block');
    }
}

// CONTRATO
function saveContrato(id) {
    var url = $('#urlUpdateContrato').val();
    var urlIndex = $('#urlIndexCliente').val();

    var percAnexo = $('#PercentualAnexo').val();
    var percEncerramento = $('#PercentualEncerramento').val();
    var grupoEmpresa = $('#GrupoEmpresa').val();
    var cobranca = $('#Cobranca').val();

    $.ajax({
	    url: url,
	    data: { id, percAnexo, percEncerramento, grupoEmpresa, cobranca, listaAtual: listaAvisos, listaNova: listaAvisosAux },
	    dataType: 'json',
	    type: 'post',
	    success: function (result) {
		    if (result) {
			    window.location = urlIndex + '?success=Atualizado!';
		    } else {
			    window.location = urlIndex + '?error=Atualização Cancelada!';
		    }
	    }
    });
}


var listaAvisos = new Array();
var listaAvisosAux = new Array();

function loadAvisos(dados) {
	$(dados).each(function(i) {
		listaAvisos.push({
			Id: dados[i].id,
			ContratoId: dados[i].contratoId,
			DataInicial: dados[i].dataInicial.toString(),
			DataFinal: dados[i].dataFinal.toString(),
			Aviso: dados[i].aviso.toString()
		});
		listaAvisosAux.push({
			Id: dados[i].id,
			ContratoId: dados[i].contratoId,
			DataInicial: dados[i].dataInicial.toString(),
			DataFinal: dados[i].dataFinal.toString(),
			Aviso: dados[i].aviso.toString()
		});
    });
	tableAvisos();
}

function addAviso(id) {
    var datainicial = $('#datainicial').val();
    var datafinal = $('#datafinal').val();
    var aviso = $('#aviso').val();

    if (datainicial !== "" && datafinal !== "" && aviso !== "") {
	    listaAvisosAux.push({ Id: '', ContratoId: id, DataInicial: datainicial, DataFinal: datafinal, Aviso: aviso });
        tableAvisos();
    } else {
        errorMessage('Aviso inválido!', 'Contrato');
    }
}

function tableAvisos() {
    var incremento = 0;
    $('#avisosTable tbody tr').remove();

    $(listaAvisosAux).each(function () {
        var btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteAviso(' + incremento + ');" style="cursor: pointer"> Excluir</a>';

        $('#datainicial').val('');
        $('#datafinal').val('');
        $('#aviso').val('');
        var html = '<tr><td>' + this.DataInicial + '</td><td>' + this.DataFinal + '</td><td>' + this.Aviso.substring(0,40) + '</td><td class="text-right">' + btnExcluir + '</td></tr>';
        $(html).appendTo($('#avisosTable'));
        incremento++;
    });
    $('#avisosTable').trigger('footable_redraw');
}

function deleteAviso(id) {
    listaAvisosAux.splice(id, 1);
    tableAvisos();
}

// SOCIO
var listaSocio = new Array();
var listaSocioAux = new Array();

function loadSociosCadastro(dados) {

    $(dados).each(function (i) {
        listaSocio.push({
            Id: dados[i].Id,
            CadastroId: dados[i].CadastroId,
            Nome: dados[i].Nome,
            DataInicial: new Date(dados[i].DataInicial).toLocaleDateString(),
            TipoInscr: dados[i].TipoInscr,
            InscrFederal: dados[i].InscrFederal,
            Cep: dados[i].Cep,
            CodigoTipoLograd: dados[i].CodigoTipoLograd,
            Endereco: dados[i].Endereco,
            Numero: dados[i].Numero,
            Complemento: dados[i].Complemento,
            Bairro: dados[i].Bairro,
            SiglaEstado: dados[i].SiglaEstado,
            CodigoMunic: dados[i].CodigoMunic,
            DddFone: dados[i].DddFone,
            Telefone: dados[i].Telefone,
            Email: dados[i].Email,
            ResponsavelLegal: dados[i].ResponsavelLegal,
            TipoSocio: dados[i].TipoSocio.toString(),
            Cargo: dados[i].Cargo,
            Qualificacao: dados[i].Qualificacao,
            PercentQuotas: dados[i].PercentQuotas,
            QtdeCotas: dados[i].QtdeCotas,
            ParticipaAssin: dados[i].ParticipaAssin,
            DeclaraPessoaFisica: dados[i].DeclaraPessoaFisica,
            QualifReceitaFederal: dados[i].QualifReceitaFederal,
            SocioOstensivo: dados[i].SocioOstensivo,
            NumeroRg: dados[i].NumeroRg,
            OrgaoEmissor: dados[i].OrgaoEmissor,
            SiglaEstadoRg: dados[i].SiglaEstadoRg,
            TituloEleitor: dados[i].TituloEleitor,
            DataNascimento: new Date(dados[i].DataNascimento).toLocaleDateString(),
            SiglaEstadoNasc: dados[i].SiglaEstadoNasc,
            CodigoMunicNasc: dados[i].CodigoMunicNasc,
            Nacionalidade: dados[i].Nacionalidade,
            GrauInstrucao: dados[i].GrauInstrucao,
            EstadoCivil: dados[i].EstadoCivil,
            MaiorIdade: dados[i].MaiorIdade,
            NomeRespLegal: dados[i].NomeRespLegal,
            CpfRespLegal: dados[i].CpfRespLegal,
            QualifRespLegal: dados[i].QualifRespLegal
        });
        listaSocioAux.push({
            Id: dados[i].Id,
            CadastroId: dados[i].CadastroId,
            Nome: dados[i].Nome,
            DataInicial: new Date(dados[i].DataInicial).toLocaleDateString(),
            TipoInscr: dados[i].TipoInscr,
            InscrFederal: dados[i].InscrFederal,
            Cep: dados[i].Cep,
            CodigoTipoLograd: dados[i].CodigoTipoLograd,
            Endereco: dados[i].Endereco,
            Numero: dados[i].Numero,
            Complemento: dados[i].Complemento,
            Bairro: dados[i].Bairro,
            SiglaEstado: dados[i].SiglaEstado,
            CodigoMunic: dados[i].CodigoMunic,
            DddFone: dados[i].DddFone,
            Telefone: dados[i].Telefone,
            Email: dados[i].Email,
            ResponsavelLegal: dados[i].ResponsavelLegal,
            TipoSocio: dados[i].TipoSocio.toString(),
            Cargo: dados[i].Cargo,
            Qualificacao: dados[i].Qualificacao,
            PercentQuotas: dados[i].PercentQuotas,
            QtdeCotas: dados[i].QtdeCotas,
            ParticipaAssin: dados[i].ParticipaAssin,
            DeclaraPessoaFisica: dados[i].DeclaraPessoaFisica,
            QualifReceitaFederal: dados[i].QualifReceitaFederal,
            SocioOstensivo: dados[i].SocioOstensivo,
            NumeroRg: dados[i].NumeroRg,
            OrgaoEmissor: dados[i].OrgaoEmissor,
            SiglaEstadoRg: dados[i].SiglaEstadoRg,
            TituloEleitor: dados[i].TituloEleitor,
            DataNascimento: new Date(dados[i].DataNascimento).toLocaleDateString(),
            SiglaEstadoNasc: dados[i].SiglaEstadoNasc,
            CodigoMunicNasc: dados[i].CodigoMunicNasc,
            Nacionalidade: dados[i].Nacionalidade,
            GrauInstrucao: dados[i].GrauInstrucao,
            EstadoCivil: dados[i].EstadoCivil,
            MaiorIdade: dados[i].MaiorIdade,
            NomeRespLegal: dados[i].NomeRespLegal,
            CpfRespLegal: dados[i].CpfRespLegal,
            QualifRespLegal: dados[i].QualifRespLegal
        });
    });
    tableSocios();
}



function addSocio(id) {

    var nome = $('#nomesocio').val();
    var datainicial = $('#datainicialsocio').val();
    var tipoinscr = $('#tipoinscrsocio').val();
    var inscrfederal = $('#inscrfederalsocio').val();
    var cep = $('#cepsocio').val();
    var logradouro = $('#codigotipologradsocio').val();
    var endereco = $('#enderecosocio').val();
    var numero = $('#numerosocio').val();
    var complemento = $('#complementosocio').val();
    var bairro = $('#bairrosocio').val();
    var estado = $('#chosen-select10').val();
    var municipio = $('#chosen-select11').val();
    var ddd = $('#dddfonesocio').val();
    var telefone = $('#numerofonesocio').val();
    var email = $('#emailsocio').val();
    var resplegal = parseInt($('#responsavellegal').val());
    var tiposocio = $('#tiposocio').val();
    var cargo = $('#cargosocio').val();
    var qualif = $('#qualificacao').val();
    var percentquotas = $('#percentquotas').val();
    var qtdecotas = $('#quantidadequotas').val();
    var partassin = parseInt($('#participaassin').val());
    var declarapf = parseInt($('#declarapessoafisica').val());
    var qualifrfb = $('#qualifreceitafederal').val();
    var socioostensivo = parseInt($('#socioostensivo').val());
    var numerorg = $('#numerorg').val();
    var orgaoemissor = $('#chosen-select12').val();
    var estadorg = $('#chosen-select13').val();
    var tituloeleitor = $('#tituloeleitor').val();
    var datanasc = $('#datanascimento').val();
    var estadonasc = $('#chosen-select14').val();
    var municnasc = $('#chosen-select15').val();
    var nacionalidade = $('#nacionalidade').val();
    var grauinstrucao = $('#grauinstrucao').val();
    var estadocivil = $('#estadocivil').val();
    var maioridade = parseInt($('#maioridade').val());
    var nomeresplegal = $('#nomeresplegal').val();
    var cpfresplegal = $('#cpfresplegal').val();
    var qualifresplegal = $('#qualifresplegal').val();

    var existeSocio = false;

    $(listaSocioAux).each(function () {
        if (this.InscrFederal === inscrfederal) {
            existeSocio = true;
            errorMessage('Sócio existente!', 'Sócios');
        }
    });

    if (!existeSocio) {
        listaSocioAux.push({
            Nome: nome, DataInicial: datainicial, TipoInscr: tipoinscr, InscrFederal: inscrfederal,
            Cep: cep, CodigoTipoLograd: logradouro, Endereco: endereco, Numero: numero, Complemento: complemento, Bairro: bairro,
            SiglaEstado: estado, CodigoMunic: municipio, DddFone: ddd, Telefone: telefone, Email: email, ResponsavelLegal: resplegal,
            TipoSocio: tiposocio, Cargo: cargo, Qualificacao: qualif, PercentQuotas: percentquotas, QtdeCotas: qtdecotas, ParticipaAssin: partassin,
            DeclaraPessoaFisica: declarapf, QualifReceitaFederal: qualifrfb, SocioOstensivo: socioostensivo, NumeroRg: numerorg, OrgaoEmissor: orgaoemissor,
            SiglaEstadoRg: estadorg, TituloEleitor: tituloeleitor, DataNascimento: datanasc, SiglaEstadoNasc: estadonasc, CodigoMunicNasc: municnasc,
            Nacionalidade: nacionalidade, GrauInstrucao: grauinstrucao, EstadoCivil: estadocivil, MaiorIdade: maioridade, NomeRespLegal: nomeresplegal,
            CpfRespLegal: cpfresplegal, QualifRespLegal: qualifresplegal
        });
        tableSocios();
    }
}

function tableSocios() {

    var incremento = 0;
    $('#sociosTable tbody tr').remove();

    $(listaSocioAux).each(function () {

        var tipoSocioConverido = "";

        if (this.TipoSocio === '1') {
            tipoSocioConverido = "Sócio";
        } else if (this.TipoSocio === '2') {
            tipoSocioConverido = "Administrador";
        } else if(this.TipoSocio === '3') {
            tipoSocioConverido = "Usufrutuário";
        } else if (this.TipoSocio === '4') {
            tipoSocioConverido = "Titular";
        } else if (this.TipoSocio === '5') {
            tipoSocioConverido = "Presidente";
        } else if (this.TipoSocio === '6') {
            tipoSocioConverido = "Síndico";
        }

        var btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteSocio(' + incremento + ');" style="cursor: pointer"> Excluir</a>';
        var html = '<tr><td>' + this.Nome + '</td><td>' + this.InscrFederal + '</td><td>' + this.DataInicial + '</td><td>' + tipoSocioConverido + '</td><td class="text-right">' + btnExcluir + '</td></tr>';
        $(html).appendTo($('#sociosTable'));
        incremento++;
    });
    $('#sociosTable').trigger('footable_redraw');
}

function deleteSocio(id) {
    listaSocioAux.splice(id, 1);
    tableSocios();
}

function saveCadastro() {
    var tipocadastro = $('#tipocadastro').val();
    var empresaExistente = $('#chosen-select').val();
    var nome = $('#nome').val();
    var data = $('#data').val();
    var contato = $('#contato').val();
    var telefone = $('#msk-phone').val();
    var email = $('#email').val();
    var razaosocial = $('#razaosocial').val();
    var nomefantasia = $('#nomefantasia').val();
    var apelido = $('#apelido').val();
    var cep = $('#cep').val();
    var codigotipolograd = $('#chosen-select2').val();
    var endereco = $('#endereco').val();
    var numero = $('#numero').val();
    var complemento = $('#complemento').val();
    var bairro = $('#bairro').val();
    var siglaestado = $('#chosen-select3').val();
    var codigomunic = $('#chosen-select4').val();
    var dddfone = $('#dddfone').val();
    var numerofone = $('#numerofone').val();
    var datainicioativ = $('#datainicioativ').val();
    var tiporegistro = $('#tiporegistro').val();
    var numeroregistro = $('#numeroregistro').val();
    var tipoinscr = $('#tipoinscr').val();
    var inscrfederal = $('#inscrfederal').val();
    var codigonaturjurid = $('#chosen-select5').val();
    var codigoativfederal = $('#chosen-select6').val();
    var inscrestad = $('#inscrestad').val();
    var codigoativestad = $('#chosen-select7').val();
    var inscrmunic = $('#inscrmunic').val();
    var codigoativmunic = $('#chosen-select8').val();
    var porteempresa = $('#porteempresa').val();
    var capitalsocial = $('#capitalsocial').val();
    var capitalintegralizado = $('#capitalintegralizado').val();
    var segmento = $('#segmento').val();
    var cobranca = $('#cobranca').val();
    var vinculado = $('#chosen-select1').val();
    var percentualanexo = $('#percentualanexo').val();
    var percentualencerramento = $('#percentualencerramento').val();
    var emailfinanceiro = $('#emailfinanceiro').val();
    var emailcontabilidade = $('#emailcontabilidade').val();
    var emailfolhapagamento = $('#emailfolhapagamento').val();
    var emailfiscal = $('#emailfiscal').val();
    var emailpublicacoes = $('#emailpublicacoes').val();

    var dataregistro = $('#dataregistro').val();
    var regime = $('#regime').val();

    var cadastro = {
        Id: '', Nome: nome, Data: data, Contato: contato, Telefone: telefone, Email: email, UsuarioId: '', TipoCadastro: tipocadastro,
        CodigoEmpresa: empresaExistente, CodigoEstab: 0,
        RazaoSocial: razaosocial, NomeFantasia: nomefantasia, Apelido: apelido, Cep: cep, CodigoTipoLograd: codigotipolograd,
        Endereco: endereco, Numero: numero, Complemento: complemento, Bairro: bairro, SiglaEstado: siglaestado, CodigoMunic: codigomunic,
        DddFone: dddfone, NumeroFone: numerofone, DataInicioAtiv: datainicioativ, TipoInscr: tipoinscr, InscrFederal: inscrfederal,
        CodigoNaturJurid: codigonaturjurid, CodigoAtivFederal: codigoativfederal, TipoRegistro: tiporegistro, NumeroRegistro: numeroregistro, DataRegistro: dataregistro,
        InscrEstad: inscrestad, CodigoAtivEstad: codigoativestad, InscrMunic: inscrmunic, CodigoAtivMunic: codigoativmunic, PorteEmpresa: porteempresa,
        CapitalSocial: capitalsocial, CapitalIntegralizado: capitalintegralizado, Regime: regime, SegmentoId: segmento, Cobranca: cobranca,
        VinculadoId: vinculado, PercentualAnexo: percentualanexo, PercentualEncerramento: percentualencerramento,
        //ProcedimentoId: ,
        //StatusContrato: ,
        EmailFinanceiro: emailfinanceiro, EmailContabilidade: emailcontabilidade, EmailFolhaPagamento: emailfolhapagamento, EmailFiscal: emailfiscal,
        EmailPublicacoes: emailpublicacoes, ProspectoId: null
    };

    var url = $('#urlAddCadastro').val();
    var urlIndex = $('#urlIndexCadastro').val();

    var socioAux = JSON.stringify(listaSocioAux);

    $.ajax({
        url: url,
        data: { cadastro: cadastro, listaSocios: socioAux },
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndex + '?success=Adicionados!';
            } else {
                window.location = urlIndex + '?error=Registros inválidos!';
            }
        }
    });

}


function editCadastro(id) {
    var tipocadastro = $('#tipocadastro').val();
    var empresaExistente = $('#chosen-select').val();
    var nome = $('#nome').val();
    var data = $('#data').val();
    var contato = $('#contato').val();
    var telefone = $('#msk-phone').val();
    var email = $('#email').val();
    var razaosocial = $('#razaosocial').val();
    var nomefantasia = $('#nomefantasia').val();
    var apelido = $('#apelido').val();
    var cep = $('#cep').val();
    var codigotipolograd = $('#chosen-select2').val();
    var endereco = $('#endereco').val();
    var numero = $('#numero').val();
    var complemento = $('#complemento').val();
    var bairro = $('#bairro').val();
    var siglaestado = $('#chosen-select3').val();
    var codigomunic = $('#chosen-select4').val();
    var dddfone = $('#dddfone').val();
    var numerofone = $('#numerofone').val();
    var datainicioativ = $('#datainicioativ').val();
    var tiporegistro = $('#tiporegistro').val();
    var numeroregistro = $('#numeroregistro').val();
    var tipoinscr = $('#tipoinscr').val();
    var inscrfederal = $('#inscrfederal').val();
    var codigonaturjurid = $('#chosen-select5').val();
    var codigoativfederal = $('#chosen-select6').val();
    var inscrestad = $('#inscrestad').val();
    var codigoativestad = $('#chosen-select7').val();
    var inscrmunic = $('#inscrmunic').val();
    var codigoativmunic = $('#chosen-select8').val();
    var porteempresa = $('#porteempresa').val();
    var capitalsocial = $('#capitalsocial').val();
    var capitalintegralizado = $('#capitalintegralizado').val();
    var segmento = $('#segmento').val();
    var cobranca = $('#cobranca').val();
    var vinculado = $('#chosen-select1').val();
    var percentualanexo = $('#percentualanexo').val();
    var percentualencerramento = $('#percentualencerramento').val();
    var emailfinanceiro = $('#emailfinanceiro').val();
    var emailcontabilidade = $('#emailcontabilidade').val();
    var emailfolhapagamento = $('#emailfolhapagamento').val();
    var emailfiscal = $('#emailfiscal').val();
    var emailpublicacoes = $('#emailpublicacoes').val();

    var dataregistro = $('#dataregistro').val();
    var regime = $('#regime').val();

    var cadastro = {
        Id: id, Nome: nome, Data: data, Contato: contato, Telefone: telefone, Email: email, UsuarioId: '', TipoCadastro: tipocadastro,
        CodigoEmpresa: empresaExistente, CodigoEstab: 0,
        RazaoSocial: razaosocial, NomeFantasia: nomefantasia, Apelido: apelido, Cep: cep, CodigoTipoLograd: codigotipolograd,
        Endereco: endereco, Numero: numero, Complemento: complemento, Bairro: bairro, SiglaEstado: siglaestado, CodigoMunic: codigomunic,
        DddFone: dddfone, NumeroFone: numerofone, DataInicioAtiv: datainicioativ, TipoInscr: tipoinscr, InscrFederal: inscrfederal,
        CodigoNaturJurid: codigonaturjurid, CodigoAtivFederal: codigoativfederal, TipoRegistro: tiporegistro, NumeroRegistro: numeroregistro, DataRegistro: dataregistro,
        InscrEstad: inscrestad, CodigoAtivEstad: codigoativestad, InscrMunic: inscrmunic, CodigoAtivMunic: codigoativmunic, PorteEmpresa: porteempresa,
        CapitalSocial: capitalsocial, CapitalIntegralizado: capitalintegralizado, Regime: regime, SegmentoId: segmento, Cobranca: cobranca,
        VinculadoId: vinculado, PercentualAnexo: percentualanexo, PercentualEncerramento: percentualencerramento,
        //ProcedimentoId: ,
        //StatusContrato: ,
        EmailFinanceiro: emailfinanceiro, EmailContabilidade: emailcontabilidade, EmailFolhaPagamento: emailfolhapagamento, EmailFiscal: emailfiscal,
        EmailPublicacoes: emailpublicacoes, ProspectoId: null
    };

    var url = $('#urlEditCadastro').val();
    var urlIndex = $('#urlIndexCadastro').val();

    var sociosAux = JSON.stringify(listaSocioAux);
    var socios = JSON.stringify(listaSocio);

    $.ajax({
        url: url,
        data: { cadastro: cadastro, listaSocios: sociosAux, listaSociosAux: socios },
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndex + '?success=Editado!';
            } else {
                window.location = urlIndex + '?error=Registros inválidos!';
            }
        }
    });

}