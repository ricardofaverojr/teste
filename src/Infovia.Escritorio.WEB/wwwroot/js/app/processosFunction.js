﻿/*CONTABILIZAR NFCES*/

function getEstab() {

    var empresaId = $("#chosen-select1").val();
    var urlGetEstab = $("#urlGetEstab").val();

    $('#chosen-select2').empty();

    $.getJSON(urlGetEstab, { empresaId: empresaId }, function (data) {
        $('#chosen-select2').append('<option value="">Selecione</option>');
        $.each(data, function (i, estab) {
            $('#chosen-select2').append('<option value=\"' + estab.Id + "\">" + estab.CodigoEstab + " - " + estab.ApelidoEstab + '</option>');
        });
        $('#chosen-select2').trigger("chosen:updated");
    });
}

function gerarDescontoSindical(id, nomeDesconto) {

    var urlGetEmpresas = $("#urlGetEmpresas").val();

    $.ajax({
        url: urlGetEmpresas,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dadosEmpresas) {

            var empresa = "";

            for (var j = 0; j < dadosEmpresas.length; j++) {
                empresa = empresa + '<option value="' + dadosEmpresas[j].Codigo + '">' + dadosEmpresas[j].Codigo + " - " + dadosEmpresas[j].Nome + '</option>';
            }

            bootbox.dialog({
                title: "<b class=\"text-info\">Gerar Desconto Sindical</b>",
                message: '<div class="row"> ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-2 control-label text-semibold">Empresa</label> ' +
                    '<div class="col-md-10"> ' +
                    '<select data-placeholder="Selecione a Empresa" id="codigoEmpresa" class="form-control chosen-select">' +
                    '<option value="">Selecione a Empresa</option>' +
                    empresa +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +

                    '</form> ' +
                    '</div> ' +
                    '</div>' +
                    '<script type="text/javascript">' +

                    '$(".modal").on("shown.bs.modal", function () {' +
                    '$(".chosen-select", this).chosen();' +
                    '});' +

                    '<\/script>',
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-primary",
                        callback: function () {

                            var codigoEmpresa = $('#codigoEmpresa').val();
                            var urlGerarDescontoSindical = $('#urlGerarDescontoSindical').val();
                            var urlRetornoDescontoSindical = $('#urlIndexDescontoSindical').val();

                            $.ajax({
                                url: urlGerarDescontoSindical,
                                data: {
                                    id: id,
                                    codigoEmpresa: codigoEmpresa
                                },
                                dataType: 'json',
                                type: 'POST',
                                success: function (result) {
                                    if (result) {
                                        window.location =
                                            urlRetornoDescontoSindical + '?success=Desconto Sindical Gerado!';
                                    } else {
                                        window.location = urlRetornoDescontoSindical + '?error=Geração Cancelada!';
                                    }
                                }
                            });

                        }
                    }
                }
            });

        }
    });
}

function gerarTermoSubRogacaoContratoTrabalho(id, nomeDesconto) {

    var urlGetEmpresas = $("#urlGetEmpresas").val();

    $.ajax({
        url: urlGetEmpresas,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dadosEmpresas) {

            var empresa = "";

            for (var j = 0; j < dadosEmpresas.length; j++) {
                empresa = empresa + '<option value="' + dadosEmpresas[j].Codigo + '">' + dadosEmpresas[j].Codigo + " - " + dadosEmpresas[j].Nome + '</option>';
            }

            bootbox.dialog({
                title: "<b class=\"text-info\">Gerar Termo Sub-Rogação Contrato de Trabalho</b>",
                message: '<div class="row"> ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-2 control-label text-semibold">Empresa Origem Funcionário</label> ' +
                    '<div class="col-md-10"> ' +
                    '<select data-placeholder="Selecione a Empresa" id="codigoEmpresaOrigem" class="form-control chosen-select">' +
                    '<option value="">Selecione a Empresa</option>' +
                    empresa +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-2 control-label text-semibold">Empresa Destino Funcionário</label> ' +
                    '<div class="col-md-10"> ' +
                    '<select data-placeholder="Selecione a Empresa" id="codigoEmpresaDestino" class="form-control chosen-select">' +
                    '<option value="">Selecione a Empresa</option>' +
                    empresa +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +

                    '</form> ' +
                    '</div> ' +
                    '</div>' +
                    '<script type="text/javascript">' +

                    '$(".modal").on("shown.bs.modal", function () {' +
                    '$(".chosen-select", this).chosen();' +
                    '});' +

                    '<\/script>',
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-primary",
                        callback: function () {

                            var codigoEmpresaOrigem = $('#codigoEmpresaOrigem').val();
                            var codigoEmpresaDestino = $('#codigoEmpresaDestino').val();

                            var urlGerarTermoSubRogacaoContratoTrabalho = $('#urlGerarTermoSubRogacaoContratoTrabalho').val();
                            var urlRetornoDescontoSindical = $('#urlIndexDescontoSindical').val();

                            $.ajax({
                                url: urlGerarTermoSubRogacaoContratoTrabalho,
                                data: {
                                    id: id,
                                    codigoEmpresaOrigem: codigoEmpresaOrigem,
                                    codigoEmpresaDestino: codigoEmpresaDestino
                                },
                                dataType: 'json',
                                type: 'POST',
                                success: function (result) {
                                    if (result) {
                                        window.location =
                                            urlRetornoDescontoSindical + '?success=Desconto Sindical Gerado!';
                                    } else {
                                        window.location = urlRetornoDescontoSindical + '?error=Geração Cancelada!';
                                    }
                                }
                            });

                        }
                    }
                }
            });

        }
    });

}

/*UPLOAD DE ARQUIVOS*/

function limpaTela() {
    $(".totalizar").css("display", "none");
    $(".pesquisarNotas").css("display", "none");
    $(".converterPlano").css("display", "none");
    $(".compararSefaz").css("display", "none");
    $(".estabelecimento").css("display", "none");
    $(".totalizarFiscal").css("display", "none");
    $(".totalizarContribuicao").css("display", "none");
}


function acaoTipoArquivo() {
    var acao = $("#chosen-select").val();

    console.log("acao: " + acao);
    //console.log("arquivo: " + arquivo);
    if (acao === "0" || acao === "1") {
        $(".totalizar").css("display", "block");
        $(".pesquisarNotas").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".compararSefaz").css("display", "none");
        $(".estabelecimento").css("display", "none");

        $('#chosen-select1').empty();
        $('#chosen-select1').append('<option value=\""\">Selecione</option>');

        if (arquivo === "Fiscal") {
            $(".totalizarFiscal").css("display", "block");
            $(".totalizarContribuicao").css("display", "none");

            $('#chosen-select1').append('<option value=\"0"\">Notas Fiscais</option>');
            $('#chosen-select1').append('<option value=\"1"\">Cupons Fiscais</option>');
            $('#chosen-select1').append('<option value=\"2"\">Conhecimentos de Transporte</option>');
            $('#chosen-select1').append('<option value=\"3"\">Todos os Registros</option>');
            $('#chosen-select1').append('<option value=\"4"\">CIAP</option>');
        }

        if (arquivo === "Contribuição") {
            $(".totalizarContribuicao").css("display", "block");
            $(".totalizarFiscal").css("display", "none");

            $('#chosen-select1').append('<option value=\"0"\">Notas Fiscais</option>');
            $('#chosen-select1').append('<option value=\"1"\">Cupons Fiscais</option>');
            $('#chosen-select1').append('<option value=\"2"\">Conhecimentos de Transporte</option>');
            $('#chosen-select1').append('<option value=\"3"\">Todos os Registros</option>');
            $('#chosen-select1').append('<option value=\"4"\">Ativo Imobilizado</option>');
        }
        $('#chosen-select1').trigger("chosen:updated");
    }


    if (acao === "2") {
        $(".pesquisarNotas").css("display", "block");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".compararSefaz").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".estabelecimento").css("display", "none");
    }

    if (acao === "3") {
        $(".converterPlano").css("display", "block");
        $(".pesquisarNotas").css("display", "none");
        $(".compararSefaz").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }

    if (acao === "4") {
        $(".compararSefaz").css("display", "block");
        $(".estabelecimento").css("display", "block");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }

    if (acao === "5") {
        $(".estabelecimento").css("display", "block");
        $(".compararSefaz").css("display", "block");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }

    if (acao === "6") {
        $(".compararSefaz").css("display", "block");
        $(".estabelecimento").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }

    if (acao === "7") {
        $(".compararSefaz").css("display", "none");
        $(".estabelecimento").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }
    if (acao === "8") {
        $(".compararSefaz").css("display", "none");
        $(".estabelecimento").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }
    if (acao === "") {
        $(".compararSefaz").css("display", "none");
        $(".estabelecimento").css("display", "none");
        $(".converterPlano").css("display", "none");
        $(".pesquisarNotas").css("display", "none");
        $(".totalizar").css("display", "none");
        $(".totalizarFiscal").css("display", "none");
        $(".totalizarContribuicao").css("display", "none");
    }
}

function verificaAjuste() {
    var ajuste = $('#chosen-select9').val();
    if (ajuste === "1") {
        $('.mostrarData').css("display", "block");
    } else {
        $('.mostrarData').css("display", "none");
        $('.mostrarData').val(" ");
    }
}


function gerarArquivo() {

    var urlSubmit = $("#urlSubmitFile").val();


}