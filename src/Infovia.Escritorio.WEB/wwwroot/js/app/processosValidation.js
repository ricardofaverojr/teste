﻿$(document).ready(function () {

    var faIcon = {
        valid: 'fa fa-check fa-lg text-success',
        invalid: 'fa fa-close fa-lg',
        validating: 'fa fa-refresh'
    }


    // CONTABILIZAÇÃO DA NFCES
    // =================================================================

    $('#dataInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarContabilizarNfces').bootstrapValidator('revalidateField', 'dataInicial');
        });

    $('#dataFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarContabilizarNfces').bootstrapValidator('revalidateField', 'dataFinal');
        });

    $('#validarContabilizarNfces').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            estab: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('estab').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // CONTABILIZAÇÃO ECF FILIAL
    // =================================================================

    $('#periodo')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarContabilizarEcfFilial').bootstrapValidator('revalidateField', 'periodo');
        });

    $('#validarContabilizarEcfFilial').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodo: {
                message: 'O Período não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // GERAR RECIBO FINANCEIRO
    // =================================================================

    $('#validarGerarRecibo').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEscritorio: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEscritorio').val();
                            return (options != null);
                        }
                    }
                }
            },
            nroInicialNota: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    integer: {
                        message: 'O valor informado não é um número.'
                    }
                }
            },
            nroFinalNota: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    integer: {
                        message: 'O informado não é um número.'
                    }
                }
            },
            serie: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Não deve exceder o tamanho permitido(100)!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // GERAR RETENÇÃO FINANCEIRO
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarRetencao').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarRetencao').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarGerarRetencao').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEscritorio: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEscritorio').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // GERAR PROCESSO CAGED
    // =================================================================

    $('#validarGeracaoProcessoCaged').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            mes: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('mes').val();
                            return (options != null);
                        }
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    // GERAR INSS RECEBIMENTO
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarInssRecebimento').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarInssRecebimento').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarGerarInssRecebimento').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {

            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            },
            imposto: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    integer: {
                        message: 'O informado não é um número.'
                    }
                }
            },
            variacao: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    integer: {
                        message: 'O informado não é um número.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // GERAR IMPORTAÇÃO FUNRURAL
    // =================================================================

    $('#dataInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarImportacaoFunrural').bootstrapValidator('revalidateField', 'dataInicial');
        });

    $('#dataFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarImportacaoFunrural').bootstrapValidator('revalidateField', 'dataFinal');
        });

    $('#competencia')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarImportacaoFunrural').bootstrapValidator('revalidateField', 'competencia');
        });

    $('#validarGerarImportacaoFunrural').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {

            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            },
            competencia: {
                message: 'A Competência não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Competência não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Competência é obrigatório.'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    // COMPARAÇÃO DA NFSE
    // =================================================================

    $('#dataInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarComparacaoNfse').bootstrapValidator('revalidateField', 'dataInicial');
        });

    $('#dataFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarComparacaoNfse').bootstrapValidator('revalidateField', 'dataFinal');
        });

    $('#validarComparacaoNfse').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            estabelecimento: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('estabelecimento').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // UPLOAD DE ARQUIVOS
    // =================================================================

    $('#validarUploadArquivos').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            empresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('empresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            acao: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('acao').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // EXPORTAÇÃO FOLHA DE PAGAMENTO CAVALETTI - LAYOUT SENIOR
    // =================================================================

    $('#dataInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExportacaoFolhaPagamento').bootstrapValidator('revalidateField', 'dataInicial');
        });

    $('#dataFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExportacaoFolhaPagamento').bootstrapValidator('revalidateField', 'dataFinal');
        });

    $('#validarExportacaoFolhaPagamento').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            EmpresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('EmpresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            bancoQuestorId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('bancoQuestorId').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // NFE SEFAZ CONTABILIDADE
    // =================================================================

    $('#validarProcessarNfeSefazContabilidade').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            empresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('empresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            estabelecimentoId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('estabelecimentoId').val();
                            return (options != null);
                        }
                    }
                }
            },
            contaDebito: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    }
                }
            },
            historico: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    }
                }
            },
            contaCredito: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('contaCredito').val();
                            return (options != null);
                        }
                    }
                }
            }

        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    // ARQUIVO CONTABIL LAYOUT DOMINIO - TERMASA
    // =================================================================

    $('#dataInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarArquivoLayoutDominio').bootstrapValidator('revalidateField', 'dataInicial');
        });

    $('#dataFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarGerarArquivoLayoutDominio').bootstrapValidator('revalidateField', 'dataFinal');
        });

    $('#validarGerarArquivoLayoutDominio').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            EmpresaId: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('EmpresaId').val();
                            return (options != null);
                        }
                    }
                }
            },
            dataInicial: {
                message: 'A Data Inicial não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Inicial é obrigatório.'
                    }
                }
            },
            dataFinal: {
                message: 'A Data Final não é válida',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Data Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Data Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // INCLUSÃO REGISTROS - FISCAL
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarIncluirRegistros').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarIncluirRegistros').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarIncluirRegistros').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            estab: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('estab').val();
                            return (options != null);
                        }
                    }
                }
            },
            tipoInsercao: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('tipoInsercao').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            },
            aliquotaCalculo: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    }
                }
            },
            chaveNfe: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    // GERAÇÃO ATUALIZAÇÃO DE PRODUTOS - FISCAL
    // =================================================================

    $('#validarAtualizacaoProdutos').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            codigoGrupoProduto: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoGrupoProduto').val();
                            return (options != null);
                        }
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    // INSERIR ANEXO ICMS RS - FISCAL
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarInserirAnexoIcmsRs').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarInserirAnexoIcmsRs').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarInserirAnexoIcmsRs').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            },
            codigoObs: {
                message: 'Inválido!',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    // EXCLUSÃO REGISTROS 1923 - FISCAL
    // =================================================================

    $('#periodoInicial')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExclusaoRegistros1923').bootstrapValidator('revalidateField', 'periodoInicial');
        });

    $('#periodoFinal')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#validarExclusaoRegistros1923').bootstrapValidator('revalidateField', 'periodoFinal');
        });

    $('#validarExclusaoRegistros1923').bootstrapValidator({
        message: 'Inválido!',
        feedbackIcons: faIcon,
        fields: {
            codigoEmpresa: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('codigoEmpresa').val();
                            return (options != null);
                        }
                    }
                }
            },
            estab: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('estab').val();
                            return (options != null);
                        }
                    }
                }
            },
            tipoExclusao: {
                excluded: 'false',
                validators: {
                    notEmpty: {
                        message: 'Inválido!'
                    },
                    callback: {
                        message: 'Campo obrigatório!',
                        callback: function (value, validator, $field) {
                            // Get the selected options
                            var options = validator.getFieldElements('tipoExclusao').val();
                            return (options != null);
                        }
                    }
                }
            },
            periodoInicial: {
                message: 'O Período Inicial não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Inicial não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Inicial é obrigatório.'
                    }
                }
            },
            periodoFinal: {
                message: 'O Período Final não é válido',
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'O campo Período Final não é válido. Verifique a Data informada.'
                    },
                    notEmpty: {
                        message: 'O campo Período Final é obrigatório.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });



    $('#aliquotaCalculo').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });


});