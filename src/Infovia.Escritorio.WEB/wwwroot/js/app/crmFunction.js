﻿/*VISUALIZAÇÃO RÁPIDA DE CONTATO*/

function novoContato() {

    $("li#itens").removeClass("active");
    $("div#tabs-box-consulta-contato-1").removeClass("in");
    $("div#tabs-box-consulta-contato-1").removeClass("active");

    $("li#vizualizacao").addClass("active");
    $("div#tabs-box-consulta-contato-2").addClass("in");
    $("div#tabs-box-consulta-contato-2").addClass("active");

    $('div#tabs-box-consulta-contato-2 input').removeAttr("disabled");
    $('div#tabs-box-consulta-contato-2 .btnSalvar').removeAttr("disabled");
}

function pesquisarContatos2() {

    var pesquisa = $("#input-search2").val();
    var url = $("#urlPesquisarContatos").val();
    $.ajax({
        url: url,
        data: { pesquisa: pesquisa },
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            $("#foo-pagination88 tbody tr").remove();
            var html = '';

            $(result).each(function () {
                var telefone1 = "";
                var telefone2 = "";
                var telefone3 = "";
                var telefone4 = "";
                var telefone5 = "";
                var pessoa = "";
                var email = "";
                var endereco = "";
                var observacao = "";

                if (this.Telefone1 !== null) {
                    telefone1 = this.Telefone1;
                }

                if (this.Telefone2 !== null) {
                    telefone2 = this.Telefone2;
                }

                if (this.Telefone3 !== null) {
                    telefone3 = this.Telefone3;
                }

                if (this.Telefone4 !== null) {
                    telefone4 = this.Telefone4;
                }

                if (this.Telefone5 !== null) {
                    telefone5 = this.Telefone5;
                }

                if (this.Pessoa !== null) {
                    pessoa = this.Pessoa;
                }

                if (this.Email !== null) {
                    email = this.Email;
                }

                if (this.Endereco !== null) {
                    endereco = this.Endereco;
                }

                if (this.Observacao !== null) {
                    observacao = this.Observacao;
                }

                html = html + '<tr id=' + this.Id + '>';
                html = html +
                    '<td>' +
                    '<label><input name="marcado" id=' + this.Id + ' type="radio" onclick="selecionar(\'' + this.Id + '\',\'vizualizar\');"></label>' +
                    '</td>' +
                    '<td class="nome">' + this.Nome + '</td>' +
                    '<td class="telefone1">' + telefone1 + '</td>' +
                    '<td class="telefone2">' + telefone2 + '</td>' +
                    '<td class="telefone3">' + telefone3 + '</td>' +
                    '<td class="telefone4">' + telefone4 + '</td>' +
                    '<td class="telefone5">' + telefone5 + '</td>' +
                    '<td class="pessoa">' + pessoa + '</td>' +
                    '<td class="email">' + email + '</td>' +
                    '<td class="endereco">' + endereco + '</td>' +
                    '<td class="observacao">' + observacao + '</td>' +
                    '<td class="text-right">' +

                    '<a class="btn btn-sm btn-default btn-hover-success icon fa fa-pencil add-tooltip" onclick="selecionar(\'' + this.Id + '\',\'editar\');" data-original-title="Editar" data-container="body"></a>' +

                    '</td>';
                html = html + '</tr>';
            });

            $(html).appendTo($("#foo-pagination88"));
            $('#foo-pagination88').footable();

            $('#foo-pagination88').trigger('footable_initialized');

            $('#show-entries').change(function (e) {
                e.preventDefault();
                $('#foo-pagination88').trigger('footable_initialized');
            });

            $('#foo-pagination88').trigger('footable_initialized');
        }
    });
}

function selecionar(id, acao) {

    $('#foo-pagination88 tr').css("font-weight", "");

    if ($("#foo-pagination88 tr td input#" + id).is(':checked')) {

        $("#foo-pagination88 tr").click(function () {
            var codigo = $(this).attr('id');

            $('#foo-pagination88 tr#' + codigo).css("font-weight", "bold");
        });

    } else {

        $("#foo-pagination88 tr").click(function () {

            var codigo = $(this).attr('id');

            $('#foo-pagination88 tr#' + codigo).css("font-weight", "");
        });

    }

    var nome = $("#foo-pagination88 tr#" + id).find('td.nome').text();
    var telefone1 = $("#foo-pagination88 tr#" + id).find('td.telefone1').text();
    var telefone2 = $("#foo-pagination88 tr#" + id).find('td.telefone2').text();
    var telefone3 = $("#foo-pagination88 tr#" + id).find('td.telefone3').text();
    var telefone4 = $("#foo-pagination88 tr#" + id).find('td.telefone4').text();
    var telefone5 = $("#foo-pagination88 tr#" + id).find('td.telefone5').text();
    var pessoa = $("#foo-pagination88 tr#" + id).find('td.pessoa').text();
    var email = $("#foo-pagination88 tr#" + id).find('td.email').text();
    var endereco = $("#foo-pagination88 tr#" + id).find('td.endereco').text();
    var observacao = $("#foo-pagination88 tr#" + id).find('td.observacao').text();

    $('div#tabs-box-consulta-contato-2 input#nome').val((nome !== 'NULL' ? nome : ""));
    $('div#tabs-box-consulta-contato-2 input#telefone1').val((telefone1 !== 'NULL' ? telefone1 : ""));
    $('div#tabs-box-consulta-contato-2 input#telefone2').val((telefone2 !== 'NULL' ? telefone2 : ""));
    $('div#tabs-box-consulta-contato-2 input#telefone3').val((telefone3 !== 'NULL' ? telefone3 : ""));
    $('div#tabs-box-consulta-contato-2 input#telefone4').val((telefone4 !== 'NULL' ? telefone4 : ""));
    $('div#tabs-box-consulta-contato-2 input#telefone5').val((telefone5 !== 'NULL' ? telefone5 : ""));
    $('div#tabs-box-consulta-contato-2 input#pessoa').val((pessoa !== 'NULL' ? pessoa : ""));
    $('div#tabs-box-consulta-contato-2 input#email').val((email !== 'NULL' ? email : ""));
    $('div#tabs-box-consulta-contato-2 input#endereco').val((endereco !== 'NULL' ? endereco : ""));
    $('div#tabs-box-consulta-contato-2 textarea#observacao').val((observacao !== 'NULL' ? observacao : ""));
    $('div#tabs-box-consulta-contato-2 input#id').val((id !== 'NULL' ? id : ""));

    if (acao === "vizualizar") {

        $('div#tabs-box-consulta-contato-2 input').attr("disabled", "true");
        $('div#tabs-box-consulta-contato-2 .btnSalvar').attr("disabled", "true");

    }

    if (acao === "editar") {

        $("li#itens").removeClass("active");
        $("div#tabs-box-consulta-contato-1").removeClass("in");
        $("div#tabs-box-consulta-contato-1").removeClass("active");

        $("li#vizualizacao").addClass("active");
        $("div#tabs-box-consulta-contato-2").addClass("in");
        $("div#tabs-box-consulta-contato-2").addClass("active");

        $('div#tabs-box-consulta-contato-2 input').removeAttr("disabled");
        $('div#tabs-box-consulta-contato-2 .btnSalvar').removeAttr("disabled");

    }

}


function salvarContato() {

    var urlManutencaoContato = $("#urlManutencaoContato").val();
    var urlRetornoHome = $("#urlRetornoHome").val();

    if ($('#nome').val() !== "" && $('#telefone1').val() !== "") {

        var id = $("#id").val();
        var nome = $("#nome").val();
        var telefone1 = $("#telefone1").val();
        var telefone2 = $("#telefone2").val();
        var telefone3 = $("#telefone3").val();
        var telefone4 = $("#telefone4").val();
        var telefone5 = $("#telefone5").val();
        var pessoa = $("#pessoa").val();
        var observacao = $("#observacao").val();
        var email = $("#email").val();
        var endereco = $("#endereco").val();

        $.ajax({
            url: urlManutencaoContato,
            data: {
                id: id,
                nome: nome,
                telefone1: telefone1,
                telefone2: telefone2,
                telefone3: telefone3,
                telefone4: telefone4,
                telefone5: telefone5,
                pessoa: pessoa,
                email: email,
                endereco: endereco,
                observacao: observacao
            },
            dataType: 'json',
            type: 'POST',
            success: function (result) {
                if (result) {
                    window.location =
                        urlRetornoHome + '?success=Contato Finalizado com sucesso!';
                } else {
                    window.location = urlRetornoHome + '?error=Alteração do Contato Cancelado!';
                }
            }
        });
    } else {
        errorMessage('Verifique os campos obrigatórios para adicionar o Contato!', 'CONTATOS');
    }

}

/*CERTIFICADO DIGITAL*/

function getFilialCertificadoDigital() {

    var empresaId = $("#chosen-select2").val();
    var urlGetFilial = $("#urlGetFilial").val();

    $('#chosen-select').empty();

    $.getJSON(urlGetFilial, { empresaId: empresaId }, function (data) {
        $('#chosen-select').append('<option value="">Selecione o Estabelecimento</option>');
        $.each(data, function (i, filial) {
            $('#chosen-select').append('<option value=\"' + filial.Id + "\">" + filial.CodigoEstab + " - " + filial.ApelidoEstab + '</option>');
        });
        $('#chosen-select').trigger("chosen:updated");
    });
}

function getSocioCertificadoDigital() {

    var empresaId = $("#chosen-select2").val();
    var urlGetSocio = $("#urlGetSocio").val();

    $('#chosen-select1').empty();

    $.getJSON(urlGetSocio, { empresaId: empresaId }, function (data) {
        $('#chosen-select1').append('<option value="">Selecione o Sócio</option>');
        $.each(data, function (i, socio) {
            $('#chosen-select1').append('<option value=\"' + socio.CODIGOSOCIO + "\">" + socio.NOMESOCIO + '</option>');
        });
        $('#chosen-select1').trigger("chosen:updated");
    });
}

function tipoCertificadoDigital() {

    if ($("#chosen-select3").val() === "0" || $("#chosen-select3").val() === "1" || $("#chosen-select3").val() === "2") {

        $(".habilitaCertificadoEcpf").css("display", "block");
        $(".habilitaCertificadoEcnpj").css("display", "none");

        $("#chosen-select").val("");
        $('#chosen-select').trigger("chosen:updated");
        $("#ChavePin").val("");

    } else if ($("#chosen-select3").val() === "3" || $("#chosen-select3").val() === "4" || $("#chosen-select3").val() === "5") {

        $(".habilitaCertificadoEcnpj").css("display", "block");
        $(".habilitaCertificadoEcpf").css("display", "none");

        $("#chosen-select1").val("");
        $('#chosen-select1').trigger("chosen:updated");

    }
}

function getInscricaoCertificadoDigital(parametro) {
    var itemId = "";
    if (parametro === "filial") {

        itemId = $("#chosen-select").val();

    } else if (parametro === "socio") {

        itemId = $("#chosen-select1").val();
    }

    var empresaId = $("#chosen-select2").val();
    var urlInscricaoFederal = $("#urlInscricaoCertificadoDigital").val();

    $.ajax({
        url: urlInscricaoFederal,
        data: { empresaId: empresaId, parametro: parametro, itemId: itemId },
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            $("#InscricaoFederal").val(result);
        }
    });
}

function setarDataExpiraCertificadoDgital() {
    var dataEmissao = $("#dataEmissao").val();
    var tipoCertificado = $("#chosen-select3").val();
    var urlSetData = $("#urlSetDataExpiraCertificadoDigital").val();

    $.ajax({
        url: urlSetData,
        data: { tipoCertificado: tipoCertificado, dataEmissao: dataEmissao },
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            $("#dataExpiraRevoga").val(result);
            $("#dataExpiraRevogaHidden").val(result);
        }
    });
}

// CADASTROS
function changeTipoCadastro() {
    var tipo = $('#tipocadastro').val();
    if (tipo === '1' && $("#nome").val() === "") {
        $('#buscaempresa-modal').modal('show');
    }
    if (tipo === '2') {
        $(".empresaEstab1").css('display', 'block');
        $(".empresaEstab2").css('display', 'none');
    } else {
        $(".empresaEstab1").css('display', 'none');
        $(".empresaEstab2").css('display', 'block');
        $('#chosen-select').val('').trigger("chosen:updated");
    }
}

/*SERVIÇOS VARIÁVEIS*/

function adicionarServicoVariavel(retorno) {
    var urlDadosClientes = $('#urlDadosClientes').val();
    var urlServicos = $("#urlDadosServicos").val();

    $.ajax({
        url: urlDadosClientes,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dadosClientes) {
            var clientes = " ";
            for (var i = 0; i < dadosClientes.length; i++) {
                clientes = clientes + "<option value=" + dadosClientes[i].Id + ">" + dadosClientes[i].Codigo + " - " + dadosClientes[i].Nome + "</option>";
            }

            var servico = "";
            $.ajax({
                url: urlServicos,
                data: {
                },
                dataType: 'json',
                type: 'POST',
                success: function (dadosServico) {

                    for (var j = 0; j < dadosServico.length; j++) {
                        servico = servico + '<option value="' + dadosServico[j].CODIGOSERVICOESCRIT + '">' + dadosServico[j].CODIGOSERVICOESCRIT + " - " + dadosServico[j].DESCRSERVICOESCRIT + '</option>';
                    }

                    bootbox.dialog({
                        title: "<b class=\"text-info\">Serviço Variável</b>",
                        message: '<div class="row"> ' +
                            '<div class="col-md-12"> ' +
                            '<form class="form-horizontal"> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Cliente</label> ' +
                            '<div class="col-md-10"> ' +
                            '<select data-placeholder="Selecione o Cliente" id="cliente" class="form-control chosen-select" tabindex="2">' +
                            '<option value="">Selecione o Cliente</option>' +
                            clientes +
                            '</select>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Serviço</label> ' +
                            '<div class="col-md-10"> ' +
                            '<select data-placeholder="Selecione o Serviço" id="servico" class="form-control chosen-select" tabindex="2">' +
                            '<option value="">Selecione o Serviço</option>' +
                            servico +
                            '</select>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Data</label> ' +
                            '<div class="col-md-10"> ' +
                            '<div id="demo-dp-txtinput">' +
                            '<input id="dataServico" type="text" class="form-control" name="dataServico" placeholder="Data do Serviço">' +
                            '</div>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Quantidade</label> ' +
                            '<div class="col-md-10"> ' +
                            '<input id="quantidade" type="text" class="form-control" name="quantidade" placeholder="Quantidade">' +
                            '</div> ' +
                            '</div> ' +

                            '</form> ' +
                            '</div> ' +
                            '</div>' +
                            '<script type="text/javascript">' +

                            '$(\'#demo-dp-txtinput input\').datepicker();' +

                            '$(".modal").on("shown.bs.modal", function () {' +
                            '$(".chosen-select", this).chosen();' +
                            '});' +

                            '$("#quantidade").priceFormat({' +
                            'prefix: " ",' +
                            'centsSeparator: \',\',' +
                            'thousandsSeparator: \'.\'' +
                            '});' +

                            '<\/script>' +
                            '<style>' +
                            //'.datepicker{z-index:1151 !important;}' +
                            '.datepicker{z-index:10000 !important;}' +
                            '</style>',
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-primary",
                                callback: function () {

                                    var cliente = $('#cliente').val();
                                    var servicoItem = $('#servico').val();
                                    var dataServico = $('#dataServico').val();
                                    var quantidade = $('#quantidade').val();

                                    var urlAdicionarServicoVariavel = $('#urlAdicionarServicoVariavel').val();

                                    var urlRetornoHome = $('#urlIndexHome').val();
                                    var urlRetornoServicoVariavel = $('#urlIndexServicoVariavel').val();

                                    var objeto = {};
                                    objeto.ClienteId = cliente;
                                    objeto.CodigoServico = servicoItem;
                                    objeto.DataServico = dataServico;
                                    objeto.Quantidade = quantidade;

                                    $.ajax({
                                        url: urlAdicionarServicoVariavel,
                                        data: {
                                            objeto: objeto
                                        },
                                        dataType: 'json',
                                        type: 'POST',
                                        success: function (result) {
                                            if (retorno === "home" || retorno === "servicoVariavel") {
                                                if (retorno === "home") {
                                                    window.location = urlRetornoHome + '?success=Serviço Variável Adicionado!';
                                                } else if (retorno === "servicoVariavel") {
                                                    window.location = urlRetornoServicoVariavel + '?success=Serviço Variável Adicionado!';
                                                }
                                            } else {
                                                window.location = urlRetornoHome + '?error=Adição de Serviço Variável Cancelada!';
                                            }
                                        }
                                    });

                                }
                            }
                        }
                    });
                }
            });
        }
    });
}


function editarServicoVariavel(objeto) {

    var item = JSON.parse(objeto);
    var dataServicoFormatada = new Date(item.DataServico);

    var urlDadosClientes = $('#urlDadosClientes').val();
    var urlServicos = $("#urlDadosServicos").val();

    $.ajax({
        url: urlDadosClientes,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dadosClientes) {
            var clientes = " ";
            for (var i = 0; i < dadosClientes.length; i++) {

                if (item.ClienteId === dadosClientes[i].Id) {
                    clientes = clientes + '<option value="' + dadosClientes[i].Id + '" selected=\"selected\" >' + dadosClientes[i].Codigo + " - " + dadosClientes[i].Nome + '</option>';
                } else {
                    clientes = clientes + "<option value=" + dadosClientes[i].Id + ">" + dadosClientes[i].Codigo + " - " + dadosClientes[i].Nome + "</option>";
                }
            }

            var servico = "";
            $.ajax({
                url: urlServicos,
                data: {
                },
                dataType: 'json',
                type: 'POST',
                success: function (dadosServico) {

                    for (var j = 0; j < dadosServico.length; j++) {

                        if (item.CodigoServico === dadosServico[j].CODIGOSERVICOESCRIT) {
                            servico = servico + '<option value="' + dadosServico[j].CODIGOSERVICOESCRIT + '" selected=\"selected\" >' + dadosServico[j].CODIGOSERVICOESCRIT + " - " + dadosServico[j].DESCRSERVICOESCRIT + '</option>';
                        } else {
                            servico = servico + '<option value="' + dadosServico[j].CODIGOSERVICOESCRIT + '">' + dadosServico[j].CODIGOSERVICOESCRIT + " - " + dadosServico[j].DESCRSERVICOESCRIT + '</option>';
                        }
                    }

                    bootbox.dialog({
                        title: "<b class=\"text-info\">Serviço Variável</b>",
                        message: '<div class="row"> ' +
                            '<div class="col-md-12"> ' +
                            '<form class="form-horizontal"> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Cliente</label> ' +
                            '<div class="col-md-10"> ' +
                            '<select data-placeholder="Selecione o Cliente" id="clienteId" class="form-control chosen-select" tabindex="2">' +
                            '<option value="">Selecione o Cliente</option>' +
                            clientes +
                            '</select>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Serviço</label> ' +
                            '<div class="col-md-10"> ' +
                            '<select data-placeholder="Selecione o Serviço" id="codigoServico" class="form-control chosen-select" tabindex="2" onchange="getValorServicoEscritorio();">' +
                            '<option value="">Selecione o Serviço</option>' +
                            servico +
                            '</select>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Data</label> ' +
                            '<div class="col-md-10"> ' +
                            '<div id="demo-dp-txtinput">' +
                            '<input id="dataServico" type="text" class="form-control" name="dataServico" placeholder="Data do Serviço" value="' + dataServicoFormatada.toLocaleDateString() + '">' +
                            '</div>' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Quantidade</label> ' +
                            '<div class="col-md-10"> ' +
                            '<input id="quantidade" type="text" class="form-control" name="quantidade" placeholder="Quantidade" value="' + item.Quantidade + '" onchange="totalizarServicoVariavel();">' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Valor Unitário</label> ' +
                            '<div class="col-md-10"> ' +
                            '<input id="valorUnitario" type="text" class="form-control" name="valorUnitario" placeholder="Valor Unitário" value="' + item.ValorUnitario + '" onchange="totalizarServicoVariavel();">' +
                            '</div> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                            '<label class="col-md-2 control-label text-semibold">Valor Total</label> ' +
                            '<div class="col-md-10"> ' +
                            '<input id="valorTotal" type="text" class="form-control" name="valorTotal" placeholder="Valor Total" value="' + item.ValorTotal + '"  disabled="disabled">' +
                            '</div> ' +
                            '</div> ' +

                            '</form> ' +
                            '</div> ' +
                            '</div>' +
                            '<script type="text/javascript">' +

                            '$(\'#demo-dp-txtinput input\').datepicker();' +

                            '$(".modal").on("shown.bs.modal", function () {' +
                            '$(".chosen-select", this).chosen();' +
                            '});' +

                            //'$("#quantidade").priceFormat({' +
                            //'prefix: " ",' +
                            //'centsSeparator: \',\',' +
                            //'thousandsSeparator: \'.\'' +
                            //'});' +

                            '$("#valorUnitario").priceFormat({' +
                            'prefix: " ",' +
                            'centsSeparator: \',\',' +
                            'thousandsSeparator: \'.\'' +
                            '});' +

                            '$("#valorTotal").priceFormat({' +
                            'prefix: " ",' +
                            'centsSeparator: \',\',' +
                            'thousandsSeparator: \'.\'' +
                            '});' +

                            '<\/script>' +
                            '<style>' +
                            //'.datepicker{z-index:1151 !important;}' +
                            '.datepicker{z-index:10000 !important;}' +
                            '</style>',
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-primary",
                                callback: function () {

                                    var cliente = $('#clienteId').val();
                                    var servicoItem = $('#codigoServico').val();
                                    var dataServico = $('#dataServico').val();
                                    var quantidade = $('#quantidade').val();
                                    var vlrUnitario = $('#valorUnitario').val();
                                    var vlrTotal = $('#valorTotal').val();

                                    var urlEditarServicoVariavel = $('#urlUpdateServicoVariavel').val();
                                    var urlRetornoServicoVariavel = $('#urlIndexServicoVariavel').val();

                                    var obj = {};

                                    obj.Id = item.Id;
                                    obj.ClienteId = cliente;
                                    obj.CodigoServico = servicoItem;
                                    obj.DataServico = dataServico;
                                    obj.Quantidade = quantidade;
                                    obj.ValorUnitario = vlrUnitario;
                                    obj.ValorTotal = vlrTotal;

                                    $.ajax({
                                        url: urlEditarServicoVariavel,
                                        data: {
                                            objeto: obj
                                        },
                                        dataType: 'json',
                                        type: 'POST',
                                        success: function (result) {
                                            if (result) {
                                                window.location = urlRetornoServicoVariavel + '?success=Serviço Variável Alterado!';
                                            } else {
                                                window.location = urlRetornoServicoVariavel + '?error=Alteração de Serviço Variável Cancelada!';
                                            }
                                        }
                                    });

                                }
                            }
                        }
                    });
                }
            });
        }
    });
}


function gerarQuestorServicoVariavel(servicoVariavelId) {

    var urlGetEscritorios = $("#urlGetEscritorios").val();

    $.ajax({
        url: urlGetEscritorios,
        data: {
        },
        dataType: 'json',
        type: 'POST',
        success: function (dadosEscritorios) {

            var escritorio = "";

            for (var j = 0; j < dadosEscritorios.length; j++) {
                escritorio = escritorio + '<option value="' + dadosEscritorios[j].CODIGOESCRIT + '">' + dadosEscritorios[j].CODIGOSERVICOESCRIT + " - " + dadosEscritorios[j].DESCRSERVICOESCRIT + '</option>';
            }

            bootbox.dialog({
                title: "<b class=\"text-info\">Gerar Serviço Variável no Questor</b>",
                message: '<div class="row"> ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-2 control-label text-semibold">Escritório</label> ' +
                    '<div class="col-md-10"> ' +
                    '<select data-placeholder="Selecione o Escritório" id="escritorioId" class="form-control chosen-select">' +
                    '<option value="">Selecione o Escritório</option>' +
                    escritorio +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-2 control-label text-semibold">Data</label> ' +
                    '<div class="col-md-10"> ' +
                    '<div id="demo-dp-txtinput">' +
                    '<input id="data" name="data" type="text" placeholder="Data" class="form-control input-md">' +
                    '</div>' +
                    '</div> ' +
                    '</div> ' +

                    '</form> ' +
                    '</div> ' +
                    '</div>' +
                    '<script type="text/javascript">' +

                    '$(\'#demo-dp-txtinput input\').datepicker();' +
                    '$(".modal").on("shown.bs.modal", function () {' +
                    '$(".chosen-select", this).chosen();' +
                    '});' +

                    '<\/script>' +
                    '<style>' +
                    '.datepicker{z-index:10000 !important;}' +
                    '</style>',
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-primary",
                        callback: function () {

                            var escritorioId = $('#escritorioId').val();
                            var data = $("#data").val();

                            var urlGerarQuestor = $('#urlGerarQuestor').val();
                            var urlRetornoServicoVariavel = $('#urlIndexServicoVariavel').val();

                            $.ajax({
                                url: urlGerarQuestor,
                                data: {
                                    id: servicoVariavelId,
                                    escritorio: escritorioId,
                                    data: data
                                },
                                dataType: 'json',
                                type: 'POST',
                                success: function (result) {
                                    if (result) {
                                        window.location =
                                            urlRetornoServicoVariavel + '?success=Serviço Variável Gerado!';
                                    } else {
                                        window.location = urlRetornoServicoVariavel + '?error=Geração Cancelada!';
                                    }
                                }
                            });

                        }
                    }
                }
            });

        }
    });

}

function cancelarServico(id, nomeItem, nomeCliente) {
    if (id !== null) {
        var url = $("#urlCancelarServico").val();
        bootbox.confirm("Tem certeza que deseja cancelar " + "o Serviço " + nomeItem + " do Cliente " + nomeCliente + " ?", function (result) {
            if (result && url !== null) {
                window.location.href = url + "/" + id;
            } else {
                errorMessage('Cancelamento não Realizado', 'Serviço Variável');
            }
        });
    }
}

function excluirGeracao(id, nomeItem) {

    if (id !== null) {
        var url = $("#urlExcluirGeracao").val();
        bootbox.confirm("Tem certeza que deseja excluir " + "a Geração do Serviço " + nomeItem + " ?", function (result) {
            if (result && url !== null) {
                window.location.href = url + "/" + id;
            } else {
                errorMessage('Exclusão Cancelada', 'Serviço Variável');
            }
        });
    }
}

function totalizarServicoVariavel(){
    var valorTotalServicoVariavel = numeral(numeral($("#quantidade").val()).value() * numeral($("#valorUnitario").val()).value()).format('0,0.00');
    $("#valorTotal").val(valorTotalServicoVariavel);
}

function getValorServicoEscritorio() {

    var item = $("#codigoServico").val();
    var urlGetValorUnit = $("#urlGetValorUnitarioServico").val();

    $.ajax({
        url: urlGetValorUnit,
        data: {
            codigo: item
        },
        dataType: 'json',
        type: 'POST',
        success: function (valorUnitario) {
            $("#valorUnitario").val(valorUnitario);
            totalizarServicoVariavel();
        }
    });

}

function buscarEmpresa() {
    var inscricao = $('#msk-cnpj').val();
    var urlConsulta = $('#urlConsultarEmpreaRFB').val();

    $.ajax({
        url: urlConsulta,
        data: { inscricao },
        dataType: 'json',
        type: 'POST',
        success: function(data) {

            $('#nome').val(data.nome);
            $('#razaosocial').val(data.nome);
            $('#nomefantasia').val(data.fantasia);
            $('#apelido').val(data.tipo);
            $('#cep').val(data.cep.replace('.',''));
            $('#codigotipolograd').val(data.logradouro.split(' ')[0]);

            var endereco = data.logradouro.replace(`${data.logradouro.split(' ')[0]} `, '');
            $('#endereco').val(endereco);
            $('#numero').val(data.numero);
            $('#complemento').val(data.complemento);
            $('#bairro').val(data.bairro);
            $('#siglaestado').val(data.uf);
            $('#codigomunic').val(data.municipio);

            var telefone = data.telefone.split('/')[0].replace(' ', '').trim().replace('(', '').replace(')', '');
            $('#dddfone').val(telefone.substring(0,2));
            $('#numerofone').val(telefone.substring(2,telefone.length));

            $('#datainicioativ').val(data.abertura);
            $('#tipoinscr').val('2').selectpicker("refresh");
            $('#inscrfederal').val(data.cnpj);
            $('#codigonaturjurid').val(data.natureza_juridica);
            $('#codigoativfederal').val(data.atividade_principal[0].code.replace(',',''));
            //$('#porteempresa').val(data.abertura);
            $('#capitalsocial').val(data.capital_social.toLocaleString('pt-BR'));

            $('#capitalsocial').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });


            $('#nome').focus();

            $('#buscaempresa-modal').modal('hide');
        }
    });
}

function maskInscricao() {
    var tipoinscricao = $('#tipoinscr').val();
    $('#inscrfederal').mask('');
    if (tipoinscricao === '1') {
        $('#inscrfederal').mask('999.999.999-99');
    }
    if (tipoinscricao === '2') {
        $('#inscrfederal').mask('99.999.999/9999-99');
    }
    if (tipoinscricao === '3') {
        $('#inscrfederal').mask('99.999.999.999-9');
    }
}

function getMunicipios(campoEstado, campoMunicipio) {
    var estado = $(`#${campoEstado}`).val();
    var url = $('#urlGetMunicipios').val();
    $(`#${campoMunicipio}`).empty();
    $(`#${campoMunicipio}`).append('<option value="">Selecione o municipio</option>');

    $.ajax({
        url: url,
        data: { estado },
        dataType: 'json',
        type: 'POST',
        success: function (dados) {
            $(dados).each(function (i) {
                $(`#${campoMunicipio}`).append(`<option value="${dados[i].CODIGOMUNIC}">${dados[i].NOMEMUNIC} - ${dados[i].SIGLAESTADO}</option>`);
            });
            $(`#${campoMunicipio}`).trigger("chosen:updated");
        }
    });
}

function getAtivEstad() {
    var estado = $('#chosen-select3').val();
    var url = $('#urlGetAtivEstad').val();
    $('#chosen-select7').empty();
    $('#chosen-select7').append('<option value="">Selecione a atividade estadual</option>');

    $.ajax({
        url: url,
        data: { estado },
        dataType: 'json',
        type: 'POST',
        success: function (dados) {
            $(dados).each(function (i) {
                $('#chosen-select7').append(`<option value="${dados[i].codigoativestad}">${dados[i].codigoativestad} - ${dados[i].descrativestad}</option>`);
            });
            $('#chosen-select7').trigger("chosen:updated");
        }
    });
}

function getAtivMunic() {
    var estado = $('#chosen-select3').val();
    var codigo = $('#chosen-select4').val();
    var url = $('#urlGetAtivMunic').val();
    $('#chosen-select8').empty();
    $('#chosen-select8').append('<option value="">Selecione a atividade municipal</option>');

    $.ajax({
        url: url,
        data: { estado, codigo },
        dataType: 'json',
        type: 'POST',
        success: function (dados) {
            $(dados).each(function (i) {
                $('#chosen-select8').append(`<option value="${dados[i].codigoativmunic}">${dados[i].codigoativmunic} - ${dados[i].descrativmunic}</option>`);
            });
            $('#chosen-select8').trigger("chosen:updated");
        }
    });
}


// SINTESE
var listaAssuntos = new Array();
var listaAssuntosAux = new Array();

function loadAssuntosSintese(dados) {

    $(dados).each(function (i) {
        listaAssuntos.push({
            Id: dados[i].Id,
            Assunto: dados[i].Assunto,
            Decisao: dados[i].Decisao,
            Responsavel: dados[i].Responsavel,
            Prazo: new Date(dados[i].Prazo).toLocaleDateString()
        });
        listaAssuntosAux.push({
            Id: dados[i].Id,
            Assunto: dados[i].Assunto,
            Decisao: dados[i].Decisao,
            Responsavel: dados[i].Responsavel,
            Prazo: new Date(dados[i].Prazo).toLocaleDateString()
        });
    });
    tableAssuntos();
}

function addAssunto() {

    var assunto = $('#assunto').val();
    var decisao = $('#decisao').val();
    var responsavel = $('#responsavel').val();
    var prazo = $('#prazo').val();

    if (assunto !== "" && decisao !== "") {
        listaAssuntosAux.push({ Id: '', Assunto: assunto, Decisao: decisao, Responsavel: responsavel, Prazo: prazo });
        tableAssuntos();

        $('#assunto').val("");
        $('#decisao').val("");
        $('#responsavel').val("");
        $('#prazo').val("");

    } else {
        errorMessage('Assunto inválido!', 'Síntese de Reunião');
    }
}

function tableAssuntos() {
    var incremento = 0;
    $('#assuntoTable tbody tr').remove();

    $(listaAssuntosAux).each(function () {
        var btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteAssunto(' + incremento + ');" style="cursor: pointer"> Excluir</a>';

        var prazoFormatado = "";
        if (this.Prazo !== null) {
            prazoFormatado = this.Prazo;
        } else {
            prazoFormatado = "";
        }

        var html = '<tr><td>' + this.Assunto + '</td><td>' + this.Decisao + '</td><td>' + this.Responsavel + '</td><td>' + prazoFormatado + '</td><td class="text-right">' + btnExcluir + '</td></tr>';
        $(html).appendTo($('#assuntoTable'));
        incremento++;
    });
    $('#assuntoTable').trigger('footable_redraw');
}

function deleteAssunto(id) {
    listaAssuntosAux.splice(id, 1);
    tableAssuntos();
}

function saveAssuntos() {
    var urlAdd = $('#urlAdicionarSintese').val();
    var urlIndexSintese = $('#urlIndexSintese').val();

    var sintese = {
        EmpresaId: $("#chosen-select1").val(),
        DataHora: $("#dataHora").val(),
        Local: $("#local").val(),
        ParticipantesEscritorio: $("#participantesEscritorio").val(),
        ParticipantesEmpresa: $("#participantesEmpresa").val()
    };

    $.ajax({
        url: urlAdd,
        data: { sintese: sintese, listaAtual: listaAssuntos, listaNova: listaAssuntosAux },
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndexSintese + '?success=Adicionados!';
            } else {
                window.location = urlIndexSintese + '?error=Registros inválidos!';
            }
        }
    });
}
    function editAssuntos() {
        var urlEdit = $('#urlUpdateSintese').val();
        var urlIndexSintese = $('#urlIndexSintese').val();

        var sintese = {
            Id: $("#sinteseId").val(),
            EmpresaId: $("#chosen-select1").val(),
            DataHora: $("#dataHora").val(),
            Local: $("#local").val(),
            ParticipantesEscritorio: $("#participantesEscritorio").val(),
            ParticipantesEmpresa: $("#participantesEmpresa").val()
        };

        $.ajax({
            url: urlEdit,
            data: { sintese: sintese, listaAtual: listaAssuntos, listaNova: listaAssuntosAux },
            dataType: 'json',
            type: 'post',
            success: function (result) {
                if (result) {
                    window.location = urlIndexSintese + '?success=Adicionados!';
                } else {
                    window.location = urlIndexSintese + '?error=Registros inválidos!';
                }
            }
        });
    }


/*LANÇAMENTO DE CONVÊNIOS*/


function getFuncionariosEmpresa() {

    var empresaId = $("#chosen-select1").val();
    var urlGetFuncionario = $("#urlGetFuncionariosEmpresa").val();

    $('#chosen-select2').empty();

    $.getJSON(urlGetFuncionario, { empresaId: empresaId }, function (data) {
        $('#chosen-select2').append('<option value="">Selecione</option>');
        $.each(data, function (i, func) {
            $('#chosen-select2').append('<option value=\"' + func.CODIGOFUNCCONTR + "\">" + func.NOMEFUNC + '</option>');
        });
        $('#chosen-select2').trigger("chosen:updated");
    });

}




var listaLctoConvenio = new Array();

function addLctoConvenio() {

    var empresaId = $('#chosen-select1').val();
    var nomeEmpresa = $('#chosen-select1 option:selected').text();

    var funcionarioId = $('#chosen-select2').val();
    var nomeFuncionario = $('#chosen-select2 option:selected').text();

    var convenioId = $('#chosen-select3').val();
    var nomeConvenio = $('#chosen-select3 option:selected').text();

    var valorLcto = $('#valorLcto').val();
    var dataLcto = $('#dataLcto').val();

    if (empresaId !== "" && funcionarioId !== "" && convenioId !== "" && valorLcto !== "" && dataLcto !== "") {
        listaLctoConvenio.push({
            EmpresaId: empresaId,
            NomeEmpresa: nomeEmpresa,
            FuncionarioId: funcionarioId,
            NomeFuncionario: nomeFuncionario,
            ConvenioId: convenioId,
            DescricaoConvenio: nomeConvenio,
            ValorLcto: valorLcto,
            DataLcto : dataLcto
        });
        tableLctosConvenio();

        $("#chosen-select2").val("");
        $('#chosen-select2').trigger("chosen:updated");
        $("#chosen-select3").val("");
        $('#chosen-select3').trigger("chosen:updated");
        $('#valorLcto').val("");
        $('#dataLcto').val("");

    } else {
        errorMessage('Lançamento inválido!', 'Lançamentos de Convênios');
    }
}


function tableLctosConvenio() {
    var incremento = 0;
    $('#lctoConvenioTable tbody tr').remove();

    $(listaLctoConvenio).each(function () {
        var btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteLctosConvenio(' + incremento + ');" style="cursor: pointer"> Excluir</a>';

        var html = '<tr><td>' + this.NomeEmpresa + '</td><td>' + this.NomeFuncionario + '</td><td>' + this.DescricaoConvenio + '</td><td>' + this.DataLcto + '</td><td>' + this.ValorLcto + '</td><td class="text-right">' + btnExcluir + '</td></tr>';
        $(html).appendTo($('#lctoConvenioTable'));
        incremento++;
    });
    $('#lctoConvenioTable').trigger('footable_redraw');
}

function deleteLctosConvenio(id) {
    listaLctoConvenio.splice(id, 1);
    tableLctosConvenio();
}

function saveLctosConvenio() {
    var urlAdd = $('#urlAdicionarLctoConvenio').val();
    var urlIndexLctoConvenio = $('#urlIndexLctoConvenio').val();

    $.ajax({
        url: urlAdd,
        data: { listaAtual: listaLctoConvenio },
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndexLctoConvenio + '?success=Adicionados!';
            } else {
                window.location = urlIndexLctoConvenio + '?error=Registros inválidos!';
            }
        }
    });
}

function habilitarExportacao() {
    var tipo = $("#chosen-select2").val();
    if (tipo === '1') {
        $(".exibeExportacao").css("display", "block");
        $("#chosen-select3").val(" ");
    } else {
        $(".exibeExportacao").css("display", "none");
        $("#chosen-select3").val(" ").trigger("chosen:updated");
    }
}


/*ATUALIZAÇÃO DE PRODUTOS*/

function getGrupoProduto(id) {

    var codigoEmpresa = $("#chosen-select1").val();
    var urlGetGrupoProduto = $("#urlGetGrupoProduto").val();

    $(id).empty();

    $.getJSON(urlGetGrupoProduto, { codigoEmpresa: codigoEmpresa }, function (data) {
        $(id).append('<option value="">Selecione</option>');
        $.each(data, function (i, gru) {
            $(id).append('<option value=\"' + gru.CODIGOGRUPOPRODUTO + "\">" + gru.CODIGOGRUPOPRODUTO + " - " + gru.DESCRGRUPOPRODUTO + '</option>');
        });
        $(id).trigger("chosen:updated");
    });

}