﻿/* BancoQuestor - Inicio */
var idBancoQuestor = "";
var bancoPadrao = false;
function modalBancoQuestor(objeto) {
    if (objeto !== null) {
        const bancoQuestor = JSON.parse(objeto);
        idBancoQuestor = bancoQuestor.Id;
        bancoPadrao = bancoQuestor.Padrao;
        $("#endereco").val(bancoQuestor.Endereco);
        $("#caminho").val(bancoQuestor.Caminho);
        $("#porta").val(bancoQuestor.Porta);
        $("#usuario").val(bancoQuestor.Usuario);
        $("#senha").val(bancoQuestor.Senha);
    }
   
    $("#bancoquestor-modal").modal("show");
    mensagemTitulo(idBancoQuestor, "tituloBancoQuestor", "Conexão Questor");
}

function addBancoQuestor() {
    var url = $("#urlAddBancoQuestor").val();
    var urlIndex = $("#urlIndexBancoQuestor").val();

    const endereco = $("#endereco").val();
    const caminho = $("#caminho").val();
    const porta = $("#porta").val();
    const usuario = $("#usuario").val();
    const senha = $("#senha").val();

    var bancoQuestor = { Endereco: endereco, Caminho: caminho, Porta: porta, Usuario: usuario, Senha: senha };
    var mensagem = "Adicionado!";
    if (idBancoQuestor !== "") {
        url = $("#urlEditBancoQuestor").val();
        bancoQuestor = { Id: idBancoQuestor, Endereco: endereco, Caminho: caminho, Porta: porta, Usuario: usuario, Senha: senha, Padrao: bancoPadrao };
        mensagem = "Atualizado!";
    }
    idBancoQuestor = "";
    bancoPadrao = false;
    $.ajax({
        url: url,
        data: bancoQuestor,
        dataType: "json",
        type: "post",
        success: function (result) {
            if (result) {
                window.location = urlIndex + "?success=" + mensagem;
            } else {
                window.location = urlIndex + "?error=Registro inválido!";
            }
        }
    });
}
/* BancoQuestor - Fim */

/* Comando - Inicio */
var idComando = "";
var codigoComando = "";
var ativoComando = false;
function modalComando(objeto) {
    if (objeto !== null) {
        const comando = JSON.parse(objeto);
        idComando = comando.Id;
        codigoComando = comando.Codigo;
        ativoComando = comando.Ativo;
        $("#descricao").val(comando.Descricao);
        $("#sql").val(comando.Sql);
        $("#parametro").val(comando.Parametro);
        $("#tiposql").val(comando.TipoSql).selectpicker("refresh");
        $("#tipomovimento").val(comando.TipoMovimento).selectpicker("refresh");
        $("#departamento").val(comando.Departamento).selectpicker("refresh");
    }

    $("#comando-modal").modal("show");
    mensagemTitulo(idComando, "tituloComando", "Comando SQL");
}

function addComando() {
    var url = $("#urlAddComando").val();
    var urlIndex = $("#urlIndexComando").val();

    const descricao = $("#descricao").val();
    const sql = $("#sql").val();
    const parametro = $("#parametro").val();
    const tiposql = $("#tiposql").val();
    const tipomovimento = $("#tipomovimento").val();
    const departamento = $("#departamento").val();

    var comando = { Descricao: descricao, Sql: sql, Parametro: parametro, TipoSql: tiposql, TipoMovimento: tipomovimento, Departamento: departamento };
    var mensagem = "Adicionado!";
    if (idComando !== "") {
        url = $("#urlEditComando").val();
        comando = { Id: idComando, Codigo: codigoComando, Descricao: descricao, Sql: sql, Parametro: parametro, TipoSql: tiposql, TipoMovimento: tipomovimento, Departamento: departamento, Ativo: ativoComando };
        mensagem = "Atualizado!";
    }
    idComando = "";
    $.ajax({
        url: url,
        data: comando,
        dataType: "json",
        type: "post",
        success: function (result) {
            if (result) {
                window.location = urlIndex + "?success=" + mensagem;
            } else {
                window.location = urlIndex + "?error=Registro inválido!";
            }
        }
    });
}
/* Comando - Fim */


/* ComandoEmpresa - Inicio */
var listaComandoEmpresas = new Array();
var listaComandoEmpresasAux = new Array();

var comandoId = "";
var codigoComandoId = "";
function modalEmpresas(id, descricao, codigoComando) {
    const urlDados = $("#urlDadosComandoEmpresas").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: "json",
        type: "post",
        success: function (dados) {
            comandoId = id;
            codigoComandoId = codigoComando;
            listaComandoEmpresasAux = [];
            listaComandoEmpresas = [];

            $(dados).each(function (i) {
                listaComandoEmpresas.push({
                    Id: dados[i].id,
                    ComandoId: dados[i].comandoId,
                    CodigoEmpresa: dados[i].codigoEmpresa,
                    CodigoComando: dados[i].codigoComando,
                    NomeEmpresa: dados[i].nomeEmpresa,
                    OrdemExecucao: dados[i].ordemExecucao
                });
                listaComandoEmpresasAux.push({
                    Id: dados[i].id,
                    ComandoId: dados[i].comandoId,
                    CodigoEmpresa: dados[i].codigoEmpresa,
                    CodigoComando: dados[i].codigoComando,
                    NomeEmpresa: dados[i].nomeEmpresa,
                    OrdemExecucao: dados[i].ordemExecucao
                });
            });

            $("#comandoEmpresas-modal").modal("show");
            $("#textoComando").val(codigoComando + " - " + descricao);
            tableComandoEmpresas();
        }
    });
}

function addEmpresaComando() {
    var codigoEmpresa = $("#chosen-select").val();
    var validar = false;
    const textoEmpresa = $("#chosen-select option:selected").text();
    const descricaoEmpresa = textoEmpresa.split("-")[0];
    const ordemExecucao = $("#ordemExecucao").val();

    if (codigoEmpresa !== "") {
        $(listaComandoEmpresasAux).each(function () {
            if (this.CodigoEmpresa === codigoEmpresa) {
                validar = true;
                errorMessage("Empresa existente!", "Empresa Vinculado ao Comando");
            }
        });

        if (!validar) {
            listaComandoEmpresasAux.push({
                Id: "",
                ComandoId: comandoId,
                CodigoEmpresa: codigoEmpresa,
                CodigoComando: codigoComandoId,
                NomeEmpresa: descricaoEmpresa,
                OrdemExecucao: ordemExecucao
            });
            tableComandoEmpresas();
        }
    } else {
        errorMessage("Empresa inválida!", "Empresa Vinculado ao Comando");
    }
}

function tableComandoEmpresas() {
    var incremento = 0;
    $("#comandoEmpresasTable tbody tr").remove();

    $(listaComandoEmpresasAux).each(function () {
        const btnExcluir = '<a id="excluir"' + incremento + ' class="btn-link" onclick="deleteComandoEmpresa(' + incremento + ');" style="cursor: pointer"> Excluir</a>';

        const html = `<tr><td>${this.CodigoEmpresa}</td><td>${this.NomeEmpresa}</td><td>${this.OrdemExecucao}</td><td class="text-right">${btnExcluir}</td></tr>`;
        $(html).appendTo($("#comandoEmpresasTable"));
        incremento++;
    });
    $("#comandoEmpresasTable").trigger("footable_redraw");
}

function deleteComandoEmpresa(id) {
    listaComandoEmpresasAux.splice(id, 1);
    tableComandoEmpresas();
}

function saveComandoEmpresas() {
    const url = $("#urlAddComandoEmpresas").val();
    var urlIndex = $("#urlIndexComando").val();

    $.ajax({
        url: url,
        data: { listaAtual: listaComandoEmpresas, listaNova: listaComandoEmpresasAux },
        dataType: "json",
        type: "post",
        success: function (result) {
            if (result) {
                window.location = urlIndex + "?success=Adicionados!";
            } else {
                window.location = urlIndex + "?error=Registros inválidos!";
            }
        }
    });
}
/* ComandoEmpresa - Fim */


/* Consulta - Inicio */
function getConsultas() {
    const departamento = $("#departamento").val();
    const url = $("#urlComandos").val();

    if (departamento !== null) {
        $("#cs-multiselect").empty();

        $.getJSON(url, {
            departamento: departamento,
            tipoSql: 1,
            tipoMovimento: -1
        }, function (data) {
            $.each(data, function (i, sql) {
                $("#cs-multiselect").append(`<option value="${sql.value}">${sql.text}</option>`);
            });
            $("#cs-multiselect").trigger("chosen:updated");
        });
    }
}

function carregarParametros() {
    const urlParametros = $("#urlParametros").val();
    const comandosConsulta = $("#cs-multiselect").val();
    const empresa = $("#chosen-select").val();

    const node = document.getElementById("parametros-fields");
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }

    if (comandosConsulta.length > 0) {

        $.ajax({
        url: urlParametros,
        data: { sql: comandosConsulta, codigo: empresa },
        dataType: "json",
        type: "post",
        success: function (data) {
            var html = "";
            $(data).each(function (i) {
                html += data[i].html;
            });
            
            $(html).appendTo("#parametros-fields");

            $("#dp-txtinput input").datepicker();
            $("#parametros-modal").modal("show");
            mensagemModal("tituloParametros", "Parâmetros");
        }
        });

    }
}

function executarSql() {

    const valoresParametro = [];
    const camposParametro = [];

    const empresaConsulta = $("#chosen-select").val();
    const conexaoConsulta = $("#configuracao").val();
    const comandosConsulta = $("#cs-multiselect").val();

    const valores = document.getElementsByName("campoModal");
    for (let i = 0; i < valores.length; i++) {
        valoresParametro.push(valores[i].value);
    }
    
    const parametros = document.getElementsByName("labelModal");
    for (let j = 0; j < parametros.length; j++) {
        camposParametro.push(parametros[j].id);
    }

    $("#codigo").val(empresaConsulta);
    $("#comandos").val(comandosConsulta);
    $("#conexao").val(conexaoConsulta);
    $("#parametros").val(camposParametro);
    $("#valores").val(valoresParametro);

    $("#parametros-modal").modal("hide");

}
/* Consulta - Final */

/* Execucao - Inicio */
function getExecucoes() {
    const departamento = $("#departamento").val();
    const movimento = $("#tipoMovimento").val();
    const url = $("#urlComandos").val();

    if (departamento !== null) {
        $("#cs-multiselect").empty();

        $.getJSON(url, {
            departamento: departamento,
            tipoSql: 0,
            tipoMovimento: movimento
        }, function (data) {
            $.each(data, function (i, sql) {
                $("#cs-multiselect").append(`<option value="${sql.value}">${sql.text}</option>`);
            });
            $("#cs-multiselect").trigger("chosen:updated");
        });
    }
}
/* Execucao - Final */

/* Rotinas - Inicio */

var listaRotina = new Array();
var listaRotinaAux = new Array();

function getComandoEmpresa() {
    const departamento = $("#departamento").val();
    const url = $("#urlComandoEmpresa").val();
    const codigo = $("#chosen-select").val();
    const tipoMovimento = $("#tipoMovimento").val();

    listaRotina = [];
    listaRotinaAux = [];

    $.getJSON(url,
        {
            departamento: departamento,
            tipoSql: 3,
            tipoMovimento: tipoMovimento,
            codigo: codigo
        },
        function (data) {
            $.each(data,
                function (i) {
                    listaRotina.push({
                        Id: data[i].id,
                        CodigoEmpresa: data[i].codigoEmpresa,
                        ComandoId: data[i].comandoId,
                        CodigoComando: data[i].codigoComando,
                        OrdemExecucao: data[i].ordemExecucao,
                        NomeEmpresa: data[i].nomeEmpresa,
                        DescricaoComando: data[i].descricaoComando,
                        Sql: data[i].sql,
                        Parametro: data[i].parametro,
                        TipoSql: data[i].tipoSql,
                        TipoMovimento: data[i].tipoMovimento,
                        Departamento: data[i].departamento,
                        Ativo: data[i].ativo,
                        DescricaoDepartamento: data[i].descricaoDepartamento,
                        DataExecucao: data[i].DataExecucao,
                        UltimosParametros: data[i].ultimosParametros
                    });

                    listaRotinaAux.push({
                        Id: data[i].id,
                        CodigoEmpresa: data[i].codigoEmpresa,
                        ComandoId: data[i].comandoId,
                        CodigoComando: data[i].codigoComando,
                        OrdemExecucao: data[i].ordemExecucao,
                        NomeEmpresa: data[i].nomeEmpresa,
                        DescricaoComando: data[i].descricaoComando,
                        Sql: data[i].sql,
                        Parametro: data[i].parametro,
                        TipoSql: data[i].tipoSql,
                        TipoMovimento: data[i].tipoMovimento,
                        Departamento: data[i].departamento,
                        Ativo: data[i].ativo,
                        DescricaoDepartamento: data[i].descricaoDepartamento,
                        DataExecucao: data[i].dataExecucao,
                        UltimosParametros: data[i].ultimosParametros
                    });

                });
            tableRotina();
        });
}

function tableRotina() {
    $("#foo-pagination tbody tr").remove();

    $(listaRotinaAux).each(function () {
        const html = `<tr id='${this.ComandoId}'><td><label><input id='${this.ComandoId}' name="marcado" type="checkbox" checked = "checked" onclick="selecaoComando('${this.ComandoId}');"></label></td>` +
            `<td>${this.CodigoComando}</td><td>${this.DescricaoComando}</td>` +
            `<td>${this.DataExecucao}</td><td>${this.UltimosParametros}</td></tr>`;
        $('#foo-pagination tbody').append(html);
        $('#foo-pagination tr#' + this.ComandoId).css("font-weight", "bold");
    });
    $('#foo-pagination').footable();
    $('#foo-pagination').trigger('footable_initialized');

    $('#show-entries').change(function (e) {
        e.preventDefault();
        $('#foo-pagination').data('page-size', $('#show-entries').val());
        $('#foo-pagination').trigger('footable_initialized');
    });
    $('#foo-pagination').trigger('footable_initialized');
}

function selecaoComando(id) {
    var codigo = "";
    if ($("#foo-pagination tr td input#" + id).is(':checked')) {
        for (const i = 0; i < listaRotina.length - 1; i++) {
            if (listaRotina[i].ComandoId === id) {
                listaRotinaAux.push(listaRotina[i]);
            }
        }

        $("#foo-pagination tr").click(function () {
            codigo = $(this).attr('id');
            $('#foo-pagination tr#' + codigo).css("font-weight", "bold");
        });

    } else {
        const posicao = listaRotinaAux.indexOf(id);
        listaRotinaAux.splice(posicao, 1);

        $("#foo-pagination tr").click(function () {
            codigo = $(this).attr('id');
            $('#foo-pagination tr#' + codigo).css("font-weight", "");
        });
    }
}

function carregarParametrosRotina() {
    const node = document.getElementById("parametros-fields");
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }

    var sqls = [];
    if (listaRotinaAux.length > 0) {
        //var parametros = "";
        $(listaRotinaAux).each(function() {
            sqls.push(this.ComandoId);
            //parametros += this.Parametro;
        });
    }

    const urlParametros = $("#urlParametros").val();
    const empresa = $("#chosen-select").val();

    $.ajax({
        url: urlParametros,
        data: { sql: sqls, codigo: empresa },
        dataType: "json",
        type: "post",
        success: function (data) {
            var html = "";
            $(data).each(function (i) {
                html += data[i].html;
            });

            $(html).appendTo("#parametros-fields");

            $("#dp-txtinput input").datepicker();
            $("#parametros-modal").modal("show");
            mensagemModal("tituloParametros", "Parâmetros");
        }
    });


    //if (sqls.length > 0) {
    //        const urlParametros = $("#urlParametros").val();
    //        const empresa = $("#chosen-select").val();

    //        $.ajax({
    //            url: urlParametros,
    //            data: { sql: sqls, codigo: empresa },
    //            dataType: "json",
    //            type: "post",
    //            success: function (data) {
    //                var html = "";
    //                $(data).each(function (i) {
    //                    html += data[i].html;
    //                });

    //                $(html).appendTo("#parametros-fields");

    //                $("#dp-txtinput input").datepicker();
    //                $("#parametros-modal").modal("show");
    //                mensagemModal("tituloParametros", "Parâmetros");
    //            }
    //        });

    //    }
    //parametros = parametros.slice(0, -1);

    //var listaParametros = parametros.split(";");
    //listaParametros = listaParametros.filter(function(x, y) {
    //    return listaParametros.indexOf(x) === y;
    //});

    //var html = "";
    //$(listaParametros).each(function (i) {

    //    if (listaParametros[i] !== "CODIGOEMPRESA") {
    //        if (listaParametros[i] === "DATAINICIAL" || listaParametros[i] === "DATAFINAL") {
    //            html += '<div class="form-group">' +
    //                `<label class="col-lg-3 control-label text-semibold" id="${listaParametros[i]}" name="labelModal">${listaParametros[i]}</label>` +
    //                '<div class="col-lg-8">' +
    //                '<div id="dp-txtinput">' +
    //                `<input type="text" class="form-control" name="campoModal" placeholder="${listaParametros[i]}" />` +
    //                "</div>" +
    //                "</div>" +
    //                "</div>";
    //        } else {
    //            html += '<div class="form-group">' +
    //                `<label class="col-lg-3 control-label text-semibold" id="${listaParametros[i]}" name="labelModal">${listaParametros[i]}</label>` +
    //                '<div class="col-lg-8">' +
    //                `<input type="text" class="form-control" name="campoModal" placeholder="${listaParametros[i]}" />` +
    //                "</div>" +
    //                "</div>";
    //        }
    //    }
    //});
}

function executarRotina() {

    const valoresParametro = [];
    const camposParametro = [];

    const empresaConsulta = $("#chosen-select").val();
    const conexaoConsulta = $("#configuracao").val();

    const valores = document.getElementsByName("campoModal");
    for (let i = 0; i < valores.length; i++) {
        valoresParametro.push(valores[i].value);
    }

    const comandosRotina = JSON.stringify(listaRotinaAux);


    const parametros = document.getElementsByName("labelModal");
    for (let j = 0; j < parametros.length; j++) {
        camposParametro.push(parametros[j].id);
    }

    $("#codigo").val(empresaConsulta);
    $("#comandos").val(comandosRotina);
    $("#conexao").val(conexaoConsulta);
    $("#parametros").val(camposParametro);
    $("#valores").val(valoresParametro);

    $("#parametros-modal").modal("hide");

}
/* Rotinas - Final */