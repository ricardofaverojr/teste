﻿function addOption(nome, texto, valor) {
    var select = document.getElementById(nome);
    select.options[select.options.length] = new Option(texto, valor, false, false);
}

function removeAllOptions(nome) {
    var select = document.getElementById(nome);
    select.options.length = 0;
}

function maskInscricao() {
    var tipoinscricao = $('#tipoinscr').val();
    $('#inscrfederal').mask('');
    if (tipoinscricao === '0') {
        $('#inscrfederal').mask('999.999.999-99');
    }
    if (tipoinscricao === '1') {
        $('#inscrfederal').mask('99.999.999/9999-99');
    }
}

function buscarRfb() {
    var url = $('#urlBuscarRfb').val();
    var inscricao = $('#cnpj').val();

    $.ajax({
        url: url,
        data: { inscricao },
        dataType: 'json',
        type: 'POST',
        success: function (data) {

            $('#razaosocial').val(data.nome);
            $('#datafundacao').val(data.abertura);
            $('#tipoinscr').val('1').selectpicker("refresh");
            $('#inscrfederal').val(data.cnpj);


            $('#razaosocial').focus();

            $('#rfb-modal').modal('hide');
        }
    });
}

function mensagemTitulo(id, cadastro, texto) {
    var mensagem = `Adicionar ${texto}`;
    if (id !== "") {
        mensagem = `Atualizar ${texto}`;    
    }
    $(`#${cadastro}`).text(mensagem);
}

function mensagemModal(idTitulo, texto) {
    $(`#${idTitulo}`).text(texto);
}