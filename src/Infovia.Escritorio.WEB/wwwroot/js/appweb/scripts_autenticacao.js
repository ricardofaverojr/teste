﻿
/* Usuario - Inicio */
var usuario;

function modalUsuario(id, nome) {
    var urlDados = $("#urlDadosUsuario").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function (objeto) {

            $('#usuario-modal').modal('show');
            $('#tituloUsuario').text(nome);
            if (objeto !== null) {
                $('#ramal').val(objeto.ramal);
                $('#icone').val(objeto.icone).selectpicker("refresh");
                usuario = {
                    Id: objeto.id,
                    Codigo: objeto.codigo,
                    Nome: objeto.nome,
                    Email: objeto.email,
                    Ramal: objeto.ramal,
                    Icone: objeto.icone
                };
            }
        }
    });
}

function saveUsuario() {
    var url = $('#urlEditUsuario').val();
    var urlIndex = $('#urlIndexUsuario').val();

    usuario.Departamento = $('#departamento').val();
    usuario.Ramal = $('#ramal').val();
    usuario.Icone = $('#icone').val();

    $.ajax({
        url: url,
        data: usuario,
        dataType: 'json',
        type: 'post',
        success: function (result) {
            if (result) {
                window.location = urlIndex + '?success=Atualizado!';
            } else {
                window.location = urlIndex + '?error=Registro inválidos!';
            }
        }
    });
}
/* Usuario - Fim */


/* GrupoUsuario - Inicio */
function modalGrupoUsuario(id, descricao) {
    var urlDados = $("#urlDadosUsuarios").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function (dados) {

            $('#grupousuario-modal').modal('show');
            $('#tituloGrupo').text(descricao);

            $('#usuariosTable tbody tr').remove();

            $(dados).each(function () {
                var html = '<tr><td>' + this.usuarioid + '</td><td>' + this.nomeusuario + '</td></tr>';
                $(html).appendTo($('#usuariosTable'));
            });
            $('#usuariosTable').trigger('footable_redraw');
        }
    });
}

function modalUsuarioGrupo(id, nome) {
    var urlDados = $("#urlDadosGruposUsuario").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function (dados) {

            $('#usuariogrupo-modal').modal('show');
            $('#tituloUsuarioGrupo').text(nome);

            $('#gruposTable tbody tr').remove();

            $(dados).each(function () {
                var html = '<tr><td>' + this.grupoid + '</td><td>' + this.nomegrupo + '</td></tr>';
                $(html).appendTo($('#gruposTable'));
            });
            $('#gruposTable').trigger('footable_redraw');
        }
    });
}

var idSincUsuario = "";
function modalSincronizarUsuario(id, nomeUsuario) {
    idSincUsuario = id;
    $('#sincronizarusuario-modal').modal('show');
    $('#tituloSincUsuario').text(`Ativar usuário ${nomeUsuario}`);
}

function sincronizarUsuario() {
    var urlRetorno = $('#urlIndexUsuario').val();
    var urlSincronizarUsuario = $('#urlSincronizarUsuario').val();

    console.log(urlSincronizarUsuario);
    console.log(idSincUsuario);

    var role = $('#role').val();
    $.ajax({
        url: urlSincronizarUsuario,
        data: {
            id: idSincUsuario,
            selectedRole: role
        },
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result) {
                window.location =
                    urlRetorno + '?success=Usuário Sincronizado!';
            } else {
                window.location = urlRetorno + '?error=Sincronização Cancelada!';
            }
        }
    });
}
/* GrupoUsuario - Fim */

/* GrupoEmpresa - Inicio */
function modalEmpresaGrupo(id, nome) {
    var urlDados = $("#urlDadosGruposEmpresa").val();
    $.ajax({
        url: urlDados,
        data: { id },
        dataType: 'json',
        type: 'post',
        success: function (dados) {
            $('#empresagrupo-modal').modal('show');
            $('#tituloGrupoEmpresa').text(nome);

            $('#gruposTable tbody tr').remove();

            $(dados).each(function () {
                var html = '<tr><td>' + this.codigoGrupo + '</td><td>' + this.nomeGrupo + '</td><td>' + this.descricaoDepartamento + '</td></tr>';
                $(html).appendTo($('#gruposTable'));
            });
            $('#gruposTable').trigger('footable_redraw');
        }
    });
}
/* GrupoEmpresa - Inicio */