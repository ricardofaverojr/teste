﻿function successMessage(message, title) {
    $.niftyNoty({
        type: 'success',
        container: 'floating',
        title: '<strong>' + title + '</strong>',
        message: message,
        closeBtn: false,
        timer: 5000
    });
}

function errorMessage(message, title) {
    $.niftyNoty({
        type: 'danger',
        container: 'floating',
        title: '<strong>' + title + '</strong>',
        message: message,
        closeBtn: false,
        timer: 5000
    });
};


function deleteItem(id, item, descricaoItem) {
    if (id != null) {
        var url = $('#deleteItem-' + id).data('request-url');
        bootbox.confirm('Confirma a exclusão do ' + item + ' ' + descricaoItem + '?', function (result) {
            if (result) {
                window.location.href = url;
            } else {
                $.niftyNoty({
                    type: 'danger',
                    icon: 'pli-cross icon-2x',
                    message: 'Remoção cancelada!',
                    container: 'floating',
                    timer: 5000
                });
            }
        });
    }
}

function desvincularItem(id, nome) {
    var url = $('#desvincular-' + id).data('request-url');
    bootbox.confirm(`Confirma a exclusão do vinculo da empresa ${nome}?`, function (result) {
        if (result) {
            window.location.href = url;
        } else {
            $.niftyNoty({
                type: 'danger',
                icon: 'pli-cross icon-2x',
                message: 'Operação cancelada!',
                container: 'floating',
                timer: 5000
            });
        }
    });
}