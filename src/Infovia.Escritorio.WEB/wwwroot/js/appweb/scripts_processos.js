﻿function modalAjudaParametros() {
    $("#ajudaparametros-modal").modal("show");
}


function carregarProcessos() {
    $("#chosen-select1").empty();
    $("#chosen-select1").append(`<option value="$">Selecione</option>`);
    const empresa = $("#chosen-select").val();
    const departamento = $("#departamento").val();
    const url = $("#urlGetProcessos").val();

    $.getJSON(url, {
        empresa: empresa,
        departamento: departamento
    }, function (data) {
        $.each(data, function (i, proc) {
            $("#chosen-select1").append(`<option value="${proc.value}">${proc.text}</option>`);
        });
        $("#chosen-select1").trigger("chosen:updated");
    });
}

function carregarParametroProcesso() {

    const urlParametros = $("#urlParametros").val();
    const processoId = $("#chosen-select1").val();
    const codigoEmpresa = $("#chosen-select").val();
    const node = document.getElementById("parametros-fields");

    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }

    if (processoId !== "" && codigoEmpresa !== "") {

        $.ajax({
            url: urlParametros,
            data: { id: processoId, codigo: codigoEmpresa },
            dataType: "json",
            type: "post",
            success: function (data) {
                var html = "";
                document.getElementById("descricaoProcesso").innerHTML = data.descricao;

                $(data.result).each(function (i) {
                    html += data.result[i].html;
                });

                $(html).appendTo("#parametros-fields");

                $("#dp-txtinput input").datepicker();
                $("#parametros-modal").modal("show");
                mensagemModal("tituloParametros", "Parâmetros");
            }
        });

    }
}

function executarProcesso() {

    const valoresParametro = [];
    const camposParametro = [];

    const empresa = $("#chosen-select").val();
    const conexao = $("#configuracao").val();
    const processo = $("#chosen-select1").val();

    const valores = document.getElementsByName("campoModal");

    var valorCheck = false;
    for (let i = 0; i < valores.length; i++) {
        if (valores[i].type === "checkbox") {
            if (valorCheck === false) {
                valoresParametro.push(`${valores[i].id}=${valores[i].checked}`);
            } else {
                valoresParametro[valoresParametro.length - 1] = `${valoresParametro[valoresParametro.length - 1]};${valores[i].id}=${valores[i].checked}`;
            }
            valorCheck = true;
        } else {
            valorCheck = false;
            valoresParametro.push(valores[i].value);
        }
    }

    const parametros = document.getElementsByName("labelModal");
    for (let j = 0; j < parametros.length; j++) {
        camposParametro.push(parametros[j].id);
    }

    $("#codigo").val(empresa);
    $("#processo").val(processo);
    $("#conexao").val(conexao);
    $("#parametros").val(camposParametro);
    $("#valores").val(valoresParametro);

    $("#parametros-modal").modal("hide");

}



/* Desconto Sindical - Inicio */

var idDescontoSindical = "";
function modalDescontoSindical(objeto) {
    $("#descontosindical-modal").modal("show");
    mensagemTitulo(idDescontoSindical, "tituloDescontoSindical", "Desconto Sindical");
}

/* Desconto Sindical - Fim */

/* Analise - Inicio */
var idAnalise = "";
function modalAnalises(objeto) {
    if (objeto !== null) {
        const analise = JSON.parse(objeto);
        idAnalise = analise.Id;
        $("#descricao").val(analise.Descricao);
    }

    $("#analise-modal").modal("show");
    mensagemTitulo(idAnalise, "tituloAnalise", "Análise Padrão");
}

function addAnalise() {
    var url = $("#urlAddAnalise").val();
    var urlIndex = $("#urlIndexAnalise").val();

    const descricao = $("#descricao").val();

    var analise = { Descricao: descricao };
    var mensagem = "Adicionado!";
    if (idAnalise !== "") {
        url = $("#urlEditAnalise").val();
        analise = { Id: idAnalise, Descricao: descricao };
        mensagem = "Atualizado!";
    }
    idAnalise = "";
    $.ajax({
        url: url,
        data: analise,
        dataType: "json",
        type: "post",
        success: function (result) {
            if (result) {
                window.location = urlIndex + "?success=" + mensagem;
            } else {
                window.location = urlIndex + "?error=Registro inválido!";
            }
        }
    });
}

function modalPlanoPadrao(id, descricao) {
    idAnalise = id;
    $("#plano-modal").modal("show");
    mensagemModal("tituloPlano", descricao);
}

idAnalisePadrao = "";
function modalNovaConta(objeto) {
    if (objeto !== null) {
        const conta = JSON.parse(objeto);
        idAnalisePadrao = analise.Id;
        $("#tipoconta").val(analise.TipoConta);
        $("#classificao").val(analise.Classificacao);
        $("#descricao").val(analise.Descricao);
        $("#operador").val(analise.Operador);
    }

    $("#novaconta-modal").modal("show");
    mensagemTitulo(idAnalisePadrao, "tituloNovaConta", "Conta");
}

valorSeq = 0;
function addContaFormula() {
    var tipoconta = $("#tipocontaformula").val();
    var contactb = $("#chosen-select").val();
    var descrconta = $("#chosen-select option:selected").text();
    var valorAnalise = $("#valorformula").val();
    var operador = $("#operadorformula").val();
    valorSeq++;






    if (codigoEmpresa !== "") {
        $(listaComandoEmpresasAux).each(function () {
            if (this.CodigoEmpresa === codigoEmpresa) {
                validar = true;
                errorMessage("Empresa existente!", "Empresa Vinculado ao Comando");
            }
        });

        if (!validar) {
            listaComandoEmpresasAux.push({
                Id: "",
                ComandoId: comandoId,
                CodigoEmpresa: codigoEmpresa,
                CodigoComando: codigoComandoId,
                NomeEmpresa: descricaoEmpresa,
                OrdemExecucao: ordemExecucao
            });
            tableComandoEmpresas();
        }
    } else {
        errorMessage("Empresa inválida!", "Empresa Vinculado ao Comando");
    }

}


/* Analise - Fim */