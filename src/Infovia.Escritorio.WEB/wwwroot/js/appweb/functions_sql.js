﻿//comandos sql
function pegaparametros() {
    var comando = $('#sql').val();
    var quebraFrase = comando.split(' ');
    var novaFrase = '';
    for (var i = 0; i < quebraFrase.length; i++) {
        if (quebraFrase[i].indexOf('@') !== -1) {
            if (novaFrase.indexOf(quebraFrase[i].replace('@', '').replace(';', '')) === -1) {
                novaFrase = novaFrase + quebraFrase[i].replace('@', '').replace(';', '') + ';';
            }
        }
    }

    novaFrase = novaFrase.replace(/'/g, '');
    $('#parametro').val(novaFrase.toUpperCase());
    //$('#ParametrosHidden').val(novaFrase.toUpperCase());
}

function verificaTipoSql() {
    var comando = $('#Sql').val();
    console.log(comando);
    if (comando.toUpperCase().match(/SELECT/)) {
        console.log(comando);
        $('#TipoSql').val(1);
    }
}

function replaceAll(str, de, para) {
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}