﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infovia.NugetLibrary.DevPack.Application;
using Infovia.NugetLibrary.DevPack.Application.Interfaces;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Controllers
{
    public class BaseController<TViewModel, TAppService> : Controller where TViewModel : ViewModel where TAppService : IAppService<TViewModel>
    {
        private readonly DomainNotificationHandler _notifications;
        private readonly IMediatorHandler _mediatorHandler;
        private readonly TAppService _service;

        public BaseController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, TAppService service)
        {
            _notifications = (DomainNotificationHandler)notifications;
            _mediatorHandler = mediatorHandler;
            _service = service;
        }

        protected bool IsValid() => !_notifications.HasNotifications();

        protected IEnumerable<string> GetErrorMessages() => _notifications.GetNotifications().Select(e => e.Value).ToList();

        protected void NotifyError(string code, string message) => _mediatorHandler.PostNotification(new DomainNotification(code, message));


        public virtual IActionResult Index(string success, string error)
        {
            LoadViewBag(success, error);
            return View(_service.GetAll());
        }

        public virtual void LoadViewBag(string success, string error)
        {
            if (!string.IsNullOrEmpty(success))
                ViewData["Success"] = success;

            if (!string.IsNullOrEmpty(error))
                ViewData["Error"] = error;
        }

        public string Get()
        {
            var search = "";
            if (TempData.Peek("search") != null)
            {
                string text = HttpContext.Request.Query["search"];
                if (text == null)
                    search = TempData.Peek("search").ToString();
                else
                {
                    search = text;
                    TempData["search"] = text;
                    TempData["page"] = 0;
                }
            }
            else
            {
                search = HttpContext.Request.Query["search"];
                TempData["search"] = search;
            }
            return search;
        }

    }
}
