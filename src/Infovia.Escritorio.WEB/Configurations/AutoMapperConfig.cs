﻿using System;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Infovia.Escritorio.WEB.Configurations.AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Infovia.Escritorio.WEB.Configurations
{
    public static class AutoMapperConfig
    {
        public static IServiceCollection AddMapperConfig(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg => cfg.AddExpressionMapping(), typeof(MapperProfile));
            return services;
        }
    }
}
