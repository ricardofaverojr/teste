﻿using Infovia.Escritorio.CRM.Application.Contatos;
using Infovia.Escritorio.CRM.Application.Contatos.Interfaces;
using Infovia.Escritorio.CRM.Data.Contatos;
using Infovia.Escritorio.CRM.Data.Context;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Infovia.Escritorio.CRM.Domain.Contatos.Interfaces;
using Infovia.Escritorio.Processo.Application.Analises;
using Infovia.Escritorio.Processo.Application.Analises.Interfaces;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos.Interfaces;
using Infovia.Escritorio.Processo.Application.LeiautesContabeis;
using Infovia.Escritorio.Processo.Application.LeiautesContabeis.Interfaces;
using Infovia.Escritorio.Processo.Application.PlanosReferencia;
using Infovia.Escritorio.Processo.Application.PlanosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados.Interfaces;
using Infovia.Escritorio.Processo.Application.TabelasContribuicao;
using Infovia.Escritorio.Processo.Application.TabelasContribuicao.Interfaces;
using Infovia.Escritorio.Processo.Application.TabelasFiscal;
using Infovia.Escritorio.Processo.Application.TabelasFiscal.Interfaces;
using Infovia.Escritorio.Processo.Data.Analises;
using Infovia.Escritorio.Processo.Data.Context;
using Infovia.Escritorio.Processo.Data.HistoricosReferencia;
using Infovia.Escritorio.Processo.Data.ImpostosRetidos;
using Infovia.Escritorio.Processo.Data.LeiautesContabeis;
using Infovia.Escritorio.Processo.Data.PlanosReferencia;
using Infovia.Escritorio.Processo.Data.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Data.TabelasContribuicao;
using Infovia.Escritorio.Processo.Data.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.Analises.Interfaces;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos.Interfaces;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis.Interfaces;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia.Interfaces;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados.Interfaces;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao.Interfaces;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal.Interfaces;
using Infovia.Escritorio.Questor.Applications;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.ApplicationsQuestor;
using Infovia.Escritorio.Questor.ServicesCache;
using Infovia.Escritorio.Questor.ServicesCache.Interfaces;
using Infovia.Escritorio.Questor.ServicesQuestor;
using Infovia.Escritorio.Questor.ServicesQuestor.Interfaces;
using Infovia.Escritorio.SQL.Application.BancosQuestor;
using Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces;
using Infovia.Escritorio.SQL.Application.Comandos;
using Infovia.Escritorio.SQL.Application.Comandos.Interfaces;
using Infovia.Escritorio.SQL.Application.Modelos;
using Infovia.Escritorio.SQL.Application.Modelos.Interfaces;
using Infovia.Escritorio.SQL.Data.BancosQuestor;
using Infovia.Escritorio.SQL.Data.Comandos;
using Infovia.Escritorio.SQL.Data.Context;
using Infovia.Escritorio.SQL.Data.Modelos;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.BancosQuestor.Interfaces;
using Infovia.Escritorio.SQL.Domain.Comandos;
using Infovia.Escritorio.SQL.Domain.Comandos.Interfaces;
using Infovia.Escritorio.SQL.Domain.Modelos;
using Infovia.Escritorio.SQL.Domain.Modelos.Interfaces;
using Infovia.NugetLibrary.DevPack.Mediator;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Infovia.Escritorio.WEB.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            // Mediator
            services.AddScoped<IMediatorHandler, MediatorHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            // Notifications
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();

            // Questor
            services.AddScoped<IConnectionQuestor, ConnectionQuestor>();

            services.AddTransient<IDepartamentoServiceQuestor, DepartamentoServiceQuestor>();
            services.AddTransient<IEmpresaServiceQuestor, EmpresaServiceQuestor>();
            services.AddTransient<IGrupoServiceQuestor, GrupoServiceQuestor>();
            services.AddTransient<IEstabelecimentoServiceQuestor, EstabelecimentoServiceQuestor>();
            services.AddTransient<IGrupoEmpresaServiceQuestor, GrupoEmpresaServiceQuestor>();
            services.AddTransient<IGrupoProdutoServiceQuestor, GrupoProdutoServiceQuestor>();
            services.AddTransient<IGrupoUsuarioServiceQuestor, GrupoUsuarioServiceQuestor>();
            services.AddTransient<IUsuarioServiceQuestor, UsuarioServiceQuestor>();
            services.AddTransient<IPlanoEspecificoServiceQuestor, PlanoEspecificoServiceQuestor>();
            services.AddTransient<IHistoricoContabilServiceQuestor, HistoricoContabilServiceQuestor>();

            services.AddTransient<IDepartamentoServiceCache, DepartamentoServiceCache>();
            services.AddTransient<IEmpresaServiceCache, EmpresaServiceCache>();
            services.AddTransient<IGrupoServiceCache, GrupoServiceCache>();
            services.AddTransient<IEstabelecimentoServiceCache, EstabelecimentoServiceCache>();
            services.AddTransient<IGrupoEmpresaServiceCache, GrupoEmpresaServiceCache>();
            services.AddTransient<IGrupoUsuarioServiceCache, GrupoUsuarioServiceCache>();
            services.AddTransient<IUsuarioServiceCache, UsuarioServiceCache>();

            services.AddScoped<ISyncCacheAppService, SyncCacheAppService>();
            services.AddScoped<IFiscalAppService, FiscalAppService>();
            services.AddScoped<ICrmAppService, CrmAppService>();
            services.AddScoped<IContabilAppService, ContabilAppService>();

            // SQL
            services.AddScoped<SqlContext>();
            services.AddScoped<IBancoQuestorRepository, BancoQuestorRepository>();
            services.AddScoped<IBancoQuestorDapper, BancoQuestorDapper>();
            services.AddScoped<IBancoQuestorService, BancoQuestorService>();
            services.AddScoped<IBancoQuestorAppService, BancoQuestorAppService>();

            services.AddScoped<IComandoRepository, ComandoRepository>();
            services.AddScoped<IComandoDapper, ComandoDapper>();
            services.AddScoped<IComandoService, ComandoService>();
            services.AddScoped<IComandoAppService, ComandoAppService>();

            services.AddScoped<IModeloRepository, ModeloRepository>();
            services.AddScoped<IModeloDapper, ModeloDapper>();
            services.AddScoped<IModeloService, ModeloService>();
            services.AddScoped<IModeloAppService, ModeloAppService>();


            // PROCESSO
            services.AddScoped<ProcessoContext>();
            services.AddScoped<IHistoricoReferenciaRepository, HistoricoReferenciaRepository>();
            services.AddScoped<IHistoricoReferenciaDapper, HistoricoReferenciaDapper>();
            services.AddScoped<IHistoricoReferenciaService, HistoricoReferenciaService>();
            services.AddScoped<IHistoricoReferenciaAppService, HistoricoReferenciaAppService>();

            services.AddScoped<IImpostoRetidoRepository, ImpostoRetidoRepository>();
            services.AddScoped<IImpostoRetidoDapper, ImpostoRetidoDapper>();
            services.AddScoped<IImpostoRetidoService, ImpostoRetidoService>();
            services.AddScoped<IImpostoRetidoAppService, ImpostoRetidoAppService>();

            services.AddScoped<ILeiauteContabilRepository, LeiauteContabilRepository>();
            services.AddScoped<ILeiauteContabilDapper, LeiauteContabilDapper>();
            services.AddScoped<ILeiauteContabilService, LeiauteContabilService>();
            services.AddScoped<ILeiauteContabilAppService, LeiauteContabilAppService>();

            services.AddScoped<IPlanoReferenciaRepository, PlanoReferenciaRepository>();
            services.AddScoped<IPlanoReferenciaDapper, PlanoReferenciaDapper>();
            services.AddScoped<IPlanoReferenciaService, PlanoReferenciaService>();
            services.AddScoped<IPlanoReferenciaAppService, PlanoReferenciaAppService>();

            services.AddScoped<ITabelaContribuicaoRepository, TabelaContribuicaoRepository>();
            services.AddScoped<ITabelaContribuicaoDapper, TabelaContribuicaoDapper>();
            services.AddScoped<ITabelaContribuicaoService, TabelaContribuicaoService>();
            services.AddScoped<ITabelaContribuicaoAppService, TabelaContribuicaoAppService>();

            services.AddScoped<ITabelaFiscalRepository, TabelaFiscalRepository>();
            services.AddScoped<ITabelaFiscalDapper, TabelaFiscalDapper>();
            services.AddScoped<ITabelaFiscalService, TabelaFiscalService>();
            services.AddScoped<ITabelaFiscalAppService, TabelaFiscalAppService>();

            services.AddScoped<IProcessoPersonalizadoRepository, ProcessoPersonalizadoRepository>();
            services.AddScoped<IProcessoPersonalizadoDapper, ProcessoPersonalizadoDapper>();
            services.AddScoped<IProcessoPersonalizadoService, ProcessoPersonalizadoService>();
            services.AddScoped<IProcessoPersonalizadoAppService, ProcessoPersonalizadoAppService>();

            services.AddScoped<IAnaliseRepository, AnaliseRepository>();
            services.AddScoped<IAnaliseDapper, AnaliseDapper>();
            services.AddScoped<IAnaliseService, AnaliseService>();
            services.AddScoped<IAnaliseAppService, AnaliseAppService>();

            services.AddScoped<IAnalisePadraoRepository, AnalisePadraoRepository>();
            services.AddScoped<IAnalisePadraoDapper, AnalisePadraoDapper>();
            services.AddScoped<IAnalisePadraoService, AnalisePadraoService>();
            services.AddScoped<IAnalisePadraoAppService, AnalisePadraoAppService>();

            services.AddScoped<IAnaliseEspecificaRepository, AnaliseEspecificaRepository>();
            services.AddScoped<IAnaliseEspecificaDapper, AnaliseEspecificaDapper>();
            services.AddScoped<IAnaliseEspecificaService, AnaliseEspecificaService>();
            services.AddScoped<IAnaliseEspecificaAppService, AnaliseEspecificaAppService>();

            // CRM
            services.AddScoped<CrmContext>();
            services.AddScoped<IContatoRepository, ContatoRepository>();
            services.AddScoped<IContatoDapper, ContatoDapper>();
            services.AddScoped<IContatoService, ContatoService>();
            services.AddScoped<IContatoAppService, ContatoAppService>();

            return services;
        }
    }
}
