﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.SQL.Application.Comandos;
using Infovia.Escritorio.SQL.Application.Comandos.Interfaces;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.SQL.Controllers
{
    [Area("SQL")]
    public class ComandoController : BaseController<ComandoViewModel, IComandoAppService>
    {
        private readonly IComandoAppService _service;
        private readonly ICrmAppService _crm;

        public ComandoController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IComandoAppService service, ICrmAppService crm) : base(notifications, mediatorHandler, service)
        {
            _service = service;
            _crm = crm;
        }

        [HttpPost]
        public JsonResult Add(ComandoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(ComandoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Update(entity));

        [HttpGet]
        public virtual IActionResult Delete(Guid id) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!" })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!" });

        public IActionResult Ativar(Guid id) =>
            _service.AtivarDesativar(id)
                ? RedirectToAction("Index", new { success = "Atualizado!" })
                : RedirectToAction("Index", new { error = "Atualização cancelada!" });

        public JsonResult GetComandoEmpresas(Guid id) => Json(_service.GetEmpresaByComando(id));

        public JsonResult AddComandoEmpresas(IList<ComandoEmpresaViewModel> listaAtual, IList<ComandoEmpresaViewModel> listaNova)
        {
            var comandoEmpresasAtual = listaAtual.Select(x => new { x.Id, x.ComandoId, x.CodigoEmpresa, x.CodigoComando, x.OrdemExecucao }).ToList();
            var comandoEmpresasNova = listaNova.Select(x => new { x.Id, x.ComandoId, x.CodigoEmpresa, x.CodigoComando, x.OrdemExecucao }).ToList();

            var listaIncluir = comandoEmpresasNova.Except(comandoEmpresasAtual).Select(x => new ComandoEmpresaViewModel()
            {
                CodigoEmpresa = x.CodigoEmpresa,
                ComandoId = x.ComandoId,
                OrdemExecucao = x.OrdemExecucao
            }).ToList();
            var listaExcluir = comandoEmpresasAtual.Except(comandoEmpresasNova).Select(x => x.Id).ToList();

            return Json(_service.UpdateEmpresa(listaIncluir, listaExcluir));
        }

        public override void LoadViewBag(string success, string error)
        {
            ViewBag.Empresas = _crm.GetEmpresasAtivas();
            ViewBag.Departamentos = _crm.GetDepartamentos();
            base.LoadViewBag(success, error);
        }
    }
}
