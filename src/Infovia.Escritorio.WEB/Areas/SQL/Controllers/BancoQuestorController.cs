﻿using System;
using Infovia.Escritorio.SQL.Application.BancosQuestor;
using Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.SQL.Controllers
{
    [Area("SQL")]
    public class BancoQuestorController : BaseController<BancoQuestorViewModel, IBancoQuestorAppService>
    {
        private readonly IBancoQuestorAppService _service;

        public BancoQuestorController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IBancoQuestorAppService service) : base(notifications, mediatorHandler, service)
        {
            _service = service;
        }

        [HttpPost]
        public JsonResult Add(BancoQuestorViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(BancoQuestorViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Update(entity));

        [HttpGet]
        public virtual IActionResult Delete(Guid id) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!" })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!" });

        public IActionResult Padrao(Guid id)
        {
            _service.Padrao(id);
            return RedirectToAction("Index", new { success = "Banco padrão definido!" });
        }

        public IActionResult Teste(Guid id) =>
            _service.Teste(id)
                ? RedirectToAction("Index", new { success = "Conexão efetuada com sucesso!" })
                : RedirectToAction("Index", new { error = "Não foi possível efetuar a conexão!" });
    }
}
