﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces;
using Infovia.Escritorio.SQL.Application.Comandos;
using Infovia.Escritorio.SQL.Application.Comandos.Interfaces;
using Infovia.Escritorio.SQL.Enums;
using Infovia.NugetLibrary.Firebird.Models;
using InfoviaFuncaoUtils;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace Infovia.Escritorio.WEB.Areas.SQL.Controllers
{
    [Area("SQL")]
    public class SqlController : Controller
    {
        private readonly IComandoAppService _comando;
        private readonly IBancoQuestorAppService _bancoQuestor;
        private readonly ICrmAppService _crm;
        //private readonly IUser _user;
        //private readonly IUsuarioAppService _usuario;
        //private readonly IParametroFunction _parametro;
        //private readonly IEstabResumoData _estab;

        public SqlController(IComandoAppService comando, IBancoQuestorAppService bancoQuestor)
        {
            _comando = comando;
            _bancoQuestor = bancoQuestor;
        }

        public IActionResult Consulta()
        {
            Load();
            return View();
        }
        public IActionResult Execucao(string result)
        {
            ViewBag.Resultado = !string.IsNullOrEmpty(result) ? result.Split(";") : null;
            Load();
            return View();
        }
        public IActionResult Rotina(string result)
        {
            ViewBag.Resultado = !string.IsNullOrEmpty(result) ? result.Split(";") : null;
            Load();
            return View();
        }

        public IActionResult Index(string success, string error, string datainicial, string datafinal)
        {
            if (!string.IsNullOrEmpty(success))
                ViewBag.Success = success;
            if (!string.IsNullOrEmpty(error))
                ViewBag.Error = error;


            var logs = new List<LogViewModel>();
            if (!string.IsNullOrEmpty(datainicial) && !string.IsNullOrEmpty(datafinal))
            {
                var dtInicio = Convert.ToDateTime(datainicial);
                var dtFinal = Convert.ToDateTime(datafinal);

                if (dtInicio <= dtFinal)
                    logs = _comando.GetLogByPredicate(x => x.DataCadastro >= dtInicio && x.DataCadastro <= dtFinal).ToList();
                else
                    return RedirectToAction("Index", new { error = "Período inválido!" });
            }
            return View(logs);
        }

        #region Funcoes Comandos
        public JsonResult GetComandos(int departamento, int tipoMovimento, int tipoSql)
        {
            var comandos = tipoMovimento < 0 ? _comando.GetByPredicate(x => x.Departamento == departamento && x.TipoSql == (ESql)tipoSql && x.Ativo) : _comando.GetByPredicate(x => x.Departamento == departamento && x.TipoMovimento == (EMovimento)tipoMovimento && x.TipoSql == (ESql)tipoSql && x.Ativo);
            var lista = from x in comandos.OrderBy(x => x.Codigo)
                        select new
                        {
                            Value = x.Id,
                            Text = $"{x.Codigo} - {x.Descricao}"
                        };
            return Json(lista.ToList());
        }

        //public JsonResult GetParametros(List<Guid> sql, int codigo)
        //{
        //    var comandos = ListaComandos(sql);
        //    var listaParametros = new List<ParametroViewModel>();
        //    if (comandos.Count > 0)
        //    {
        //        var lista = ListaParametros(comandos);
        //        listaParametros = _parametro.GetParametros(lista, codigo);
        //    }
        //    return Json(listaParametros);
        //}

        //public JsonResult GetComandosEmpresa(int codigo, int departamento, int tipoMovimento)
        //{
        //    var comandos = _comando.GetEmpresaByCodEmpresa(codigo).ToList();
        //    comandos = comandos.Where(x => x.Departamento == departamento && x.TipoMovimento == (EMovimento)tipoMovimento && x.TipoSql == ESql.RotinaEmpresa && x.Ativo).OrderBy(x => x.OrdemExecucao).ToList();
        //    foreach (var comando in comandos)
        //    {
        //        var log = _comando.GetLastLog(comando.CodigoEmpresa, comando.ComandoId);
        //        if (log != null)
        //        {
        //            comando.DataExecucao = log.DataCadastro.ToString("G");
        //            comando.UltimosParametros = log.Parametros;
        //        }
        //        else
        //        {
        //            comando.DataExecucao = "Não executado";
        //            comando.UltimosParametros = "Não executado";
        //        }
        //    }
        //    return Json(comandos.ToList());
        //}

        private List<ComandoViewModel> ListaComandos(IEnumerable<Guid> comandos)
        {
            var listaComandos = _comando.GetByPredicate(x => x.Ativo).ToList();
            return (from item in comandos from itens in listaComandos where item == itens.Id select itens).ToList();
        }

        private static List<string> ListaParametros(IEnumerable<ComandoViewModel> comandos)
        {
            var resultado = new List<string>();
            var parametros = comandos.Aggregate<ComandoViewModel, string>(null, (current, listaSql) => string.Concat(current, listaSql.Parametro + ";"));

            if (parametros != null)
            {
                var listaParametros = parametros.Split(';');
                resultado = listaParametros.Where(s => !string.IsNullOrEmpty(s) && s != "CODIGOEMPRESA").Distinct().ToList();
            }
            return resultado;
        }

        private static IList<ComandoViewModel> ListaExecucao(IList<ComandoViewModel> lista, IList<string> parametros, IList<string> valores, out List<ParametroValorViewModel> valorComandos, int codigoEmpresa)
        {
            valorComandos = new List<ParametroValorViewModel>();

            var tiposParametros = new List<string>();
            foreach (var item in lista)
                tiposParametros.AddRange(item.Parametro.Split(";"));

            tiposParametros = tiposParametros.Distinct().ToList();

            foreach (var sql in lista)
            {
                var valor = "";
                sql.Sql = sql.Sql.Replace("@CODIGOEMPRESA", codigoEmpresa.ToString());
                for (var i = 0; i <= parametros.Count - 1; i++)
                {
                    var tipo = tiposParametros.FirstOrDefault(x => x.StartsWith(parametros[i]));
                    var valorParam = "";
                    if (!string.IsNullOrEmpty(tipo) && tipo.Contains(":"))
                    {
                        int.TryParse(tipo.Split(":")[1], out var tipoValor);
                        switch (tipoValor)
                        {
                            //Date = 0, 
                            //Text = 1, 
                            //Number = 2, 
                            //Estab = 3, 
                            //Enum = 4, 
                            //FileTexto = 5, 
                            //FileXML = 6, 
                            //FileExcel = 7, 
                            //FileWord = 8, 
                            //FilePDF = 9, 
                            //CheckBox = 10, 
                            //GrupoProduto = 11

                            case 0:
                                var date = valores[i].Split('/');
                                valorParam = $"'{date[2]}-{date[1]}-{date[0]}'";
                                break;
                            case 1:
                                if (valores[i].Contains(','))
                                    valorParam = $"'{valores[i].Replace(",", "','")}'";
                                else
                                    valorParam = $"'{valores[i]}'";
                                break;
                            case 2:
                                valorParam = valores[i];
                                break;
                            case 3:
                                valorParam = valores[i];
                                break;
                            case 11:
                                valorParam = valores[i];
                                break;
                            default:
                                break;
                        }

                    }
                    else
                    {
                        valorParam = valores[i];
                        tipo = parametros[i];
                    }
                    sql.Sql = sql.Sql.Replace("@" + tipo, valorParam);

                    //sql.Sql = sql.Sql.Replace("@" + parametros[i], valores[i]);
                    //if (valores[i].Contains("/") && valores[i].Length == 10)
                    //{
                    //    var date = valores[i].Split('/');
                    //    sql.Sql = sql.Sql.Replace(valores[i], $"'{date[2]}-{date[1]}-{date[0]}'");
                    //}
                    valor = valor + valores[i] + ";";
                }
                valorComandos.Add(new ParametroValorViewModel
                {
                    CodigoSql = sql.Codigo,
                    ValorParametros = valor
                });
                sql.Sql = sql.Sql.Replace("\r\n", " ");
            }
            return lista;
        }

        #endregion

        public IActionResult ExecutarConsulta(int codigo, string comandos, Guid conexao, string parametros, string valores)
        {
            var listaComandosId = comandos.Split(',');
            var listaGuid = listaComandosId.Select(Guid.Parse).ToList();
            var listaComandos = ListaComandos(listaGuid);
            var listaParametros = parametros.Split(',');
            var listaValores = valores.Split(',');
            var listaExecucao = ListaExecucao(listaComandos, listaParametros, listaValores, out var valorComandos, codigo);
            //var usuarioId = _usuario.GetByExpression(u => u.Email == _user.Name).First().Id;

            if (listaExecucao.Count > 0)
            {
                var connectionString = _bancoQuestor.GetById(conexao).ToString();

                var fileName = $"ConsultaSQL_{DateTime.Now:ddMMyyyy_hhmm}";
                var package = new ExcelPackage();

                foreach (var comando in listaExecucao)
                {
                    var data = new SqlUtils(connectionString).GetDataTable(comando.Sql);

                    //_logComando.Add(new LogModel()
                    //{
                    //    CodigoEmpresa = codigo,
                    //    ComandoId = comando.Id,
                    //    CodigoComando = comando.Codigo,
                    //    BancoQuestorId = conexao,
                    //    Sql = comando.Sql,
                    //    Resultado = "",
                    //    Parametros = valorComandos.Find(x => x.CodigoSql == comando.Codigo).ValorParametros,
                    //    Departamento = comando.Departamento,
                    //    UsuarioId = usuarioId
                    //});

                    var worksheet = package.Workbook.Worksheets.Add($"Consulta_{comando.Codigo}");
                    worksheet.Cells["A1"].LoadFromDataTable(data, true);
                }

                var fileContents = package.GetAsByteArray();

                if (fileContents == null || fileContents.Length == 0)
                    return NotFound();

                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName + ".xlsx");
            }

            return View("Consulta");
        }

        public IActionResult ExecutarAlteracao(int codigo, string comandos, Guid conexao, string parametros, string valores)
        {
            var listaComandosId = comandos.Split(',');
            var listaGuid = listaComandosId.Select(Guid.Parse).ToList();
            var listaComandos = ListaComandos(listaGuid);
            var listaParametros = parametros.Split(',');
            var listaValores = valores.Split(',');
            var listaExecucao = ListaExecucao(listaComandos, listaParametros, listaValores, out var valorComandos, codigo);
            //var usuarioId = _usuario.GetByExpression(u => u.Email == _user.Name).First().Id;

            var resultado = new List<string>();
            if (listaExecucao.Count > 0)
            {
                var connectionString = _bancoQuestor.GetById(conexao).ToString();

                foreach (var comando in listaExecucao)
                {
                    var result = new SqlUtils(connectionString).Execute(comando.Sql);

                    //_logComando.Add(new LogModel()
                    //{
                    //    CodigoEmpresa = codigo,
                    //    ComandoId = comando.Id,
                    //    CodigoComando = comando.Codigo,
                    //    BancoQuestorId = conexao,
                    //    Sql = comando.Sql,
                    //    Resultado = $"{result} linha(s) foram afetadas",
                    //    Parametros = valorComandos.Find(x => x.CodigoSql == comando.Codigo).ValorParametros,
                    //    Departamento = comando.Departamento,
                    //    UsuarioId = usuarioId
                    //});

                    resultado.Add($"{comando.Codigo} - {comando.Descricao}: {result} linha(s) foram afetadas.");
                }
            }

            return RedirectToAction("Execucao", new { result = resultado.Aggregate((x, y) => x + ';' + y) });
        }

        //public IActionResult ExecutarRotina(int codigo, string comandos, Guid conexao, string parametros, string valores)
        //{
        //    var comandosEmpresa = JsonConvert.DeserializeObject<List<ComandoEmpresaCompletoModel>>(comandos);
        //    var listaComandos = comandosEmpresa.OrderBy(x => x.OrdemExecucao)
        //        .Select(comando => new ComandoViewModel()
        //        {
        //            Id = comando.ComandoId,
        //            Codigo = comando.CodigoComando,
        //            Departamento = comando.Departamento,
        //            Descricao = comando.DescricaoComando,
        //            Parametro = comando.Parametro,
        //            Sql = comando.Sql,
        //            TipoSql = comando.TipoSql,
        //            TipoMovimento = comando.TipoMovimento,
        //            Ativo = comando.Ativo,
        //            DescricaoDepartamento = comando.DescricaoDepartamento
        //        })
        //        .ToList();


        //    var listaParametros = parametros.Split(',');
        //    var listaValores = valores.Split(',');
        //    var listaExecucao = ListaExecucao(listaComandos, listaParametros, listaValores, out var valorComandos, codigo);

        //    var resultado = new List<string>();
        //    if (listaExecucao.Count > 0)
        //    {
        //        var connectionString = _bancoQuestor.GetById(conexao).ToString();

        //        foreach (var comando in listaExecucao)
        //        {
        //            var result = new SqlUtils(connectionString).Execute(comando.Sql);

        //            _comando.AddLog(new LogViewModel()
        //            {
        //                CodigoEmpresa = codigo,
        //                ComandoId = comando.Id,
        //                CodigoComando = comando.Codigo,
        //                BancoQuestorId = conexao,
        //                Sql = comando.Sql,
        //                Resultado = $"{result} linha(s) foram afetadas",
        //                Parametros = valorComandos.Find(x => x.CodigoSql == comando.Codigo).ValorParametros,
        //                Departamento = comando.Departamento,
        //                CodigoUsuario = 52
        //            });

        //            resultado.Add($"{comando.Codigo} - {comando.Descricao}: {result} linha(s) foram afetadas.");
        //        }
        //    }

        //    return RedirectToAction("Rotina", new { result = resultado.Aggregate((x, y) => x + ';' + y) });
        //}

        public IActionResult Download(Guid id)
        {
            var log = _comando.GetLogById(id);
            var data = new SqlUtils(log.ConnectionString).GetDataTable(log.Sql);
            var fileName = $"ConsultaSQL_{DateTime.Now:ddMMyyyy_hhmm}";

            var fileContents = new ExportExcel().Export(data, $"Consulta_{log.CodigoComando}");

            if (fileContents == null || fileContents.Length == 0)
                return NotFound();

            //_logComando.Add(new LogModel()
            //{
            //    CodigoEmpresa = codigo,
            //    ComandoId = comando.Id,
            //    CodigoComando = comando.Codigo,
            //    BancoQuestorId = conexao,
            //    Sql = comando.Sql,
            //    Resultado = "",
            //    Parametros = valorComandos.Find(x => x.CodigoSql == comando.Codigo).ValorParametros,
            //    Departamento = comando.Departamento,
            //    UsuarioId = usuarioId
            //});

            return File(fileContents: fileContents, contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownloadName: fileName + ".xlsx");
        }

        public IActionResult ReExecute(Guid id)
        {
            var log = _comando.GetLogById(id);
            var result = new SqlUtils(log.ConnectionString).Execute(log.Sql);

            //_logComando.Add(new LogModel()
            //{
            //    CodigoEmpresa = codigo,
            //    ComandoId = comando.Id,
            //    CodigoComando = comando.Codigo,
            //    BancoQuestorId = conexao,
            //    Sql = comando.Sql,
            //    Resultado = "",
            //    Parametros = valorComandos.Find(x => x.CodigoSql == comando.Codigo).ValorParametros,
            //    Departamento = comando.Departamento,
            //    UsuarioId = usuarioId
            //});

            return result > 0
                ? RedirectToAction("Index", new { success = $"{result} linhas afetadas!" })
                : RedirectToAction("Index", new { error = $"Não há linhas alteradas!" });
        }


        public void Load()
        {
            ViewBag.Empresas = _crm.GetEmpresasAtivas();
            ViewBag.Departamentos = _crm.GetDepartamentos();
            ViewBag.BancoQuestor = _bancoQuestor.GetAll();
        }
    }
}
