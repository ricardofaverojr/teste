﻿using System;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos.Interfaces;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.Processo.Controllers
{
    [Area("Processo")]
    public class ImpostoRetidoController : BaseController<ImpostoRetidoViewModel, IImpostoRetidoAppService>
    {
        private readonly IImpostoRetidoAppService _service;

        public ImpostoRetidoController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IImpostoRetidoAppService service) : base(notifications, mediatorHandler, service)
        {
            _service = service;
        }

        [HttpPost]
        public JsonResult Add(ImpostoRetidoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(ImpostoRetidoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Update(entity));

        [HttpGet]
        public virtual IActionResult Delete(Guid id) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!" })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!" });
    }
}
