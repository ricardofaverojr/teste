﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infovia.Escritorio.Processo.Application.PlanosReferencia;
using Infovia.Escritorio.Processo.Application.PlanosReferencia.Interfaces;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.Processo.Controllers
{
    [Area("Processo")]
    public class PlanoReferenciaController : Controller
    {
        private readonly IPlanoReferenciaAppService _service;
        private readonly IContabilAppService _contabil;
        private readonly ICrmAppService _crm;


        public PlanoReferenciaController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IPlanoReferenciaAppService service, IContabilAppService contabil, ICrmAppService crm)
        {
            _service = service;
            _contabil = contabil;
            _crm = crm;
        }

        public IActionResult Index(int? codigo, string success, string error)
        {
            var planoEspecifico = new List<PlanoEspecificoQuestor>();
            var planosReferencia = new List<PlanoReferenciaViewModel>();

            if (codigo != null)
            {
                planosReferencia = _service.GetByPredicate(hr => hr.CodigoEmpresa == codigo).ToList();
                ViewBag.Codigo = codigo;
                planoEspecifico = _contabil.GetPlanoEspecifico((int)codigo).ToList();
                foreach (var conta in planosReferencia)
                    planoEspecifico.Find(h => h.CodigoConta == conta.CodigoConta).Ativo = false;
            }

            ViewBag.PlanoEspecifico = planoEspecifico;
            return View(planosReferencia);
        }

        [HttpPost]
        public JsonResult Add(PlanoReferenciaViewModel entity) => Json(ModelState.IsValid && _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(PlanoReferenciaViewModel entity) => Json(ModelState.IsValid && _service.Update(entity));

        [HttpGet]
        public virtual IActionResult Delete(Guid id, int codigo) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!", codigo })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!", codigo });
    }
}
