﻿using System;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados.Interfaces;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.Processo.Controllers
{
    public class ProcessoPersonalizadoController : BaseController<ProcessoPersonalizadoViewModel, IProcessoPersonalizadoAppService>
    {
        private readonly IProcessoPersonalizadoAppService _service;

        public ProcessoPersonalizadoController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IProcessoPersonalizadoAppService service) : base(notifications, mediatorHandler, service)
        {
            _service = service;
        }

        [HttpPost]
        public JsonResult Add(ProcessoPersonalizadoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(ProcessoPersonalizadoViewModel entity) => Json(ModelState.IsValid && IsValid() && _service.Update(entity));

        [HttpGet]
        public virtual IActionResult Delete(Guid id, int codigo) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!", codigo })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!", codigo });
    }
}
