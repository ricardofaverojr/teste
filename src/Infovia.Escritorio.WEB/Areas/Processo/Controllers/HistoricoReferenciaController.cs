﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia.Interfaces;
using Infovia.Escritorio.Questor.Applications.Interfaces;
using Infovia.Escritorio.Questor.Models;
using Infovia.Escritorio.WEB.Controllers;
using Infovia.NugetLibrary.DevPack.Mediator.Interfaces;
using Infovia.NugetLibrary.DevPack.Messages.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.WEB.Areas.Processo.Controllers
{
    [Area("Processo")]
    public class HistoricoReferenciaController : Controller
    {
        private readonly IHistoricoReferenciaAppService _service;
        private readonly IContabilAppService _contabil;
        private readonly ICrmAppService _crm;

        public HistoricoReferenciaController(INotificationHandler<DomainNotification> notifications, IMediatorHandler mediatorHandler, IHistoricoReferenciaAppService service, IContabilAppService contabil, ICrmAppService crm)
        {
            _service = service;
            _contabil = contabil;
            _crm = crm;
        }

        public IActionResult Index(int? codigo, string success, string error)
        {
            var historicos = new List<HistoricoReferenciaViewModel>();
            var historicosQuestor = new List<HistoricoContabilQuestor>();
            if (codigo != null)
            {
                ViewBag.Codigo = codigo;
                historicos = _service.GetByPredicate(hr => hr.CodigoEmpresa == codigo).ToList();
                historicosQuestor = _contabil.GetHistoricoCtb().ToList();

                foreach (var historico in historicos)
                    historicosQuestor.Find(h => h.CodigoHistorico == historico.CodigoHistorico).Ativo = false;
            }
            ViewBag.Historicos = historicosQuestor;
            LoadViewBag(success, error);
            return View(historicos.ToList());
        }

        [HttpPost]
        public JsonResult Add(HistoricoReferenciaViewModel entity) => Json(ModelState.IsValid &&  _service.Add(entity));

        [HttpPost]
        public JsonResult Edit(HistoricoReferenciaViewModel entity) => Json(ModelState.IsValid && _service.Update(entity));

        [HttpGet]
        public IActionResult Delete(Guid id, int codigo) =>
            _service.Delete(id)
                ? RedirectToAction("Index", new { success = "Removido!", codigo })
                : RedirectToAction("Index", new { error = "Exclusão cancelada!", codigo });

        public void LoadViewBag(string success, string error)
        {
            ViewBag.Empresas = _crm.GetEmpresasAtivas();
            ViewBag.Success = success;
            ViewBag.Error = error;
        }
    }
}
