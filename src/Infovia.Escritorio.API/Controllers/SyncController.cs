﻿using Infovia.Escritorio.Questor.Applications.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyncController : Controller
    {
        private readonly ISyncCacheAppService _service;

        public SyncController(ISyncCacheAppService service)
        {
            _service = service;
        }

        [HttpGet("sync")]
        public bool Sync()
        {
            _service.StoreCache();
            return true;
        }
    }
}
