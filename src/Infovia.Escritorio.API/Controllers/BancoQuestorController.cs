using System;
using System.Collections.Generic;
using Infovia.Escritorio.SQL.Application.BancosQuestor;
using Infovia.Escritorio.SQL.Application.BancosQuestor.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Infovia.Escritorio.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BancoQuestorController : ControllerBase
    {
        private readonly IBancoQuestorAppService _service;

        public BancoQuestorController(IBancoQuestorAppService service)
        {
            _service = service;
        }

        // GET: api/BancoQuestor
        [HttpGet]
        public IEnumerable<BancoQuestorViewModel> Get()
        {
            return _service.GetAll();
        }

        // GET: api/BancoQuestor/5
        [HttpGet("{id:guid}", Name = "Get")]
        public BancoQuestorViewModel Get(Guid id)
        {
            return _service.GetById(id);
        }

        // POST: api/BancoQuestor
        [HttpPost]
        public void Post(BancoQuestorViewModel value)
        {
            var banco = value;
            if (ModelState.IsValid)
                _service.Add(banco);
        }

        // PUT: api/BancoQuestor/5
        [HttpPut("{id:guid}")]
        public void Put(Guid id, BancoQuestorViewModel value)
        {
            //value.Id = id;
            if (ModelState.IsValid)
                _service.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id:guid}")]
        public void Delete(Guid id)
        {
            _service.Delete(id);
        }

        [HttpGet("padrao/{id:guid}")]
        public void Padrao(Guid id)
        {
            _service.Padrao(id);
        }

        [HttpGet("teste/{id:guid}")]
        public bool Teste(Guid id)
        {
            return _service.Teste(id);
        }
    }
}
