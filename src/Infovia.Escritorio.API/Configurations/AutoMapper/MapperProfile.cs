﻿using System;
using AutoMapper;
using Infovia.Escritorio.CRM.Application.Contatos;
using Infovia.Escritorio.CRM.Domain.Contatos;
using Infovia.Escritorio.Processo.Application.Analises;
using Infovia.Escritorio.Processo.Application.HistoricosReferencia;
using Infovia.Escritorio.Processo.Application.ImpostosRetidos;
using Infovia.Escritorio.Processo.Application.LeiautesContabeis;
using Infovia.Escritorio.Processo.Application.PlanosReferencia;
using Infovia.Escritorio.Processo.Application.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Application.TabelasContribuicao;
using Infovia.Escritorio.Processo.Application.TabelasFiscal;
using Infovia.Escritorio.Processo.Domain.Analises;
using Infovia.Escritorio.Processo.Domain.HistoricosReferencia;
using Infovia.Escritorio.Processo.Domain.ImpostosRetidos;
using Infovia.Escritorio.Processo.Domain.LeiautesContabeis;
using Infovia.Escritorio.Processo.Domain.PlanosReferencia;
using Infovia.Escritorio.Processo.Domain.ProcessosPersonalizados;
using Infovia.Escritorio.Processo.Domain.TabelasContribuicao;
using Infovia.Escritorio.Processo.Domain.TabelasFiscal;
using Infovia.Escritorio.SQL.Application.BancosQuestor;
using Infovia.Escritorio.SQL.Application.Comandos;
using Infovia.Escritorio.SQL.Domain.BancosQuestor;
using Infovia.Escritorio.SQL.Domain.Comandos;

namespace Infovia.Escritorio.API.Configurations.AutoMapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            // SQL
            CreateMap<BancoQuestor, BancoQuestorViewModel>().ReverseMap();
            CreateMap<Comando, ComandoViewModel>().ReverseMap();
            CreateMap<Log, LogViewModel>().ReverseMap();
            CreateMap<ComandoEmpresa, ComandoEmpresaViewModel>().ReverseMap();

            // PROCESSO
            CreateMap<HistoricoReferencia, HistoricoReferenciaViewModel>().ReverseMap();
            CreateMap<ImpostoRetido, ImpostoRetidoViewModel>().ReverseMap();
            CreateMap<LeiauteContabil, LeiauteContabilViewModel>().ReverseMap();
            CreateMap<PlanoReferencia, PlanoReferenciaViewModel>().ReverseMap();
            CreateMap<TabelaContribuicao, TabelaContribuicaoViewModel>().ReverseMap();
            CreateMap<TabelaFiscal, TabelaFiscalViewModel>().ReverseMap();
            CreateMap<ProcessoPersonalizado, ProcessoPersonalizadoViewModel>().ReverseMap();
            CreateMap<Analise, AnaliseViewModel>().ReverseMap();
            CreateMap<AnalisePadrao, AnalisePadraoViewModel>().ReverseMap();
            CreateMap<AnaliseEspecifica, AnaliseEspecificaViewModel>().ReverseMap();
            CreateMap<AnalisePadraoFormula, AnalisePadraoFormulaViewModel>().ReverseMap();
            CreateMap<AnaliseEspecificaFormula, AnaliseEspecificaFormula>().ReverseMap();

            // CRM
            CreateMap<Contato, ContatoViewModel>().ReverseMap();
        }
    }
}
